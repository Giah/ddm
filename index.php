<?php
require_once('vendor/autoload.php');
require_once('functions.php');

$env = new Dotenv\Dotenv(__DIR__);
$env->load();

error_reporting(E_ALL);

if ($_ENV['DEBUG'] === 'true') {
  // Custom error handler
  set_error_handler(function($severity, $message, $filename, $lineno) {
    if (error_reporting() == 0) { return; }
    // Throw Exception on notice and warning in order to intercept with Behat
    if (error_reporting() & $severity) {
      throw new \ErrorException($message, 0, $severity, $filename, $lineno);
    }
  });

  Kint::enabled(true);

  $whoops = new \Whoops\Run;
  $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
  $whoops->register();
  $faker_seed = $_ENV['FAKER_SEED'];
}
else { // Write error on log file
  $log = $_ENV['PATHS_ERROR_LOG'];
  if(!file_exists(dirname($log))) {
    mkdir(dirname($log), 0777, true);
  }
  if (!file_exists($log)) {
    fclose(fopen($log, 'w'));
  }
  ini_set("log_errors", 1);
  ini_set("error_log", $log);
  ini_set('display_errors', 0);
  Kint::enabled(false);
  $faker_seed = false;
}

// customize in functions.php
$template = get_template_name();

/*========================
 Get page data
========================*/

/* Replace dashed-names to CamelCase */
$dataClass = str_replace('-', '', ucwords($template, '-'));

/* Get page data */
require_once("data/$dataClass.php");
$page = new $dataClass($faker_seed);

/* Get shared data */
require_once("data/Common.php");
$common = new Common();

/*========================
 Setup Plates template engine: http://platesphp.com/
========================*/
require_once('lib/PlatesUtils.php');
$templates = new League\Plates\Engine();
$templates->addFolder('layouts', $_ENV['PATHS_TEMPLATES_LAYOUTS']);
$templates->addFolder('pages', $_ENV['PATHS_TEMPLATES_PAGES']);
$templates->addFolder('components', $_ENV['PATHS_TEMPLATES_COMPONENTS']);
$templates->loadExtension(new PlatesUtils());


/*==========  RENDER  ==========*/
$templates->addData(array_merge(
  $common->toArray(),
  $page->toArray(),
  [
    'template' => $template, // also pass the name of the template as a variable
    'debug' => $_ENV['DEBUG'] === 'true'
  ]
));
echo $templates->render("pages::$template");
