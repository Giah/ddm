module.exports = {
  root: {
    src: './src',
    dest: './public',
  },

  tasks: {
    static: {
      src: 'static',
      dest: './',
    },

    js: {
      src: 'javascripts',
      dest: 'javascripts',
      entries: {
        app: ['app.js'],
      },
      alias: {
        vue$: 'node_modules/vue/dist/vue.common.js',
      },
      extensions: ['js', 'json'],
      babel: {
        presets: [
          [
            'env', {
              targets: {
                browsers: [
                  'last 2 versions',
                  'safari >= 7',
                  'IE >= 10',
                ],
              },
            },
          ],
        ],
        plugins: [
          'transform-strict-mode',
          'babel-plugin-transform-object-rest-spread',
        ],
      },
      extractSharedJs: false,
    },

    css: {
      src: 'stylesheets',
      dest: 'stylesheets',
      sass: {
        includePaths: [],
      },
      postcss: {
        'postcss-will-change': {},
        autoprefixer: {
          browsers: [
            '> 5%',
            'ie > 9',
            'Safari > 7',
          ],
        },
      },
      extensions: ['scss', 'css'],
    },

    images: {
      src: 'images',
      dest: 'images',
      extensions: ['jpg', 'png', 'svg', 'gif'],
    },

    fonts: {
      src: 'fonts',
      dest: 'fonts',
      extensions: ['woff2', 'woff'],
    },

    sprites: {
      src: 'sprites',
      dest: 'images',
      extensions: ['svg', 'jpg', 'png'],
    },

    svgSprite: {
      src: 'sprites',
      dest: 'images',
      extensions: ['svg'],
    },

    imageSprite: {
      src: 'sprites',
      dest: 'images',
      extensions: ['jpg', 'png'],
      sass: {
        dest: 'stylesheets/generated',
        extensions: ['scss', 'css'],
      },
    },

    production: {
      rev: true,
    },
  },
};
