var gulp = require('gulp');
var del = require('del');
var path = require('path');
var config = require('../config');

var cleanTask = (cb) => {
  del([
    path.join(config.root.dest, '/**'),
    path.join('!', config.root.dest),
    path.join(config.root.src, config.tasks.imageSprite.sass.dest, '/**'),
    path.join('!', config.root.src, config.tasks.imageSprite.sass.dest),
  ]).then((paths) => {
    cb();
  });
};

gulp.task('clean', cleanTask);
module.exports = cleanTask;
