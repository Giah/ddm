var config       = require('../config')
if(!config.tasks.imageSprite || !config.tasks.css) return

var getFolders   = require('../lib/getFolders')
var handleErrors = require('../lib/handleErrors')
var gulp         = require('gulp')
var rename       = require('gulp-rename')
var sass         = require('gulp-sass')
var path         = require('path')
var postcss      = require('gulp-postcss')
var processor    = require('postcss')

function postcssProcessors(stylesheetPath, spritePath, name, format) {
  var sprites = require('postcss-sprites')({
  	stylesheetPath: stylesheetPath,
    spritePath: spritePath,
  	relativeTo: 'rule',
  	spritesmith: {
  		exportOpts: {
  			format: format
  		},
  		algorithm: 'binary-tree',
  		engine: 'gmsmith',
  	},
  	hooks: {
      onUpdateRule: (rule, token, image) => {
        var backgroundSizeX = (image.spriteWidth / image.coords.width) * 100;
        var backgroundSizeY = (image.spriteHeight / image.coords.height) * 100;
        var backgroundPositionX = (image.coords.x / (image.spriteWidth - image.coords.width)) * 100;
        var backgroundPositionY = (image.coords.y / (image.spriteHeight - image.coords.height)) * 100;

        backgroundSizeX = isNaN(backgroundSizeX) ? 0 : backgroundSizeX;
        backgroundSizeY = isNaN(backgroundSizeY) ? 0 : backgroundSizeY;
        backgroundPositionX = isNaN(backgroundPositionX) ? 0 : backgroundPositionX;
        backgroundPositionY = isNaN(backgroundPositionY) ? 0 : backgroundPositionY;

        var backgroundImage = processor.decl({
            prop: 'background-image',
            value: 'url(' + image.spriteUrl + ')'
        });

        var backgroundSize = processor.decl({
            prop: 'background-size',
            value: backgroundSizeX + '% ' + backgroundSizeY + '%'
        });

        var backgroundPosition = processor.decl({
            prop: 'background-position',
            value: backgroundPositionX + '% ' + backgroundPositionY + '%'
        });

        rule.insertAfter(token, backgroundImage);
        rule.insertAfter(backgroundImage, backgroundPosition);
        rule.insertAfter(backgroundPosition, backgroundSize);
      },
      onSaveSpritesheet: function () {
        return path.join(spritePath, name + '.' + format);
      }
    }
  });

  return [
    sprites,
  ];
}

var imageSpriteTask = function() {

  var paths = {
    src: path.join(config.root.src, config.tasks.imageSprite.src),
    dest: path.join(config.root.dest, config.tasks.imageSprite.dest),
    dest_sass: path.join(config.root.src, config.tasks.imageSprite.sass.dest)
  }

  return config.tasks.imageSprite.extensions.map(extension => {
    var folders = getFolders(path.join(paths.src, extension));

    return folders.map(folder => {
      var src_sass = path.join(paths.src, extension, folder, '*{' + config.tasks.imageSprite.sass.extensions + '}');

      return gulp.src(src_sass)
        .pipe(sass({ src: src_sass, dest: paths.dest_sass }))
        .pipe(postcss(postcssProcessors(
          path.join(config.root.dest, config.tasks.css.dest),
          paths.dest,
          folder,
          extension
        )))
        .on('error', handleErrors)
        .pipe(rename('_' + folder + '.scss'))
        .pipe(gulp.dest(paths.dest_sass))
    });
  });
}

gulp.task('imageSprite', imageSpriteTask)
module.exports = imageSpriteTask
