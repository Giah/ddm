var gulp            = require('gulp')
var gulpSequence    = require('gulp-sequence')
var getEnabledTasks = require('../lib/getEnabledTasks')

var codeTask = function(cb) {
  var tasks = getEnabledTasks('development')
  gulpSequence(tasks.codeTasks, cb)  
}

gulp.task('code', codeTask)
module.exports = codeTask
