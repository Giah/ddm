var config      = require('../config')
if(!config.tasks.svgSprite) return

var getFolders  = require('../lib/getFolders')
var browserSync = require('browser-sync')
var gulp        = require('gulp')
var imagemin    = require('gulp-imagemin')
var svgstore    = require('gulp-svgstore')
var path        = require('path')
var fs          = require('fs')

var svgSpriteTask = function() {

  var paths = {
    src: path.join(config.root.src, config.tasks.svgSprite.src),
    dest: path.join(config.root.dest, config.tasks.svgSprite.dest)
  }

  return config.tasks.svgSprite.extensions.map(extension => {
    var folders = getFolders(path.join(paths.src, extension));

    return folders.map(folder => {
      var files = path.join(paths.src, extension, folder, '/*.' + extension);

      return gulp.src([files, '*!README.md'])
        .pipe(imagemin())
        .pipe(svgstore())
        .pipe(gulp.dest(paths.dest))
        .pipe(browserSync.stream())
    });
  });
}

gulp.task('svgSprite', svgSpriteTask)
module.exports = svgSpriteTask
