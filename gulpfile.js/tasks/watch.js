var config = require('../config')
var gulp   = require('gulp')
var path   = require('path')
var watch  = require('gulp-watch')

var watchTask = function() {
  var watchableTasks = ['fonts', 'images', 'svgSprite','html', 'css', 'js']

  watchableTasks.forEach(function(taskName) {
    var task = config.tasks[taskName]
    if(task) {
      var glob = path.join(config.root.src, task.src, '**/*.{' + task.extensions.join(',') + '}')
      if(taskName === 'js') {
        taskName = 'webpackDevelopment'
      }
      watch(glob, function() {
       require('./' + taskName)()
      })
    }
  })
}

gulp.task('watch', watchTask)
module.exports = watchTask
