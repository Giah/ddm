var path = require('path');
var fs = require('fs');

module.exports = function getFolders(dir) {
  if (fs.existsSync(dir)) {
    return fs.readdirSync(dir).filter(file => fs.statSync(path.join(dir, file)).isDirectory());
  }
  return [];
};
