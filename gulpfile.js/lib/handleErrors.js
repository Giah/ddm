var notify = require("gulp-notify")

module.exports = function(errorObject, callback) {
  notify.onError(function(errorObject){ return errorObject.toString().split(': ').join(':\n'); })
  // Keep gulp from hanging on this task
  if (typeof this.emit === 'function') this.emit('end')
}
