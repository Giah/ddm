<?php $this->layout('layouts::default') ?>

<section class="ddm-Login ddm-Container" is="Appear">

  <?php $this->insert('components::section-header', [
    'title' => $title,
    'sub_title' => $sub_title,
    'classes' => ['m-t(l)'],
    'variants' => ['small'],
  ]) ?>

  <div class="m-t(l)">

    <form class="ddm-Login-form ddm-Box" action="<?= $this->e($form['action']) ?>" method="get">

      <h3 class="ddm-Login-title"><?= $form['title'] ?></h3>
      <p class="ddm-Login-subtitle"><?= $form['sub_title'] ?></p>

      <div class="ddm-Login-formField ddm-Login-formField--email">
        <label class="ddm-Login-formLabel ddm-Login-formLabel--email ddm-FormLabel" for="<?= $this->e($form['email']['name']) ?>"><?= $form['email']['label'] ?></label>
        <input class="ddm-Login-input ddm-Login-input--email ddm-Input ddm-Input--white" type="text" name="<?= $this->e($form['email']['name']) ?>"/>
      </div>

      <div class="ddm-Login-formField ddm-Login-formField--password">
        <label class="ddm-Login-formLabel ddm-Login-formLabel--password ddm-FormLabel" for="<?= $this->e($form['password']['name']) ?>"><?= $form['password']['label'] ?></label>
        <input class="ddm-Login-input ddm-Login-input--password ddm-Input ddm-Input--white" type="password" name="<?= $this->e($form['password']['name']) ?>"/>
      </div>

      <div class="ddm-Login-formField ddm-Login-formField--passwordRecover">
        <a class="ddm-Login-passwordRecover" href="<?= $this->e($form['password_recover']['href']) ?>" title="<?= $this->e($form['password_recover']['title']) ?>">
          <?= $form['password_recover']['text'] ?>
        </a>
      </div>

      <div class="ddm-Login-formField ddm-Login-formField--remember">
        <label class="ddm-Login-formLabel ddm-Login-formLabel--remember ddm-FormLabel ddm-FormLabel--inline" for="<?= $this->e($form['remember']['name']) ?>">
          <input class="ddm-Login-input ddm-Login-input--remember ddm-Input" type="checkbox" name="<?= $this->e($form['remember']['name']) ?>"/>
          <?= $form['remember']['label'] ?>
        </label>
      </div>

      <div class="ddm-Login-formField ddm-Login-formField--submit">
        <input class="ddm-Login-input ddm-Login-input--submit ddm-RectButton ddm-RectButton--brownOnWhite" type="submit" name="submit" value="<?= $this->e($form['submit']['value']) ?>">
      </div>
    </form>

    <div class="ddm-Login-register ddm-Box">
      <h2 class="ddm-Login-title"><?= $register['title'] ?></h2>
      <p class="ddm-Login-subtitle"><?= $register['subtitle'] ?></p>
      <a class="ddm-Login-registerLink ddm-RectButton ddm-RectButton--brownOnWhite" href="<?= $this->e($register['link']['href']) ?>" title="<?= $this->e($register['link']['title']) ?>">
        <?= $register['link']['text'] ?>
      </a>
    </div>

  </div>

</section>
