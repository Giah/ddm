<?php $this->layout('layouts::default') ?>

<section class="ddm-ShopEmotional">

  <form class="ddm-ShopEmotional-form" is="ShopEmotionalForm" action="index.php?template=shop">

    <?php foreach($form as $key => $section): ?>
      <div class="ddm-ShopEmotional-formSection" is="ShopEmotionalFormSection" :section-index="<?= $key ?>">

        <div class="ddm-ShopEmotional-background">
          <div class="ddm-ShopEmotional-background-smartphone" style="background-image:url('<?= $section['background']['smartphone'] ?>');"></div>
          <div class="ddm-ShopEmotional-background-tablet" style="background-image:url('<?= $section['background']['tablet'] ?>');"></div>
          <div class="ddm-ShopEmotional-background-desktop" style="background-image:url('<?= $section['background']['desktop'] ?>');"></div>
        </div>

        <div class="ddm-ShopEmotional-formContent">

          <h2 class="ddm-ShopEmotional-question">
            <?= $form[$key]['question'] ?>
          </h2>

          <div class="ddm-ShopEmotional-inputs">

            <?php foreach($section['inputs'] as $key2 => $input): ?>

              <?php if($input['type'] === 'button'): ?>

                <button class="ddm-ShopEmotional-button ddm-RectButton ddm-RectButton--transparent">
                  <?= $input['text'] ?>
                </button>

              <?php elseif($input['type'] === 'select'): ?>

                <select class="ddm-Input ddm-Input--transparent" is="SelectInput">
                  <?php foreach($input['options'] as $option): ?>
                    <option <?= $this->attributes([
                        'value' => $option['value'],
                        'selected' => (!empty($option['selected']) ? 'selected' : ''),
                      ]);
                    ?>>
                      <?php echo  $option['text']; ?>
                    </option>
                  <?php endforeach; ?>
                </select>

              <?php endif; ?>

            <?php endforeach; ?>

          </div>

        </div>

      </div>
    <?php endforeach; ?>

  </form>

</section>
