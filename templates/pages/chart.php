<?php $this->layout('layouts::default') ?>

<section class="ddm-Chart ddm-Container">

  <?php
  $this->insert('components::section-header', [
    'title' => $title,
    'sub_title' => $summary,
    'classes' => ['m-t(m)'],
    'variants' => ['small', 'left']
  ])
  ?>

  <div class="m-t(l)">

    <form class="ddm-Chart-form" action="<?= $this->e($form['action']) ?>" method="get">

      <?php
      foreach ($articles_table['body'] as &$row):
        foreach ($row as $key => &$cell):
          switch($key):
            case 0: $this->capture(true); ?>

              <div class="ddm-Chart-productImage">
                <picture>
                  <source media="(max-width: 1023px)" srcset="<?= $this->e($cell['image_mobile']['src']) ?>" alt="<?= $this->e($cell['image_mobile']['alt']) ?>">
                  <source media="(min-width: 1024px)" srcset="<?= $this->e($cell['image']['src']) ?>" alt="<?= $this->e($cell['image']['alt']) ?>">
                  <img src="<?= $this->e($cell['image']['src']) ?>" alt="<?= $this->e($cell['image']['alt']) ?>">
                </picture>
              </div>
              <div class="ddm-Chart-productName">
                <?= $cell['name'] ?>
              </div>

            <?php $cell = $this->capture(false);
            break;
            case 1: $this->capture(true); ?>

              <div class="ddm-Chart-productDetails ddm-FormattedHTML"><?= $cell ?></div>

            <?php $cell = $this->capture(false);
            break;
            case 2: $this->capture(true); ?>

              <input class="ddm-Chart-quantity ddm-Input ddm-Input--pink" id="ddm-quantity" name="quantity" type="number" min="1" step="1" value="<?= $this->e($cell) ?>" />

            <?php $cell = $this->capture(false);
            break;
            case 3: $this->capture(true); ?>

              <div class="ddm-Chart-price"><?= $currency . $cell ?></div>

            <?php $cell = $this->capture(false);
            break;
            case 4: $this->capture(true); ?>

              <button class="ddm-ButtonRemove" type="submit" value="<?= $this->e($form['remove']['value']) ?>">
                <span class="ddm-ButtonRemove-icon ddm-Icon">
                  <svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#bin"></use></svg>
                </span>

                <?php echo !empty($form['remove']['text']) ? $form['remove']['text'] : ''; ?>
              </button>

            <?php $cell = $this->capture(false);
            break;
          endswitch;
        endforeach;
      endforeach;

      $this->insert('components::table', [
        'table' => $articles_table,
        'variants' => ['chart']
      ])
      ?>

      <div class="ddm-Chart-recap ddm-Box">
        <div class="ddm-Chart-recapTitle">
          <?= $recap['title'] ?>
        </div>
        <div class="ddm-Chart-recapSum">
          <span class="ddm-Chart-recapLabel"><?= $recap['subtotal'] ?>:</span>
          <span class="ddm-Chart-currency"><?= $currency . $subtotal['formatted'] ?></span>
        </div>
        <div class="ddm-Chart-recapShipping">
          <span class="ddm-Chart-recapLabel"><?= $recap['shipping'] ?>:</span>
          <span class="ddm-Chart-currency"><?= $currency . $shipping_cost['formatted'] ?></span>
          <br/>
          <span class="ddm-Chart-shippingAlternate"><?= $recap['shipping_alternate'] ?></span>
          <button class="ddm-Chart-recalc" type="submit" value="<?= $this->e($form['shipping_recalc']['value']) ?>">
            <?= $form['shipping_recalc']['text'] ?>
          </button>
        </div>
        <div class="ddm-Chart-recapTotal">
          <span class="ddm-Chart-recapLabel"><?= $recap['total'] ?>:</span>
          <span class="ddm-Chart-currency ddm-Chart-currency--total"><?= $currency . $total ?></span>
        </div>
        <div class="ddm-Chart-recapSubmit">
          <button class="ddm-Chart-submit ddm-RectButton ddm-RectButton--brownOnWhite" type="submit" value="<?= $this->e($form['submit']['value']) ?>">
            <?= $form['submit']['text'] ?>
          </button>
        </div>
      </div>

      <div class="ddm-clearfix"></div>
      <div class="ddm-Chart-promo">
        <label class="ddm-Chart-promoLabel" for="promo_code"><?= $promo_labels['label'] ?></label>
        <input class="ddm-Chart-promoInput ddm-Input ddm-Input--white" type="text" placeholder="<?= $this->e($promo_labels['placeholder']) ?>" />
        <button class="ddm-Chart-promoRecalc ddm-RectButton ddm-RectButton--white" type="submit" value="<?= $this->e($form['promo_recalc']['value']) ?>">
          <?= $form['promo_recalc']['text'] ?>
        </button>
      </div>

    </form>

  </div>

</section>
