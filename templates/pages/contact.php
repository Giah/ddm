<?php $this->layout('layouts::default') ?>

<?php $this->insert('components::jumbotron', [ 'centered' => 'centered', ]) ?>

<section class="ddm-Contact ddm-Container" is="Appear">

  <?php $this->insert('components::section-header', [
    'title' => $title,
    'sub_title' => $sub_title,
    'classes' => ['m-t(l)'],
    'variants' => ['inverted'],
  ]) ?>

  <div class="m-t(l)">

    <form class="ddm-Contact-form" action="<?= $this->e($form['action']) ?>" method="get">
      <div class="ddm-Contact-formFields">

        <div class="ddm-Contact-formField ddm-Contact-formField--name">
          <label class="ddm-Contact-formLabel ddm-Contact-formLabel--name ddm-FormLabel" for="<?= $this->e($form['name']['name']) ?>"><?= $form['name']['label'] ?></label>
          <input class="ddm-Contact-input ddm-Contact-input--name ddm-Input ddm-Input--white" type="text" name="<?= $this->e($form['name']['name']) ?>"/>
        </div>

        <div class="ddm-Contact-formField ddm-Contact-formField--surname">
          <label class="ddm-Contact-formLabel ddm-Contact-formLabel--surname ddm-FormLabel" for="<?= $this->e($form['surname']['name']) ?>"><?= $form['surname']['label'] ?></label>
          <input class="ddm-Contact-input ddm-Contact-input--surname ddm-Input ddm-Input--white" type="text" name="<?= $this->e($form['surname']['name']) ?>"/>
        </div>

        <div class="ddm-Contact-formField ddm-Contact-formField--email">
          <label class="ddm-Contact-formLabel ddm-Contact-formLabel--email ddm-FormLabel" for="<?= $this->e($form['email']['name']) ?>"><?= $form['email']['label'] ?></label>
          <input class="ddm-Contact-input ddm-Contact-input--email ddm-Input ddm-Input--white" type="text" name="<?= $this->e($form['email']['name']) ?>"/>
        </div>

        <div class="ddm-Contact-formField ddm-Contact-formField--message">
          <label class="ddm-Contact-formLabel ddm-Contact-formLabel--message ddm-FormLabel" for="<?= $this->e($form['message']['name']) ?>"><?= $form['message']['label'] ?></label>
          <textarea class="ddm-Contact-input ddm-Contact-input--message ddm-Input ddm-Input--white" type="text" name="<?= $this->e($form['message']['name']) ?>" placeholder="<?= $this->e($form['message']['label']) ?>"></textarea>
        </div>

        <div class="ddm-Contact-formField ddm-Contact-formField--privacy">
          <label class="ddm-Contact-formLabel ddm-Contact-formLabel--privacy ddm-FormLabel ddm-FormLabel--inline" for="<?= $this->e($form['privacy']['name']) ?>">
            <input class="ddm-Contact-input ddm-Contact-input--privacy ddm-Input ddm-Input--white" type="checkbox" name="<?= $this->e($form['privacy']['name']) ?>"/>
            <?= $form['privacy']['label'] ?>
          </label>
        </div>

        <div class="ddm-Contact-formField ddm-Contact-formField--newsletter">
          <label class="ddm-Contact-formLabel ddm-Contact-formLabel--newsletter ddm-FormLabel ddm-FormLabel--inline" for="<?= $this->e($form['newsletter']['name']) ?>">
            <input class="ddm-Contact-input ddm-Contact-input--newsletter ddm-Input" type="checkbox" name="<?= $this->e($form['newsletter']['name']) ?>"/>
            <?= $form['newsletter']['label'] ?>
          </label>
        </div>

        <div class="ddm-Contact-formField ddm-Contact-formField--submit">
          <input class="ddm-Contact-input ddm-Contact-input--submit ddm-RectButton ddm-RectButton--brownOnWhite" type="submit" name="submit" value="<?= $this->e($form['submit']['value']) ?>">
        </div>

      </div>
    </form>

    <div class="ddm-Contact-stores">
      <h3 class="ddm-Contact-storesLabel"><?= $stores_label ?></h3>

      <div class="ddm-Contact-cards">

        <?php
        foreach($cards as $card):
          $this->insert('components::contact-card', [
            'image' => $card['image'],
            'name' => $card['name'],
            'address' => $card['address'],
            'tel' => $card['tel'],
            'hours' => $card['hours'],
            'closed' => $card['closed'],
          ]);
        endforeach; ?>

      </div>
    </div>

  </div>

</section>
