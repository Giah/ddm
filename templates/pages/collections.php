<?php $this->layout('layouts::default') ?>

<?php $this->insert('components::jumbotron', [ 'centered' => 'centered', ]) ?>

<section class="ddm-PreviewBoxes ddm-Container m-t(l)@desktop-only" is="Appear">
  <div class="ddm-PreviewBoxes-group">

    <?php
    foreach($preview_boxes as $item):
      $this->insert('components::preview-box', [
        'image' => $item['image'],
        'image_mobile' => $item['image_mobile'],
        'title' => $item['title'],
        'sub_title' => $item['sub_title'],
        'link' => $item['link'],
        'buttonCover' => true,
        'cols' => '3',
      ]);
    endforeach; ?>

  </div>
</section>
