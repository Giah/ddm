<?php $this->layout('layouts::default') ?>

<section class="ddm-Account ddm-Account--overview ddm-Container">

  <?php
  $this->insert('components::account-menu', [
    'title' => $account_menu['title'],
    'items' => $account_menu['items'],
    'active' => 'account-menu-1',
  ])
  ?>

  <?php
  $this->insert('components::section-header', [
    'title' => $title,
    'sub_title' => $sub_title,
    'classes' => ['m-t(l)'],
    'variants' => ['small', 'left']
  ])
  ?>

  <section class="ddm-Account-section ddm-Account-section--preferences m-t(l)">

    <form class="ddm-Preferences-form" action="<?= $this->e($form['action']) ?>" method="get">

      <fieldset class="ddm-Preferences-fieldset ddm-Preferences-fieldset--split ddm-FormFieldset">

        <?php
        $this->insert('components::section-header', [
          'title' => $form['promo-notifications']['header']['title'],
          'sub_title' => $form['promo-notifications']['header']['sub_title'],
          'variants' => ['account', 'left']
        ])
        ?>

        <div class="ddm-Preferences-formField ddm-Preferences-formField--inline ddm-Preferences-formField--promo-notify-email m-t(m)">
          <label class="ddm-Preferences-formLabel ddm-Preferences-formLabel--promo-notify-email ddm-FormLabel ddm-FormLabel--inline" for="<?= $this->e($form['promo-notifications']['promo-notify-email']['name']) ?>">
            <input class="ddm-Preferences-input ddm-Preferences-input--promo-notify-email ddm-Input ddm-Input--white" type="checkbox" name="<?= $this->e($form['promo-notifications']['promo-notify-email']['name']) ?>"/>
            <?= $form['promo-notifications']['promo-notify-email']['label']; ?>
          </label>
        </div>

        <div class="ddm-Preferences-formField ddm-Preferences-formField--inline ddm-Preferences-formField--promo-notify-sms m-t(m)">
          <label class="ddm-Preferences-formLabel ddm-Preferences-formLabel--promo-notify-sms ddm-FormLabel ddm-FormLabel--inline" for="<?= $this->e($form['promo-notifications']['promo-notify-sms']['name']) ?>">
            <input class="ddm-Preferences-input ddm-Preferences-input--promo-notify-sms ddm-Input ddm-Input--white" type="checkbox" name="<?= $this->e($form['promo-notifications']['promo-notify-sms']['name']) ?>"/>
            <?= $form['promo-notifications']['promo-notify-sms']['label']; ?>
          </label>
        </div>

      </fieldset>

      <fieldset class="ddm-Preferences-fieldset ddm-Preferences-fieldset--split ddm-FormFieldset m-t(m)@tablet-and-below">

        <?php
        $this->insert('components::section-header', [
          'title' => $form['shop-notifications']['header']['title'],
          'sub_title' => $form['shop-notifications']['header']['sub_title'],
          'variants' => ['account', 'left']
        ])
        ?>

        <div class="ddm-Preferences-formField ddm-Preferences-formField--inline ddm-Preferences-formField--shop-notify-email m-t(m)">
          <label class="ddm-Preferences-formLabel ddm-Preferences-formLabel--shop-notify-email ddm-FormLabel ddm-FormLabel--inline" for="<?= $this->e($form['shop-notifications']['shop-notify-email']['name']) ?>">
            <input class="ddm-Preferences-input ddm-Preferences-input--shop-notify-email ddm-Input ddm-Input--white" type="checkbox" name="<?= $this->e($form['shop-notifications']['shop-notify-email']['name']) ?>"/>
            <?= $form['shop-notifications']['shop-notify-email']['label']; ?>
          </label>
        </div>

        <div class="ddm-Preferences-formField ddm-Preferences-formField--inline ddm-Preferences-formField--shop-notify-sms m-t(m)">
          <label class="ddm-Preferences-formLabel ddm-Preferences-formLabel--shop-notify-sms ddm-FormLabel ddm-FormLabel--inline" for="<?= $this->e($form['shop-notifications']['shop-notify-sms']['name']) ?>">
            <input class="ddm-Preferences-input ddm-Preferences-input--shop-notify-sms ddm-Input ddm-Input--white" type="checkbox" name="<?= $this->e($form['shop-notifications']['shop-notify-sms']['name']) ?>"/>
            <?= $form['shop-notifications']['shop-notify-sms']['label']; ?>
          </label>
        </div>

      </fieldset>

      <fieldset class="ddm-Preferences-fieldset ddm-Preferences-fieldset--tel ddm-FormFieldset m-t(m)@tablet-and-below">

        <div class="ddm-Preferences-formField ddm-Preferences-formField--tel">
          <label class="ddm-Preferences-formLabel ddm-Preferences-formLabel--tel ddm-FormLabel" for="<?= $this->e($form['tel']['name']) ?>"><?= $form['tel']['label']; ?></label>
          <input class="ddm-Preferences-input ddm-Preferences-input--tel ddm-Input" type="text" name="<?= $this->e($form['tel']['name']) ?>"/>
        </div>

      </fieldset>

      <fieldset class="ddm-Preferences-fieldset ddm-FormFieldset m-t(m)">

        <div class="ddm-Preferences-formField ddm-Preferences-formField--submit">
          <input class="ddm-Preferences-input ddm-Preferences-input--submit ddm-RectButton ddm-RectButton--brownOnWhite" type="submit" name="<?= $this->e($form['submit']['name']) ?>" value="<?= $this->e($form['submit']['value']) ?>">
        </div>

      </fieldset>

    </form>

  </section>

</section>
