<?php $this->layout('layouts::default') ?>

<section class="ddm-Account ddm-Account--overview ddm-Container">

  <?php
  $this->insert('components::account-menu', [
    'title' => $account_menu['title'],
    'items' => $account_menu['items'],
    'active' => 'account-menu-1',
  ])
  ?>

  <?php
  $this->insert('components::section-header', [
    'title' => $title,
    'classes' => ['m-t(l)'],
    'variants' => ['small', 'left']
  ])
  ?>

  <section class="ddm-Account-section ddm-Account-section--orders m-t(m)">

    <?php
    foreach ($orders_table['body'] as &$row):
      foreach ($row as $key => &$cell):
        switch($key):
          case 4: $this->capture(true); ?>
            <a class="ddm-Account-orderLink ddm-RectButton ddm-RectButton--low ddm-RectButton--brownOnWhite" href="<?= $this->e($cell['href']) ?>" title="<?= $this->e($cell['title']) ?>">
              <?= $this->e($cell['text']) ?>
            </a>
            <?php $cell = $this->capture(false);
          break;
          default: $this->capture(true); ?>
            <div class="ddm-Account-order<?php echo ucfirst($key); ?>"><?= $this->e($cell) ?></div>
            <?php $cell = $this->capture(false);
          break;
        endswitch;
      endforeach;
    endforeach;

    $this->insert('components::table', [
      'table' => $orders_table,
      'variants' => ['orders']
    ])
    ?>
  </section>

</section>
