<?php $this->layout('layouts::default') ?>

<div class="ddm-Stores ddm-Container m-t(s)">

  <?php for ($i = 0; $i < count($stores); $i++): ?>

    <?php $this->insert('components::store', [
      'images' => $stores[$i]['images'],
      'name' => $stores[$i]['name'],
      'address' => $stores[$i]['address'],
      'description' => $stores[$i]['description']
    ]) ?>

  <?php endfor; ?>

</div>
