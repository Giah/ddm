<?php $this->layout('layouts::default') ?>

<section class="ddm-Container ddm-Container--fsw m-t(m)" is="Appear">

  <div class="ddm-SplitVertical">

    <div class="ddm-SplitVertical-col ddm-SplitVertical-col--left">

      <?php $this->insert('components::image-strip', [
        'map' => array_reduce($variants, function($carry, $item) {
          $carry = array_merge($carry, array_fill(0, count($item['thumbnails']), $item['name']));
          return $carry;
        }, []),
        'thumbnails' => array_reduce($variants, function($carry, $item) {
          return array_merge($carry, $item['thumbnails']);
        }, []),
        'images' => $images,
        'attributes' => ['data-selected' => $variants[0]['name']],
      ]) ?>

    </div>

    <div class="ddm-SplitVertical-col ddm-SplitVertical-col--right ddm-Container">

      <?php $this->insert('components::product-form', [
        'name' => $name,
        'currency' => $currency,
        'price' => $price,
        'variants_label' => $variants_label,
        'variants' => $variants,
        'materials_guide_link' => $materials_guide_link,
        'sizes_label' => $sizes_label,
        'sizes' => $sizes,
        'quantity_label' => $quantity_label,
        'sizes_guide_link' => $sizes_guide_link,
        'notes' => $notes,
        'description_label' => $description_label,
        'description_text' => $description_text,
        'description_read_more' => $description_read_more,
        'shipping_link' => $shipping_link,
        'chart_link' => $chart_link,
        'wishlist_link' => $wishlist_link,
        'share_label' => $share_label,
      ]) ?>

    </div>

  </div>

</section>

<section class="ddm-Container m-t(l)" is="Appear">

  <?php $this->insert('components::section-header', [
    'title' => $product_matches_label,
    'sub_title' => '',
    'classes' => ['m-t(l)'],
    'variants' => 'product-line',
  ]) ?>

  <div class="ddm-PreviewBoxes-group">

    <?php
    foreach($product_matches as $item):
      $this->insert('components::preview-box', [
        'image' => $item['image'],
        'image_mobile' => $item['image_mobile'],
        'title' => $item['title'],
        'price' => $item['price'],
        'currency' => $currency,
        'link' => $item['link'],
        'title_size' => 'small',
        'cols' => '4',
        'rounded' => true,
        'buttonCover' => true,
      ]);
    endforeach; ?>

  </div>
</section>

<section class="m-t(l)" is="Appear">
  <?php $this->insert('components::jumbotron', [
    'jumbotron' => $collection_teaser,
    'classes' => ['m-t(l)'],
    'circle' => 'circle',
  ]) ?>
</section>

<section class="ddm-Container m-t(l)" is="Appear">

  <?php $this->insert('components::section-header', [
    'title' => $shop_suggestions_label,
    'sub_title' => '',
    'variants' => 'product',
  ])   ?>

  <div class="ddm-PreviewBoxes-group">

    <?php
    foreach($shop_suggestions as $item):
      $this->insert('components::preview-box', [
        'image' => $item['image'],
        'image_mobile' => $item['image_mobile'],
        'title' => $item['title'],
        'price' => $item['price'],
        'currency' => $currency,
        'link' => $item['link'],
        'title_size' => 'small',
        'buttonCover' => true,
        'cols' => '4',
      ]);
    endforeach; ?>

  </div>
</section>



<div class="ddm-ProductModal" is="ProductModal">

  <div class="ddm-ProductModal-overlay"></div>

  <div class="ddm-ProductModal-content">

    <div class="ddm-ProductModal-closeButton" data-hook="close-Xt4f">
      <span class="ddm-ProductModal-closeButtonIcon ddm-Cross ddm-Icon"><svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#cross"></use></svg></span>
    </div>

    <div class="ddm-ProductModal-contentInner">

      <div class="ddm-ProductModal-item">
        <p class="ddm-ProductModal-label"><?= $product_modal_labels['product-name'] ?></p>
        <p class="ddm-ProductModal-text ddm-ProductModal-text--name" data-hook="name-Xt4f"></p>
      </div>

      <div class="ddm-ProductModal-item">
        <p class="ddm-ProductModal-label"><?= $product_modal_labels['product-metal'] ?></p>
        <p class="ddm-ProductModal-text ddm-ProductModal-text--metal" data-hook="metal-Xt4f"></p>
      </div>

      <div class="ddm-ProductModal-item">
        <p class="ddm-ProductModal-label"><?= $product_modal_labels['product-stone'] ?></p>
        <p class="ddm-ProductModal-text ddm-ProductModal-text--stone" data-hook="stone-Xt4f"></p>
      </div>

      <?php $this->insert('components::rect-button', [
        'classes'=>[
          'ddm-ProductModal-buyButton',
          'ddm-RectButton--brownOnWhite',
        ],
        'link' => $buy_link
      ]) ?>

      <div class="ddm-ProductModal-closeLink" data-hook="close-Xt4f">
        <?= $product_modal_labels['close'] ?>
      </div>

    </div>
  </div>

</div>
