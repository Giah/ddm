<?php $this->layout('layouts::default') ?>

<?php $this->insert('components::jumbotron', [ 'centered' => 'centered', ]) ?>

<section class="ddm-Shop ddm-Container" is="Appear">

  <div class="ddm-Shop-header">

    <div class="ddm-Shop-col ddm-Shop-col--left">

      <div class="ddm-Shop-headerTitle" is="ShopFiltersMenuButton">
        <?= $filter_title ?>
        <span class="ddm-Shop-openIcon ddm-ArrowDown ddm-Icon"><svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#arrow-down"></use></svg></span>
      </div>

    </div>

    <div class="ddm-Shop-col ddm-Shop-col--right">

      <div class="ddm-Shop-categoryCount">
        <?= $products_category_count ?>
        <?= $products_category_count_label ?>
      </div>

      <div class="ddm-Shop-sort">

        <?php if(!empty($sort_title)): ?>

        <label for="Shop-sort" class="ddm-Shop-headerTitle">
          <?= $sort_title ?>
          <span class="ddm-Shop-openIcon ddm-ArrowDown ddm-Icon"><svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#arrow-down"></use></svg></span>
        </label>

        <?php endif; ?>

        <select class="ddm-Shop-sortSelect ddm-Input ddm-Input--pink" name="shop-sort" is="SelectInput">
          <?php foreach($sort_options as $name => $value): ?>

          <option value="<?= $this->e($name) ?>"><?= $value ?></option>

          <?php endforeach; ?>
        </select>

      </div>

    </div>

  </div>

  <div class="ddm-Shop-body">

    <div class="ddm-Shop-col ddm-Shop-col--left" is="ShopFiltersMenu">

      <!--========================
       FILTERS
      ========================-->
      <?php $this->insert('components::accordion', [
        'attributes' => 'data-opened="0"',
        'sections' => array_map(function($item) {
          return [
            'title' => '
              <div class="ddm-Shop-filtersSection">'
                . $item['title'] .
                '<span class="ddm-Accordion-closeIcon ddm-Cross ddm-Icon"><svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#cross"></use></svg></span>
                <span class="ddm-Accordion-openIcon ddm-ArrowDown ddm-Icon"><svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#arrow-down"></use></svg></span>
              </div>',
            'content' => $this->fetch('components::shop-filters', ['items' => $item['items']]),
          ];
        }, $filters)
      ]) ?>

    </div>

    <div class="ddm-Shop-col ddm-Shop-col--right">

      <div class="ddm-Shop-list">

        <?php
        foreach($products as $product):
          $this->insert('components::shop-item', [
            'currency' => $currency,
            'image' => $product['image'],
            'image_mobile' => $product['image_mobile'],
            'title' => $product['title'],
            'price' => $product['price'],
            'info' => $product['info'],
            'link' => $product['link'],
            'wishlist_link' => $product['wishlist_link'],
          ]);
        endforeach; ?>

      </div>

      <a class="ddm-Shop-button ddm-RectButton ddm-RectButton--brownOnWhite m-t(l)"
         href="<?= $this->e($load_more_link['href']) ?>"
         title="<?= $this->e($load_more_link['title']) ?>"
      >
        <?= $load_more_link['text'] ?>
      </a>

    </div>

  </div>

</section>
