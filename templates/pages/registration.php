<?php $this->layout('layouts::default') ?>

<section class="ddm-Registration ddm-Container" is="Appear">

  <?php $this->insert('components::section-header', [
    'title' => $title,
    'sub_title' => $sub_title,
    'classes' => ['m-t(l)'],
    'variants' => ['small'],
  ]) ?>

  <form class="ddm-Registration-form m-t(l)" action="<?= $this->e($form['action']) ?>" method="get">

    <div class="ddm-Registration-formField ddm-Registration-formField--name">
      <label class="ddm-Registration-formLabel ddm-Registration-formLabel--name ddm-FormLabel" for="<?= $this->e($form['name']['name']) ?>"><?= $form['name']['label'] ?></label>
      <input class="ddm-Registration-input ddm-Registration-input--name ddm-Input ddm-Input--white" type="text" name="<?= $this->e($form['name']['name']) ?>"/>
    </div>

    <div class="ddm-Registration-formField ddm-Registration-formField--surname">
      <label class="ddm-Registration-formLabel ddm-Registration-formLabel--surname ddm-FormLabel" for="<?= $this->e($form['surname']['name']) ?>"><?= $form['surname']['label'] ?></label>
      <input class="ddm-Registration-input ddm-Registration-input--surname ddm-Input ddm-Input--white" type="text" name="<?= $this->e($form['surname']['name']) ?>"/>
    </div>

    <div class="ddm-Registration-formField ddm-Registration-formField--email">
      <label class="ddm-Registration-formLabel ddm-Registration-formLabel--email ddm-FormLabel" for="<?= $this->e($form['email']['name']) ?>"><?= $form['email']['label'] ?></label>
      <input class="ddm-Registration-input ddm-Registration-input--email ddm-Input ddm-Input--white" type="text" name="<?= $this->e($form['email']['name']) ?>"/>
    </div>

    <div class="ddm-Registration-formField ddm-Registration-formField--password">
      <label class="ddm-Registration-formLabel ddm-Registration-formLabel--password ddm-FormLabel" for="<?= $this->e($form['password']['name']) ?>"><?= $form['password']['label'] ?></label>
      <input class="ddm-Registration-input ddm-Registration-input--password ddm-Input ddm-Input--white" type="password" name="<?= $this->e($form['password']['name']) ?>"/>
    </div>

    <div class="ddm-Registration-formField ddm-Registration-formField--gender">
      <label class="ddm-Registration-formLabel ddm-Registration-formLabel--gender ddm-FormLabel"><?= $form['gender']['label'] ?></label>
      <label class="ddm-Registration-formLabel ddm-Registration-formLabel--gender-1 ddm-FormLabel ddm-FormLabel--inline" for="<?= $this->e($form['gender'][0]['name']) ?>">
        <input class="ddm-Registration-input ddm-Registration-input--gender-1 ddm-Input" type="radio" name="<?= $this->e($form['gender'][0]['name']) ?>"/>
        <?= $form['gender'][0]['label'] ?>
      </label>
      <label class="ddm-Registration-formLabel ddm-Registration-formLabel--gender-2 ddm-FormLabel ddm-FormLabel--inline" for="<?= $this->e($form['gender'][1]['name']) ?>">
        <input class="ddm-Registration-input ddm-Registration-input--gender-2 ddm-Input" type="radio" name="<?= $this->e($form['gender'][1]['name']) ?>"/>
        <?= $form['gender'][1]['label'] ?>
      </label>
    </div>

    <div class="ddm-Registration-formField ddm-Registration-formField--day">
      <label class="ddm-Registration-formLabel ddm-Registration-formLabel--day ddm-FormLabel"><?= $form['date']['day']['label'] ?></label>
      <select class="ddm-Registration-input ddm-Registration-input--day ddm-Input ddm-Input--white">
        <?php foreach($form['date']['day']['options'] as $option): ?>
          <option <?= $this->attributes([
              'value' => $option['value'],
              'selected' => (!empty($option['selected']) ? 'selected' : ''),
            ]);
          ?>>
            <?= $option['text']; ?>
          </option>
        <?php endforeach; ?>
      </select>
    </div>
    <div class="ddm-Registration-formField ddm-Registration-formField--month">
      <label class="ddm-Registration-formLabel ddm-Registration-formLabel--month ddm-FormLabel"><?= $form['date']['month']['label'] ?></label>
      <select class="ddm-Registration-input ddm-Registration-input--month ddm-Input ddm-Input--white">
        <?php foreach($form['date']['month']['options'] as $option): ?>
          <option <?= $this->attributes([
              'value' => $option['value'],
              'selected' => (!empty($option['selected']) ? 'selected' : ''),
            ]);
          ?>>
            <?= $option['text'] ?>
          </option>
        <?php endforeach; ?>
      </select>
    </div>

    <div class="ddm-Registration-formField ddm-Registration-formField--privacy">
      <label class="ddm-Registration-formLabel ddm-Registration-formLabel--privacy ddm-FormLabel ddm-FormLabel--inline" for="<?= $this->e($form['privacy']['name']) ?>">
        <input class="ddm-Registration-input ddm-Registration-input--privacy ddm-Input" type="checkbox" name="<?= $this->e($form['privacy']['name']) ?>"/>
        <?= $form['privacy']['label'] ?>
      </label>
    </div>

    <div class="ddm-Registration-formField ddm-Registration-formField--submit">
      <input class="ddm-Registration-input ddm-Registration-input--submit ddm-RectButton ddm-RectButton--brownOnWhite" type="submit" name="submit" value="<?= $this->e($form['submit']['value']) ?>">
    </div>
  </form>

</section>
