<?php $this->layout('layouts::default') ?>

<?php $this->insert('components::jumbotron', [ 'centered' => 'centered', ]) ?>

<section class="ddm-PreviewBoxes ddm-Container m-t(l)@desktop-only" is="Appear">
  <div class="ddm-PreviewBoxes-group">

    <?php
    foreach($news as $item):
      $this->insert('components::preview-box', [
        'image' => $item['image'],
        'image_mobile' => $item['image_mobile'],
        'category' => $item['category'],
        'date' => $item['date'],
        'title' => $item['title'],
        'sub_title' => $item['sub_title'],
        'link' => $item['link'],
        'buttonCover' => true,
        'cols' => '3',
      ]);
    endforeach; ?>

  </div>
</section>
