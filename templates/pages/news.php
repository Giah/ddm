<?php $this->layout('layouts::default') ?>

<section class="ddm-Container ddm-Container--fsw m-t(m)" is="Appear">

  <div class="ddm-SplitVertical">

    <div class="ddm-SplitVertical-col ddm-SplitVertical-col--left">

      <?php $this->insert('components::image-strip', [ 'images' => $images ]) ?>

    </div>

    <article class="ddm-SplitVertical-col ddm-SplitVertical-col--right">

      <?php if (!empty($category) && !empty($date)): ?>
        <div class="ddm-MetaInfo">
          <span class="ddm-MetaInfo-category"><?= $category ?></span>
          <span class="ddm-MetaInfo-date"><?= $date ?></span>
        </div>
      <?php endif; ?>

      <div class="m-t(s) p(s)">

        <?php $this->insert('components::section-header', [
          'title' => $title,
          'sub_title' => $sub_title,
          'variants' => ['news', 'left']
        ]) ?>

        <?php if(!empty($content)): ?>
          <div class="ddm-NewsContent ddm-FormattedHTML">
            <?= $content ?>
          </div>
        <?php endif; ?>

        <?php if(!empty($link)): ?>

        <?php endif; ?>

      </div>

    </article>

  </div>

</section>
