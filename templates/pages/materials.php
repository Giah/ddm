<?php $this->layout('layouts::default') ?>

<?php $this->insert('components::jumbotron', [ 'circle' => 'circle', 'centered' => 'centered' ]) ?>


<section class="ddm-Materials ddm-Materials--narrow ddm-Container" is="Appear">

  <p class="ddm-Materials-disclaimer m-t(l)">
    <?= $disclaimer ?>
  </p>

  <div class="ddm-ExpandingBoxContainer m-t(l)" is="ExpandingBoxContainer">
    <?php
    foreach ($materials_list as $item):
      $this->insert('components::expanding-box', [
        'image' => $item['image'],
        'title' => $item['title'],
        'link' => $item['link'],
        'title_size' => '',
      ]);
    endforeach; ?>
  </div>
</section>
