<?php $this->layout('layouts::default') ?>

<?php $this->insert('components::jumbotron', [
  'circle' => 'circle',
  'centered' => 'centered',
]) ?>

<section class="ddm-Container" is="Appear">

  <?php $this->insert('components::section-header', [
    'title' => $title,
    'sub_title' => $sub_title,
    'classes' => ['m-t(l)'],
    'variants' => 'small',
  ]) ?>

  <div class="m-t(l)">
    <?php $this->insert('components::timeline') ?>
  </div>

</section>
