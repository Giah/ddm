<?php $this->layout('layouts::default') ?>

<section class="ddm-Simple ddm-Container" is="Appear">

  <article class="ddm-FormattedHTML"><?= $content ?></article>

</section>
