<?php $this->layout('layouts::default') ?>

<section class="ddm-Account ddm-Account--wishlist ddm-Container">

  <?php
  $this->insert('components::account-menu', [
    'title' => $account_menu['title'],
    'items' => $account_menu['items'],
    'active' => 'account-menu-1',
  ])
  ?>

  <?php
  $this->insert('components::section-header', [
    'title' => $title,
    'sub_title' => $sub_title,
    'classes' => ['m-t(m)'],
    'variants' => ['close', 'small', 'left']
  ])
  ?>

  <div class="m-t(l)">

    <div class="ddm-Account-wishlist">

      <?php
      foreach ($wishlist_table['body'] as &$row):
        foreach ($row as $key => &$cell):
          switch($key):
            case 0: $this->capture(true); ?>

              <div class="ddm-Account-productImage ddm-Account-productImage--big">
                <picture>
                  <source media="(max-width: 1023px)" srcset="<?= $this->e($cell['image_mobile']['src']) ?>" alt="<?= $this->e($cell['image_mobile']['alt']) ?>">
                  <source media="(min-width: 1024px)" srcset="<?= $this->e($cell['image']['src']) ?>" alt="<?= $this->e($cell['image']['alt']) ?>">
                  <img src="<?= $this->e($cell['image']['src']) ?>" alt="<?= $this->e($cell['image']['alt']) ?>">
                </picture>
              </div>

            <?php $cell = $this->capture(false);
            break;
            case 1: $this->capture(true); ?>

              <div class="ddm-Account-productDetails">
                <div class="ddm-Account-productName"><?= $cell['name']; ?></div>
                <?= $cell['details']; ?>
              </div>

            <?php $cell = $this->capture(false);
            break;
            case 2: $this->capture(true); ?>

              <a class="ddm-Account-addToChart ddm-RectButton ddm-RectButton--low ddm-RectButton--brownOnWhite" href="<?= $this->e($cell['add_to_chart']['href']) ?>" title="<?= $this->e($cell['add_to_chart']['title']) ?>">
                <?= $cell['add_to_chart']['text']; ?>
              </a>
              <a class="ddm-Account-wishlistRemove ddm-ButtonRemove" href="<?= $this->e($cell['remove']['href']) ?>" title="<?= $this->e($cell['remove']['title']) ?>">
                <span class="ddm-ButtonRemove-icon ddm-Icon">
                  <svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#bin"></use></svg>
                </span>
                <?= $cell['remove']['text']; ?>
              </a>

            <?php $cell = $this->capture(false);
            break;
          endswitch;
        endforeach;
      endforeach;

      $this->insert('components::table', [
        'table' => $wishlist_table,
        'variants' => ['wishlist']
      ])
      ?>
    </div>

    <div class="ddm-Account-share">
      <?php
      $this->insert('components::share', [
        'text' => $share_label
      ])
      ?>
    </div>

  </div>

</section>
