<?php $this->layout('layouts::default') ?>

<?php $this->insert('components::jumbotron', ['centered' => 'centered']) ?>

<section class="ddm-RevealBoxes ddm-Container" is="Appear">

  <?php $this->insert('components::section-header', [
    'title' => $title,
    'sub_title' => $sub_title,
    'classes' => ['m-t(l)'],
  ]) ?>

  <div class="ddm-RevealBoxes-group m-t(l)">
    <?php $size = [ 'wide', 'narrow', 'narrow', 'wide' ]; ?>
    <?php for($i = 0; $i < count($reveal_boxes); $i++): ?>
      <?php $this->insert('components::reveal-box', [
        'image' => $reveal_boxes[$i]['image'],
        'image_mobile' => $reveal_boxes[$i]['image_mobile'],
        'title' => $reveal_boxes[$i]['title'],
        'sub_title' => $reveal_boxes[$i]['sub_title'],
        'link' => $reveal_boxes[$i]['link'],
        // Cycle trough box dimensions (left to right, top to bottom)
        'size' => $size[$i]
      ]) ?>
    <?php endfor; ?>
  </div>

</section>

<section class="ddm-PreviewBoxes ddm-Container m-t(l)" is="Appear">
  <div class="ddm-PreviewBoxes-group">
    <?php foreach($preview_boxes as $item): ?>
      <?php $this->insert('components::preview-box', [
        'image' => $item['image'],
        'image_mobile' => $item['image_mobile'],
        'title' => $item['title'],
        'sub_title' => $item['sub_title'],
        'link' => $item['link'],
        'cols' => '4',
      ]) ?>
    <?php endforeach; ?>
  </div>
</section>

<section class="ddm-Materials ddm-Materials--home ddm-Container m-t(l)" is="Appear">

  <?php $this->insert('components::section-header', [
    'title' => $materials['title'],
    'sub_title' => $materials['sub_title'],
    'classes' => ['m-t(l)'],
    'variants' => ['white']
  ]) ?>

  <div class="ddm-ExpandingBoxContainer ddm-ExpandingBoxContainer--cropped m-t(l)" is="ExpandingBoxContainer">
    <?php foreach ($materials_list as $item): ?>
      <?php $this->insert('components::expanding-box', [
        'image' => $item['image'],
        'title' => $item['title'],
        'link' => $item['link'],
        'title_size' => '',
      ]) ?>
    <?php endforeach ?>
  </div>

  <?php
  if (isset($materials['link'])): ?>
    <?php $this->insert('components::rect-button', [
      'classes'=>[
        'ddm-Materials-button',
        "ddm-RectButton--transparent",
      ],
      'link' => $materials['link']
    ]) ?>
  <?php endif; ?>

</section>
