<?php $this->layout('layouts::default') ?>

<?php $this->insert('components::jumbotron', [ 'circle' => 'circle', ]) ?>

<section class="ddm-RevealBoxes ddm-Container" is="Appear">

  <?php $this->insert('components::section-header', [
    'title' => $title,
    'sub_title' => $sub_title,
    'classes' => ['m-t(l)'],
    'variants' => 'small'
  ]) ?>

  <div class="ddm-RevealBoxes-group m-t(l)">

    <?php
    $sizes = [ 'wide', 'narrow', 'narrow', 'wide' ];

    for($i = 0; $i < count($reveal_boxes); $i++):
      $item = $reveal_boxes[$i];

      $this->insert('components::reveal-box', [
        'image' => $item['image'],
        'image_mobile' => $item['image_mobile'],
        'title' => $item['title'],
        'sub_title' => $item['sub_title'],
        'link' => $item['link'],
        // Cycle trough box dimensions (left to right, top to bottom)
        'size' => $sizes[$i]
      ]);
    endfor; ?>

  </div>

</section>

<section class="ddm-PreviewBoxes ddm-Container m-t(l)" is="Appear">
  <div class="ddm-PreviewBoxes-group">

    <?php
    foreach($preview_boxes as $item):

      $this->insert('components::preview-box', [
        'image' => $item['image'],
        'image_mobile' => $item['image_mobile'],
        'title' => $item['title'],
        'sub_title' => $item['sub_title'],
        'link' => $item['link'],
        'cols' => '4',
        'title_size' => 'small',
      ]);

    endforeach; ?>

  </div>

</section>
