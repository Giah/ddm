<?php $this->layout('layouts::default') ?>

<section class="ddm-Account ddm-Account--overview ddm-Container">

  <?php
  $this->insert('components::account-menu', [
    'title' => $account_menu['title'],
    'items' => $account_menu['items'],
    'active' => 'account-menu-1',
  ])
  ?>

  <?php
  $this->insert('components::section-header', [
    'title' => $title,
    'sub_title' => $sub_title,
    'classes' => ['m-t(l)'],
    'variants' => ['small', 'left']
  ])
  ?>

  <section class="ddm-Account-section ddm-Account-section--registration">

    <?php
    $this->insert('components::section-header', [
      'title' => $registration_header,
      'sub_title' => '',
      'classes' => ['m-t(l)'],
      'variants' => ['account', 'left']
    ])
    ?>

    <div class="m-t(m)">
      <?php foreach($registration_data as $data): ?>
        <p class="ddm-Account-registrationData">
          <strong><?= $data['label'] ?></strong>
          <span><?= $data['content'] ?></span>
        </p>
      <?php endforeach ?>
    </div>
    <a class="ddm-Account-editRegistration ddm-Account-button ddm-RectButton ddm-RectButton--low ddm-RectButton--brownOnWhite" href=<?= $this->e($edit_registration_link['href']) ?> title=<?= $this->e($edit_registration_link['title']) ?>>
      <?= $edit_registration_link['text'] ?>
    </a>
  </section>

  <section class="ddm-Account-section ddm-Account-section--orders">

    <?php
    $this->insert('components::section-header', [
      'title' => $orders_header,
      'sub_title' => '',
      'classes' => ['m-t(l)'],
      'variants' => ['account', 'left']
    ])
    ?>

    <div class="m-t(m)">
      <?php
      foreach ($orders_table['body'] as &$row):
        foreach ($row as $key => &$cell):
          switch($key):
            case 4: $this->capture(true) ?>
              <a class="ddm-Account-orderLink ddm-RectButton ddm-RectButton--low ddm-RectButton--brownOnWhite" href=<?= $this->e($cell['href']) ?> title=<?= $this->e($cell['title']) ?>>
                <?= $cell['text'] ?>
              </a>
              <?php $cell = $this->capture(false);
            break;
            default: $this->capture(true) ?>
              <div class="ddm-Account-order<?php echo ucfirst($key) ?>"><?= $cell ?></div>
              <?php $cell = $this->capture(false);
            break;
          endswitch;
        endforeach;
      endforeach;

      $this->insert('components::table', [
        'table' => $orders_table,
        'variants' => ['orders']
      ])
      ?>

      <a class="ddm-Account-showOrders ddm-Account-button ddm-RectButton ddm-RectButton--low ddm-RectButton--brownOnWhite" href=<?= $this->e($show_orders_link['href']) ?> title=<?= $this->e($show_orders_link['title']) ?>>
        <?= $show_orders_link['text'] ?>
      </a>
    </div>
  </section>

  <section class="ddm-Account-section ddm-Account-section--wishlist">

    <?php
    $this->insert('components::section-header', [
      'title' => $wishlist_header['title'],
      'sub_title' => $wishlist_header['sub_title'],
      'classes' => ['m-t(l)'],
      'variants' => ['account', 'left']
    ])
    ?>

    <div class="m-t(m)">
      <div class="ddm-Account-wishlist ddm-PreviewBoxes-group">
        <?php
        for($i = 0; $i < 3; $i++):
          $this->insert('components::preview-box', [
            'image' => $wishlist[$i]['image'],
            'image_mobile' => $wishlist[$i]['image_mobile'],
            'title' => $wishlist[$i]['title'],
            'sub_title' => '',
            'price' => $wishlist[$i]['price'],
            'currency' => $currency,
            'link' => $wishlist[$i]['link'],
            'title_size' => 'small',
            'cols' => '3',
            'rounded' => true,
          ]);
        endfor; ?>
      </div>
      <a class="ddm-Account-showWishlist ddm-Account-button ddm-RectButton ddm-RectButton--low ddm-RectButton--brownOnWhite" href=<?= $this->e($show_wishlist_link['href']) ?> title=<?= $this->e($show_wishlist_link['title']) ?>>
        <?= $show_wishlist_link['text'] ?>
      </a>
      <div class="ddm-Account-share">
        <?php
        $this->insert('components::share', [
          'text' => $share_label
        ])
        ?>
      </div>
    </div>
  </section>

</section>
