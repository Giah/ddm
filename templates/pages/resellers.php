<?php $this->layout('layouts::default') ?>

<?php $this->insert('components::jumbotron', [ 'centered' => 'centered' ]) ?>

<section class="ddm-Resellers ddm-Container" is="Appear">

  <?php $this->insert('components::section-header', [
    'title' => $title,
    'sub_title' => $sub_title,
    'classes' => ['m-t(l)'],
    'variants' => ['inverted']
  ]) ?>

  <form class="ddm-Resellers-form m-t(l)" action="<?= $this->e($form['action']) ?>" method="get">

    <select class="ddm-Resellers-select ddm-Input ddm-Input--white">
      <?php foreach($form['cities'] as $option): ?>
        <option <?= $this->attributes([
            'value' => $option['value'],
            'selected' => (!empty($option['selected']) ? 'selected' : ''),
          ]);
        ?>>
          <?php echo  $option['text']; ?>
        </option>
      <?php endforeach; ?>
    </select>

    <input class="ddm-Resellers-submit ddm-RectButton ddm-RectButton--brownOnWhite" type="submit" value="<?= $this->e($form['submit']['value']) ?>">

  </form>


  <div class="ddm-Resellers-searchResults m-t(l)" is="Appear">

    <?php
    foreach($results as $result):
      $this->insert('components::reseller', [
        'name' => $result['name'],
        'address' => $result['address'],
        'cap' => $result['cap'],
        'city' => $result['city'],
        'prov' => $result['prov'],
        'tel' => $result['tel'],
      ]);
    endforeach; ?>

  </div>
</section>
