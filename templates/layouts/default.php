<!DOCTYPE html>
<html class="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title></title>

  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="<?= $this->asset($paths['stylesheets'] . "style.css"); ?>" media="screen"/>

  <script>
    var prefix = 'ddm';
  </script>
  <script src="<?= $paths['public'] . '/' . 'dom4.js'; ?>" async defer>/* DOM4: https://github.com/WebReflection/dom4 */</script>
  <script src="<?= $this->asset($paths['javascripts'] . 'app.js'); ?>" charset="utf-8" async defer></script>

  <?= $head; ?>

</head>

<body class="<?= $this->e($template); ?>">

<div style="position:absolute;left:-1px;top:-1px;width:0;height:0;visibility:hidden;">
  <?= file_get_contents($this->asset($paths['images'] . 'icons.svg')); ?>
</div>

<div class="ddm-MainWrapper ddm-Wrapper" id="ddm-MainWrapper">

  <?php $this->insert('components::main-header'); ?>

  <main class="ddm-MainContainer ddm-Wrapper">
    <?= $this->section('content') ?>
  </main>

  <?php if($template !== 'shop-emotional'): ?>

    <footer class="ddm-MainFooter ddm-Wrapper">
      <?php $this->insert('components::footer-boxes') ?>
      <?php $this->insert('components::footer-bar') ?>
    </footer>

  <?php endif; ?>

</div> <!-- # .ddm-MainWrapper -->

<div class="ddm-isHidden" id="ddm-mediaBreakpoints" class='ddm-mediaBreakpoints'>
  <div class="ddm-mediaBreakpoints-item ddm-mediaBreakpoints-item-smartphone" data-breakpoint-name="smartphone"></div>
  <div class="ddm-mediaBreakpoints-item ddm-mediaBreakpoints-item-tablet" data-breakpoint-name="tablet"></div>
  <div class="ddm-mediaBreakpoints-item ddm-mediaBreakpoints-item-desktop" data-breakpoint-name="desktop"></div>
</div>

</body>
</html>
