<?= $this->mark(true) ?>

<div class="ddm-Timeline ddm-Container">
  <div class="ddm-Timeline-items">

    <?php foreach($timeline as $i => $item): ?>

      <div class="ddm-Timeline-item ddm-Timeline-item--odd">

        <p class="ddm-Timeline-date ddm-Timeline-date--top" aria-hidden="true">
          <?= $item['date'] ?>
        </p>

        <div class="ddm-Timeline-image">
          <picture>
            <source media="(max-width: 767px)" srcset="<?= $this->e($item['image_mobile']['src']) ?>" alt="<?= $this->e($item['image_mobile']['alt']) ?>">
            <source media="(min-width: 768px)" srcset="<?= $this->e($item['image']['src']) ?>" alt="<?= $this->e($item['image']['alt']) ?>">
            <img src="<?= $this->e($item['image']['src']) ?>" alt="<?= $this->e($item['image']['alt']) ?>">
          </picture>
        </div>
      </div>

      <div class="ddm-Timeline-item ddm-Timeline-item--even">
        <div class="ddm-Timeline-itemInner">
          <div class="ddm-Timeline-content">
            <p class="ddm-Timeline-date">
              <?= $item['date'] ?>
              <span class="ddm-Timeline-pin ddm-Icon"><span></span></span>
            </p>
            <h3 class="ddm-Timeline-title"><?= $item['title'] ?></h3>
            <p class="ddm-Timeline-text"><?= $item['text'] ?></p>
          </div>
        </div>
        <span class="ddm-Timeline-bubbles ddm-Icon">
          <svg xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="#bubbles"></use>
          </svg>
        </span>
      </div>

    <?php endforeach; ?>

  </div>
</div>

<?= $this->mark() ?>
