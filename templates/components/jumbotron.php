<?= $this->mark(true) ?>
<?php
/**
 *
 * @param {array} $jumbotron
 *          {string} $jumbotron.image
 *          {string} $jumbotron.title
 *          {string} [$jumbotron.image_mobile]
 *          {string} [$jumbotron.sub_title]
 *          {string} [$jumbotron.text]
 *          {string} [$jumbotron.link]
 * @param {string} [$circle]
 * @param {string} [$centered]
 *
 */
?>

<div <?= $this->classes([
  'ddm-Container',
  'ddm-Container--fsw',
  'ddm-Jumbotron',
  "ddm-Jumbotron--{$template}"
]);
?>>

  <div <?= $this->classes([
    'ddm-Jumbotron-content',
    "ddm-Jumbotron-content--{$template}",
    "ddm-Jumbotron-content--circle" => !empty($circle),
    "ddm-Jumbotron-content--centered" => !empty($centered),
    ])
  ?>>

    <div <?= $this->classes(['ddm-Jumbotron-contentInner', "ddm-Jumbotron-contentInner--{$template}"]) ?>>

      <p <?= $this->classes(['ddm-Jumbotron-title', "ddm-Jumbotron-title--{$template}"]); ?>>
        <?= $jumbotron['title'] ?>
      </p>

      <?php if (!empty($jumbotron['sub_title'])): ?>
        <p <?= $this->classes(['ddm-Jumbotron-subTitle', "ddm-Jumbotron-subTitle--{$template}"]); ?>>
          <?= $jumbotron['sub_title'] ?>
        </p>
      <?php endif; ?>

      <?php if (!empty($jumbotron['text'])): ?>
        <div <?= $this->classes(['ddm-Jumbotron-text', "ddm-Jumbotron-text--{$template}"]); ?>>
          <?= $jumbotron['text'] ?>
        </div>
      <?php endif ?>

      <?php if (isset($jumbotron['link']['href'])): ?>
        <?php $this->insert('components::rect-button', [
          'classes' => [
            'ddm-Jumbotron-button',
            "ddm-RectButton--transparent",
          ],
          'link' => $jumbotron['link']
        ]); ?>
      <?php endif; ?>

    </div>

  </div>

  <div class="<?= "ddm-Jumbotron-slideshow--{$template}" ?>">
      <?php $this->insert('components::slideshow', [
      'images' => $jumbotron['images']
    ]); ?>
  </div>

</div>

<?= $this->mark() ?>
