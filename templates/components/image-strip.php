<?= $this->mark(true) ?>
<?php
/**
 *
 * @param {array} $variants
 * @param {array} $images
 * @param {array} $thumbnails
 * @param {string} $attributes
 *
 */
?>
<div class="ddm-ImageStrip" is="ImageStrip" <?php if(!empty($attributes)) echo $this->attributes($attributes); ?>>

  <?php if(isset($thumbnails) && count($thumbnails) > 1): ?>

    <div class="ddm-ImageStrip-thumbnails">

      <?php foreach($thumbnails as $key => $thumbnail): if(!empty($thumbnail['src'])): ?>

        <div class="ddm-ImageStrip-thumbnail" <?= $this->attributes(['data-id' => (isset($map) ? $map[$key] : '')]); ?>>
          <img <?= $this->attributes([ 'src' => $thumbnail['src'], 'alt' => $thumbnail['alt'] ]) ?>/>
        </div>

      <?php endif; endforeach; ?>

    </div>
  <?php endif; ?>

  <?php if(isset($images) && count($images) > 1): ?>

    <div <?= $this->classes(['ddm-ImageStrip-images', 'ddm-ImageStrip-images--narrow' => (isset($thumbnails) && count($thumbnails) > 1)]); ?>>

      <?php foreach ($images as $key => $image): if(!empty($image['src'])): ?>

        <div class="ddm-ImageStrip-image">
          <img <?= $this->attributes([ 'src' => $image['src'], 'alt' => $image['alt'] ]) ?>/>
        </div>

      <?php endif; endforeach; ?>

    </div>

  <?php endif; ?>

</div>

<?= $this->mark() ?>
