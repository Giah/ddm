<?= $this->mark(true) ?>

<div class="ddm-MenuService">

  <div class="ddm-MenuService-container ddm-MenuService-container--top ddm-Container">

    <div class="ddm-MenuService-item ddm-MenuService-item--s">
      <span class="ddm-MenuService-itemInner ddm-MenuService-itemInner--first">
        <a href="<?= $this->e($chart_preview['link']['href']) ?>" title="<?= $this->e($chart_preview['link']['title']) ?>">
          <?= $this->e($chart_preview['link']['text']) ?>
        </a>
        <span class="ddm-MenuService-itemsCount"><?= '[' . $chart_preview['items'] . ']' ?></span>
      </span>
      <span class="ddm-MenuService-itemInner">
        <a href="<?= $this->e($wishlist_preview['link']['href']) ?>" title="<?= $this->e($wishlist_preview['link']['title']) ?>">
          <?= $this->e($wishlist_preview['link']['text']) ?>
        </a>
        <span class="ddm-MenuService-itemsCount"><?= '[ ' . $wishlist_preview['items'] . ' ]' ?></span>
      </span>
    </div>

    <div class="ddm-MenuService-item ddm-MenuService-item--d ddm-MenuService-item--o">
      <span class="ddm-MenuService-itemInner">
        <?php $this->insert('components::language-selector') ?>
      </span>
      <span class="ddm-MenuService-itemInner">
        <a href="<?= $this->e($resellers_link['href']) ?>" title="<?= $this->e($resellers_link['title']) ?>">
          <?= $this->e($resellers_link['text']) ?>
        </a>
      </span>
    </div>

    <?php $login_logout_link = false ? $login_link : $logout_link ?>
    <a class="ddm-MenuService-item ddm-MenuService-item--right" href="<?= $this->e($login_logout_link['href']) ?>" title="<?= $this->e($login_logout_link['title']) ?>">
      <span class="ddm-MenuService-itemInner ddm-MenuService-itemInner--first ddm-MenuService-itemInner--last">
        <?= $this->e($login_logout_link['text']) ?>
      </span>
    </a>

  </div>

  <div class="ddm-MenuService-container ddm-MenuService-container--bottom ddm-Container">

    <div class="ddm-MenuService-item ddm-MenuService-item--s">
      <span class="ddm-MenuService-itemInner ddm-MenuService-itemInner--first">
        <?php $this->insert('components::language-selector') ?>
      </span>
      <span class="ddm-MenuService-itemInner">
        <a href="<?= $this->e($resellers_link['href']) ?>" title="<?= $this->e($resellers_link['title']) ?>">
          <?= $this->e($resellers_link['text']) ?>
        </a>
      </span>
    </div>

    <p class="ddm-MenuService-item ddm-MenuService-item--d" id="ddm-PromoShipping">
      <span><?= $this->e($promo_shipping) ?></span>
    </p>

    <div class="ddm-MenuService-item ddm-MenuService-item--right ddm-MenuService-item--d">
      <span class="ddm-MenuService-itemInner">
        <a href="<?= $this->e($chart_preview['link']['href']) ?>" title="<?= $this->e($chart_preview['link']['title']) ?>">
          <?= $this->e($chart_preview['link']['text']) ?>
        </a>
        <span class="ddm-MenuService-itemsCount"><?= '[ ' . $chart_preview['items'] . ' ]' ?></span>
      </span>
      <span class="ddm-MenuService-itemInner">
        <a href="<?= $this->e($wishlist_preview['link']['href']) ?>" title="<?= $this->e($wishlist_preview['link']['title']) ?>">
          <?= $this->e($wishlist_preview['link']['text']) ?>
        </a>
        <span class="ddm-MenuService-itemsCount"><?= '[ ' . $wishlist_preview['items'] . ' ]' ?></span>
      </span>
    </div>

    <div class="ddm-MenuService-item ddm-MenuService-item--right ddm-MenuService-item--search">
      <div class="ddm-MenuService-itemInner ddm-MenuService-itemInner--last">
        <?php $this->insert('components::search-bar') ?>
      </div>
    </div>

  </div>

</div>

<?= $this->mark() ?>
