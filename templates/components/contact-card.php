<?= $this->mark(true) ?>
<?php
/**
 *
 * @param {array} $image - an array of 4 images. First image has the wide format, the other three are squared.
 * @param {string} $name - Name of the store
 * @param {string} $address - Address of the store
 * @param {string} $description - A store description (allows HTML formatting)
 *
 */
?>

<div class="ddm-ContactCard">
  <div class="ddm-ContactCard-inner">

    <?php if(!empty($image['src'])): ?>
      <?php $this->capture(true); ?>
      <img src="<?= $this->e($image['src']) ?>" alt="<?= $this->e($image['alt']) ?>">
      <?php $content = $this->capture(false); ?>

      <?php $this->insert('components::proportional-container', [
        'content' => $content,
        'class' => 'ddm-ContactCard-image'
      ]) ?>
    <?php endif; ?>

    <?php if(!empty($name)): ?>
      <h2 class="ddm-ContactCard-name"><?= $name ?></h2>
    <?php endif; ?>

    <?php if(!empty($address)): ?>
      <div class="ddm-ContactCard-info"><?= $address ?></div>
    <?php endif; ?>

    <?php if(!empty($tel['label'])): ?>
      <div class="ddm-ContactCard-infoLabel"><?= $tel['label'] ?></div>
    <?php endif; ?>

    <?php if(!empty($tel['value'])): ?>
      <div class="ddm-ContactCard-info"><?= $tel['value'] ?></div>
    <?php endif; ?>

    <?php if(!empty($hours['label'])): ?>
      <div class="ddm-ContactCard-infoLabel"><?= $hours['label'] ?></div>
    <?php endif; ?>

    <?php if(!empty($hours['value'])): ?>
      <div class="ddm-ContactCard-info"><?= $hours['value'] ?></div>
    <?php endif; ?>

    <?php if(!empty($closed['label'])): ?>
      <div class="ddm-ContactCard-infoLabel"><?= $closed['label'] ?></div>
    <?php endif; ?>

    <?php if(!empty($closed['value'])): ?>
      <div class="ddm-ContactCard-info"><?= $closed['value'] ?></div>
    <?php endif; ?>

  </div>
</div>

<?= $this->mark() ?>
