<?= $this->mark(true) ?>

<?php
/**
 *
 * @param {array} $images - an array of 4 images. First image has the wide format, the other three are squared.
 * @param {string} $name - Name of the store
 * @param {string} $address - Address of the store
 * @param {string} $description - A store description (allows HTML formatting)
 *
 */
?>

<div class="ddm-Store" is="Appear">

  <div class="ddm-Store-col ddm-Store-col--1">

    <?php if(!empty($images[0]['src'])): ?>

      <?php $this->capture(true); ?>
      <img src="<?= $this->e($images[0]['src']) ?>" alt="<?= $this->e($images[0]['alt']) ?>">
      <?php $content = $this->capture(false); ?>

      <?php $this->insert('components::proportional-container', [
        'content' => $content,
        'class' => 'ddm-Store-image ddm-Store-image--wide'
      ]) ?>

    <?php endif; ?>

  </div>

  <?php for($i = 1; $i < 4; $i++): ?>

    <div class="ddm-Store-col ddm-Store-col--<?= $i + 1 ?>">

      <?php if(!empty($images[$i]['src'])): ?>

        <?php $this->capture(true); ?>
        <img src="<?= $this->e($images[$i]['src']) ?>" alt="<?= $this->e($images[$i]['alt']) ?>">
        <?php $content = $this->capture(false); ?>

        <?php $this->insert('components::proportional-container', [
          'content' => $content,
          'class' => 'ddm-Store-image ddm-Store-image--square'
        ]) ?>

      <?php endif; ?>

    </div>

  <?php endfor; ?>

  <div class="ddm-Store-col ddm-Store-col--5">

    <?php if(!empty($name)): ?>
      <h2 class="ddm-Store-name"><?= $name ?></h2>
    <?php endif; ?>

    <?php if(!empty($address)): ?>
      <h3 class="ddm-Store-address"><?= $address ?></h3>
    <?php endif; ?>

    <?php if(!empty($description)): ?>
      <div class="ddm-Store-description m-t(m) ddm-FormattedHTML"><?= $description ?></div>
    <?php endif; ?>

    <?php if(!empty($tel_label)): ?>
      <div class="ddm-Store-infoLabel"><?= $tel_label ?></div>
    <?php endif; ?>

    <?php if(!empty($tel)): ?>
      <div class="ddm-Store-info"><?= $tel ?></div>
    <?php endif; ?>

    <?php if(!empty($hours_label)): ?>
      <div class="ddm-Store-infoLabel"><?= $hours_label ?></div>
    <?php endif; ?>

    <?php if(!empty($hours)): ?>
      <div class="ddm-Store-info"><?= $hours ?></div>
    <?php endif; ?>

    <?php if(!empty($closed_label)): ?>
      <div class="ddm-Store-infoLabel"><?= $closed_label ?></div>
    <?php endif; ?>

    <?php if(!empty($closed)): ?>
      <div class="ddm-Store-info"><?= $closed ?></div>
    <?php endif; ?>

  </div>

</div>

<?= $this->mark() ?>
