<?= $this->mark(true) ?>
<?php
/**
 *
 * @param {array} $link
 *          {string} $link.href
 *          {string} $link.title
 *          {string} $link.text
 *
 */
?>

<?php if(empty($attributes)) { $attributes = []; } ?>

<a
<?= $this->classes(array_merge($class, ["ddm-LinearButton"])); ?>
<?= $this->attributes(array_merge(['href' => $link['href'], 'title' => $link['title']], $attributes)); ?>
>

  <span class="ddm-LinearButton-borders">
    <span class="ddm-LinearButton-border ddm-LinearButton-border--top"></span>
    <span class="ddm-LinearButton-border ddm-LinearButton-border--right"></span>
    <span class="ddm-LinearButton-border ddm-LinearButton-border--bottom"></span>
    <span class="ddm-LinearButton-border ddm-LinearButton-border--left"></span>
  </span>

  <?php echo !empty($link['text']) ? $link['text'] : ''; ?>

</a>

<?= $this->mark() ?>
