<?= $this->mark(true) ?>

<div class="ddm-Reseller">
  <div class="ddm-Reseller-inner">

    <?php if(!empty($name)): ?>
      <h2 class="ddm-Reseller-data ddm-Reseller-data--name"><?= $name ?></h2>
    <?php endif; ?>

    <?php if(!empty($address)): ?>
      <p class="ddm-Reseller-data ddm-Reseller-data--address">
        <?= $address ?>

        <?php if(!empty($cap)): ?>
          &nbsp;-&nbsp;<?= $cap ?>
        <?php endif; ?>
      </p>
    <?php endif; ?>

    <?php if(!empty($city)): ?>
      <p class="ddm-Reseller-data ddm-Reseller-data--city">
        <?= $city ?>

        <?php if(!empty($prov)): ?>
          (<?= $prov ?>)
        <?php endif; ?>
      </p>
    <?php endif; ?>

    <?php if(!empty($tel)): ?>
      <p class="ddm-Reseller-data ddm-Reseller-data--tel">
        Tel: <?= $tel ?>
      </p>
    <?php endif; ?>

  </div>
</div>

<?= $this->mark() ?>
