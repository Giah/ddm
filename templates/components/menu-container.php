<?= $this->mark(true) ?>

<div
  class="ddm-MenuContainer"
  is="MenuContainer"
  <?= $this->attributes([ 'data-hidden' => $template === 'shop-emotional', ]) ?>
>
  <?php $this->insert('components::menu-service'); ?>
  <?php $this->insert('components::main-navigation'); ?>
</div>

<?= $this->mark() ?>
