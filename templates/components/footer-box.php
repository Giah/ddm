<?= $this->mark(true) ?>
<?php
/**
 *
 * @param {string="big","small"} $params.size
 * @param {string} $params.icon - the icon SVG id
 * @param {string} $params.title - The title printed below the icon
 * @param {string} [$params.link] - A link printed below the title
 * @param {string} [$params.background="dark","darker"] - Only needed in combination with $size="big" -> big & dark OR big & darker
 *
 */
?>

<div
  <?= $this->classes([
    'ddm-FooterBox',
    "ddm-FooterBox--$size",
    "ddm-FooterBox--$background" => $background,
    'ddm-Flip' => !empty($newsletter)])
  ?>
  <?php if(!empty($newsletter)) { echo ' is="Flip"'; } ?>
>

  <div class="ddm-FooterBox-content ddm-Flip-content">

    <div class="ddm-FooterBox-front ddm-Flip-front">

      <div class="ddm-FooterBox-inner">

        <span <?= $this->classes(['ddm-Icon', 'ddm-FooterBox-icon', "ddm-FooterBox-icon--$size"]) ?>>
          <svg xmlns:xlink="http://www.w3.org/1999/xlink">
              <use xlink:href="#<?php echo $icon; ?>"></use>
          </svg>
        </span>

        <p <?= $this->classes(['ddm-FooterBox-title', "ddm-FooterBox-title--$size"]) ?>>
          <?= $this->e($title) ?>
        </p>

        <?php
        if (isset($link)):
          if ($size === 'big'):
            $this->insert('components::linear-button', [
              'class'=>[
                'ddm-FooterBox-button',
                'ddm-LinearButton',
                'ddm-LinearButton--white',
              ],
              'attributes' => [
                'data-hook-flip' => 'true'
              ],
              'link' => $link
            ]);
          else:
            $this->insert('components::link', [
              'class'=>[
                'ddm-FooterBox-button',
                'ddm-Button--invisible',
                'ddm-Button--cover',
              ],
              'link' => $link
            ]);
          endif;
        endif;
        ?>

      </div>

    </div>

    <?php if (!empty($newsletter)): ?>
      <div class="ddm-FooterBox-back ddm-Flip-back">
        <div class="ddm-FooterBox-inner">
          <?php $this->insert('components::newsletter-form', ['form' => $newsletter]) ?>
        </div>
      </div>
    <?php endif; ?>

  </div>

</div>

<?= $this->mark() ?>
