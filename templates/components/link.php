<?= $this->mark(true) ?>
<?php
/**
 *
 * @param {array} $link
 *          {string} $link.href
 *          {string} $link.title
 *          {string} $link.text
 *
 */
?>

<?php if(empty($attributes)) { $attributes = []; } ?>

<a
<?= $this->classes($class); ?>
<?= $this->attributes(array_merge(['href' => $link['href'], 'title' => $link['title']], $attributes)); ?>
>
  <?php echo !empty($link['text']) ? $link['text'] : ''; ?>
</a>

<?= $this->mark() ?>
