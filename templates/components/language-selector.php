<?= $this->mark(true) ?>

<select class="ddm-LanguageSelector ddm-Input ddm-Input--brown" name="site-language" is="SelectInput">
    <option value="it">ITA</option>
    <option value="en">ENG</option>
</select>

<?= $this->mark() ?>
