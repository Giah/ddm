<?= $this->mark(true) ?>

<div class="ddm-FooterBoxes">

    <?php $this->insert('components::footer-box', [
      'title' => $footer['newsletter_title'],
      'link' => $footer['newsletter_link'],
      // Settings for styling pourposes
      'size' => 'big',
      'icon' => 'envelope',
      'background' => 'darker',
      'newsletter' => $footer['newsletter_form']
    ]) ?>

    <?php $this->insert('components::footer-box', [
      'title' => $footer['store_locator_title'],
      'link' => $footer['store_locator_link'],
      // Settings for styling pourposes
      'size' => 'big',
      'icon' => 'map',
      'background' => 'dark',
    ]) ?>

    <?php $this->insert('components::footer-box', [
      'title' => $footer['size_guide_title'],
      'link' => $footer['size_guide_link'],
      // Settings for styling pourposes
      'size' => 'small',
      'icon' => 'sizes',
      'background' => false,
    ]) ?>

    <?php $this->insert('components::footer-box', [
      'title' => $footer['shipping_title'],
      'link' => $footer['shipping_link'],
      // Settings for styling pourposes
      'size' => 'small',
      'icon' => 'truck',
      'background' => false,
    ]) ?>

    <?php $this->insert('components::footer-box', [
      'title' => $footer['return_title'],
      'link' => $footer['return_link'],
      // Settings for styling pourposes
      'size' => 'small',
      'icon' => 'return',
      'background' => false,
    ]) ?>

    <?php $this->insert('components::footer-box', [
      'title' => $footer['payments_title'],
      'link' => $footer['payments_link'],
      // Settings for styling pourposes
      'size' => 'small',
      'icon' => 'credit-card',
      'background' => false,
    ]) ?>

</div>

<?= $this->mark() ?>
