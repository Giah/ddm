<form class="ddm-NewsletterForm" action="<?= $this->e($form['action']) ?>" method="get">

  <div class="ddm-NewsletterForm-formField ddm-NewsletterForm-formField--name">
    <label class="ddm-NewsletterForm-formLabel ddm-NewsletterForm-formLabel--name ddm-FormLabel" for="<?= $this->e($form['name']['name']) ?>"><?= $form['name']['label'] ?></label>
    <input class="ddm-NewsletterForm-input ddm-NewsletterForm-input--name ddm-Input ddm-Input--white" type="text" name="<?= $this->e($form['name']['name']) ?>"/>
  </div>

  <div class="ddm-NewsletterForm-formField ddm-NewsletterForm-formField--email">
    <label class="ddm-NewsletterForm-formLabel ddm-NewsletterForm-formLabel--email ddm-FormLabel" for="<?= $this->e($form['email']['name']) ?>"><?= $form['email']['label'] ?></label>
    <input class="ddm-NewsletterForm-input ddm-NewsletterForm-input--email ddm-Input ddm-Input--white" type="text" name="<?= $this->e($form['email']['name']) ?>"/>
  </div>

  <div class="ddm-NewsletterForm-formField ddm-NewsletterForm-formField--privacy">
    <label class="ddm-NewsletterForm-formLabel ddm-NewsletterForm-formLabel--privacy ddm-FormLabel ddm-FormLabel--inline" for="<?= $this->e($form['privacy']['name']) ?>">
      <input class="ddm-NewsletterForm-input ddm-NewsletterForm-input--privacy ddm-Input" type="checkbox" name="<?= $this->e($form['privacy']['name']) ?>"/>
      <?= $form['privacy']['label'] ?>
    </label>
  </div>

  <div class="ddm-NewsletterForm-formField ddm-NewsletterForm-formField--submit">
    <input class="ddm-NewsletterForm-input ddm-NewsletterForm-input--submit ddm-RectButton ddm-RectButton--brownOnWhite" type="submit" name="submit" value="<?= $this->e($form['submit']['value']) ?>">
  </div>
</form>
