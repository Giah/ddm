<?= $this->mark(true) ?>

<?php
/**
 *
 * @param {string} $content
 * @param {string} $class
 * @param {string} $attributes
 *
 */
?>
<div <?php echo !empty($attributes) ? $attributes : ''; ?> class="ddm-ProportionalContainer <?php echo !empty($class) ? $class : ''; ?>">
  <div class="ddm-ProportionalContainer-ratio">
    <div class="ddm-ProportionalContainer-content">
      <?= $content ?>
    </div>
  </div>
</div>

<?= $this->mark() ?>
