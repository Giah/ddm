<?= $this->mark(true) ?>
<?php
/**
 *
 * @param {string} $title
 * @param {string} $sub_title
 * @param {string|array} [$variants]
 *
 */

$variants = isset($variants) ? (!is_array($variants) ? [$variants] : $variants) : [];
$classes = isset($classes) ? (!is_array($classes) ? [$classes] : $classes) : [];
$attributes = isset($attributes) ? (!is_array($attributes) ? [$attributes] : $attributes) : [];
?>

<header <?= $this->classes(array_merge(['ddm-SectionHeader'], array_map(function($variant) { return "ddm-SectionHeader--$variant"; }, $variants), $classes)); ?> <?= $this->attributes($attributes) ?>>
  <h1 <?= $this->classes(array_merge(['ddm-SectionHeader-title'], array_map(function($variant) { return "ddm-SectionHeader-title--$variant"; }, $variants))); ?>>
    <?= $title ?>
  </h1>

  <?php if(!empty($sub_title)): ?>
    <div <?= $this->classes(array_merge(['ddm-SectionHeader-subTitle'], array_map(function($variant) { return "ddm-SectionHeader-subTitle--$variant"; }, $variants))); ?>>
      <?= $sub_title ?>
    </div>
  <?php endif; ?>

</header>

<?= $this->mark() ?>
