<?= $this->mark(true) ?>
<?php
/**
 *
 * @param {string} $text
 *
 */
?>
<div class="ddm-Share">

  <?php if(!empty($text)): ?>
    <p class="ddm-Share-shareCTA">
      <?= $this->e($text) ?>
    </p>
  <?php endif; ?>

  <a class="ddm-Share-shareButton" target="_blank" href="#">
    <span class="ddm-Share-shareIcon ddm-Icon">
      <svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#facebook-icon"></use></svg>
    </span>
    <span class="ddm-Share-shareIcon ddm-Share-shareIcon--inverted ddm-Icon">
      <svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#facebook-icon-inverted"></use></svg>
    </span>
  </a>

  <a class="ddm-Share-shareButton" target="_blank" href="#">
    <span class="ddm-Share-shareIcon ddm-Icon">
      <svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#instagram-icon"></use></svg>
    </span>
    <span class="ddm-Share-shareIcon ddm-Share-shareIcon--inverted ddm-Icon">
      <svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#instagram-icon-inverted"></use></svg>
    </span>
  </a>

</div>

<?= $this->mark() ?>
