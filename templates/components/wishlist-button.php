<?= $this->mark(true) ?>
<?php
/**
 *
 * @param {array} $link
 *          {string} $link.href
 *          {string} $link.title
 *          {string} $link.text
 *
 */
?>

<a <?php
  echo $this->classes(array_merge($classes, ["ddm-WishlistButton"]));
  echo $this->attributes([ 'href' => $link['href'], 'title' => $link['title'] ]);
?>>

  <span class="ddm-WishlistButton-icon ddm-Icon">
    <svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#heart"></use></svg>
  </span>

  <?= !empty($link['text']) ? $link['text'] : ''; ?>

</a>

<?= $this->mark() ?>
