<?= $this->mark(true) ?>

<?php
/**
 *
 * @param {string} $name
 * @param {string} $currency
 * @param {string} $price
 * @param {string} $variants_label
 * @param {array} $variants
 *          {string} $variants.name
 *          {string} $variants.stone
 *          {string} $variants.metal
 *          {string} $variants.button_image
 *            {string} $variants.button_image.src
 *            {string} $variants.button_image.title
 * @param {array} $materials_guide_link
 *          {string} $materials_guide_link.href
 *          {string} $materials_guide_link.title
 *          {string} $materials_guide_link.text
 * @param {string} $sizes_label
 * @param {array} $sizes
 *          {string} $sizes.value
 *          {string} $sizes.text
 * @param {string} $quantity_label
 * @param {array} $sizes_guide_link
 *          {string} $sizes_guide_link.href
 *          {string} $sizes_guide_link.title
 *          {string} $sizes_guide_link.text
 * @param {string} $notes
 * @param {string} $description_label
 * @param {string} $description_text
 * @param {string} $description_read_more
 * @param {array} $shipping_link
 *          {string} $shipping_link.href
 *          {string} $shipping_link.title
 *          {string} $shipping_link.text
 * @param {array} $chart_link
 *          {string} $chart_link.href
 *          {string} $chart_link.title
 *          {string} $chart_link.text
 * @param {array} $whishlist_link
 *          {string} $whishlist_link.href
 *          {string} $whishlist_link.title
 *          {string} $whishlist_link.text
 * @param {array} $share_label
 *
 */

?>
<form class="ddm-ProductForm ddm-Form" is="ProductForm">

  <!-- Hidden fields -->
  <?php foreach($variants as $variant): ?>
    <input name="<?= $this->e($variant['name']) ?>" type="hidden" value="<?php echo htmlentities(json_encode($variant), ENT_QUOTES, 'UTF-8'); ?>" />
  <?php endforeach; ?>

  <!-- Product name -->
  <?php if (!empty($name)): ?>
    <h2 class="ddm-ProductForm-name">
      <?= $name ?>
    </h2>
  <?php endif; ?>


  <!-- Product price -->
  <?php if(!empty($price)): ?>
    <div class="ddm-ProductForm-price">
      <?= $currency . $price ?>
    </div>
  <?php endif; ?>


  <!-- Product variants buttons -->
  <?php if(!empty($variants_label) && !empty($variants)): ?>

    <div class="ddm-ProductForm-variants">

      <div class="ddm-ProductForm-variantsLabel">
        <?= $variants_label ?>
      </div>

      <div class="ddm-ProductForm-variantsButtons">
        <?php foreach($variants as $variant): ?>
          <div class="ddm-ProductForm-variantButton">
            <?php if(!empty($variant['button_image']['src']) && !empty($variant['name'])): ?>
              <input id="<?= $this->e($variant['name']) ?>" type="radio" name="variant" value="<?= $this->e($variant['name']) ?>" />
              <label
                class="ddm-ProductForm-variantLabel"
                for="<?= $this->e($variant['name']) ?>"
                style="background-image:url('<?= $variant['button_image']['src'] ?>')">
                  <?= $variant['name'] ?>
              </label>
            <?php endif; ?>
          </div>
        <?php endforeach; ?>
      </div>

      <div class="ddm-ProductForm-variantsNames">
        <?php foreach($variants as $variant): ?>
          <div class="ddm-ProductForm-variantName ddm-isHidden">
            <?= $variant['name'] ?>
          </div>
        <?php endforeach; ?>
      </div>

      <?php if(!empty($materials_guide_link)): ?>
        <a class="ddm-ProductForm-materialsGuideLink"
           href="<?= $this->e($materials_guide_link['href']) ?>"
           title="<?= $this->e($materials_guide_link['title']) ?>"
        >
          <?= $materials_guide_link['text'] ?>
        </a>
      <?php endif; ?>

    </div>

  <?php endif; ?>


  <!-- Product size and quantity -->
  <div class="ddm-ProductForm-inputs">

    <?php if(!empty($sizes)): ?>

      <div class="ddm-ProductForm-sizes">

        <label class="ddm-ProductForm-sizesLabel" for="ddm-size">
          <?= $sizes_label ?>
        </label>

        <select name="sizes" id="ddm-size" class="ddm-Input ddm-Input--pink" is="SelectInput">
          <?php foreach($sizes as $size): ?>
            <?php if(!empty($size['value']) && !empty($size['text'])): ?>
              <option value="<?= $this->e($size['value']) ?>"><?= $size['text'] ?></option>
            <?php endif; ?>
          <?php endforeach; ?>
        </select>

      </div>

    <?php endif; ?>

    <div class="ddm-ProductForm-quantity">
      <label class="ddm-ProductForm-quantityLabel" for="ddm-quantity">
        <?= $quantity_label ?>
      </label>
      <input class="ddm-ProductForm-quantityInput ddm-Input ddm-Input--pink" id="ddm-quantity" name="quantity" type="number" min="1" step="1" value="1" />
    </div>


    <div>
      <?php if(!empty($sizes_guide_link)): ?>
        <a class="ddm-ProductForm-sizesGuideLink"
           href="<?= $this->e($sizes_guide_link['href']) ?>"
           title="<?= $this->e($sizes_guide_link['title']) ?>">
           <?= $sizes_guide_link['text'] ?>
        </a>
      <?php endif; ?>
    </div>

  </div>


  <!-- Product notes -->
  <?php if(!empty($notes)): ?>
    <p class="ddm-ProductForm-notes">
      <?= $notes ?>
    </p>
  <?php endif; ?>


  <!-- Product description -->
  <div class="ddm-ProductForm-description">

    <?php $this->capture(true); ?>
    <div class="ddm-ProductForm-descriptionLabel ddm-Input ddm-Input--pink">
      <?= $description_label ?>
      <span class="ddm-Accordion-closeIcon ddm-Cross ddm-Icon"><svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#cross"></use></svg></span>
      <span class="ddm-Accordion-openIcon ddm-ArrowDown ddm-Icon"><svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#arrow-down"></use></svg></span>
      <?php if(!empty($description_read_more)): ?>
        <span class="ddm-ProductForm-descriptionReadMore"><?= $description_read_more ?></span>
      <?php endif; ?>
    </div>
    <?php $accordion_title = $this->capture(false); ?>

    <?php $this->capture(true); ?>
    <div class="ddm-ProductForm-descriptionText">
      <?= $description_text ?>
    </div>
    <?php   $accordion_content = $this->capture(false); ?>

    <?php $this->insert('components::accordion', [
    'sections' => [[
      'title' => $accordion_title,
      'content' => $accordion_content
      ]]
    ]) ?>
  </div>


  <!-- Shipping info link -->
  <?php if(!empty($shipping_link)): ?>
    <a class="ddm-ProductForm-shippingLink"
       href="<?= $this->e($shipping_link['href']) ?>"
       title="<?= $this->e($shipping_link['title']) ?>">
       <?= $shipping_link['text'] ?>
    </a>
  <?php endif; ?>


  <!-- Add to chart link -->
  <?php
  if (isset($chart_link)):
    $this->insert('components::rect-button', [
      'classes'=>[
        'ddm-ProductForm-chartButton',
        'ddm-RectButton--brownOnWhite',
      ],
      'attributes' => [
        'is' => 'AddToChartButton'
      ],
      'link' => $chart_link
    ]);
  endif; ?>


  <!-- Add to wishlist link -->
  <?php $this->insert('components::wishlist-button', [
    'link' => $wishlist_link,
    'classes'=> ['ddm-ProductForm-wishlistButton'],
  ]) ?>


  <!-- Social buttons -->
  <div class="ddm-ProductForm-share">
    <?php $this->insert('components::share', ['text' => $share_label]) ?>
  </div>


</form>

<?= $this->mark() ?>
