<?= $this->mark(true) ?>

<div is="MenuIcon" <?= $this->classes(['ddm-MenuIcon', 'ddm-MenuIcon--desktop' => $template === 'shop-emotional']); ?>>
  <span class="ddm-MenuIcon-open ddm-Icon ddm-isVisible">
    <svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#menu-hamburger-open"></use></svg>
    <svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#menu-hamburger-open-inverted"></use></svg>
  </span>

  <span class="ddm-MenuIcon-close ddm-Icon">
    <svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#menu-hamburger-close"></use></svg>
    <svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#menu-hamburger-close-inverted"></use></svg>
  </span>
</div>

<?= $this->mark() ?>
