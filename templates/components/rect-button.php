<?= $this->mark(true) ?>
<?php
/**
 *
 * @param {array} $link
 *          {string} [$link.href]
 *          {string} [$link.text]
 *          {string} [$link.title]
 * @param {string|array} $classes
 * @param {string|array} $attributes
 *
 */

$attributes = isset($attributes) ? (!is_array($attributes) ? [$attributes] : $attributes) : [];
$classes = isset($classes) ? (!is_array($classes) ? [$classes] : $classes) : [];
?>

<a <?php
  echo $this->classes(array_merge($classes, ["ddm-RectButton"]));
  echo $this->attributes(array_merge([
    'href' => $link['href'],
    'title' => $link['title'],
  ], $attributes));
?>>
  <?= $this->e($link['text']) ?>
</a>

<?= $this->mark() ?>
