<?= $this->mark(true) ?>
<?php
/**
 *
 * @param {array} $items
 * @param {int} $active
 *
 */
?>

<nav class="ddm-AccountMenu">

  <?php $this->capture(true); ?>
  <div class="ddm-AccountMenu-item ddm-AccountMenu-item--toggle">
    <?= $this->e($title) ?>
    <span class="ddm-Accordion-closeIcon ddm-Cross ddm-Icon"><svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#cross"></use></svg></span>
    <span class="ddm-Accordion-openIcon ddm-ArrowDown ddm-Icon"><svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#arrow-down"></use></svg></span>
  </div>
  <?php $accordion_title = $this->capture(false); ?>

  <?php $this->capture(true); ?>
  <ul class="ddm-AccountMenu-items">

    <?php foreach($items as $item): ?>
      <li <?= $this->classes(['ddm-AccountMenu-item', 'ddm-isActive' => (isset($active) && $active === $item['id'])]); ?>>
        <a class="ddm-AccountMenu-link" href="<?= $this->e($item['href']) ?>" title="<?= $this->e($item['title']) ?>">
          <?= $this->e($item['text']) ?>
        </a>
      </li>
    <?php endforeach; ?>

    <li class="ddm-AccountMenu-item ddm-AccountMenu-item--logout">
      <a class="ddm-AccountMenu-link" href="<?= $this->e($logout_link['href']) ?>" title="<?= $this->e($logout_link['title']) ?>">
        <span class="ddm-AccountMenu-logoutIcon ddm-Icon"><svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#logout"></use></svg></span>
        <span class="ddm-AccountMenu-logoutIcon ddm-AccountMenu-logoutIcon--hover ddm-Icon"><svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#logout-hover"></use></svg></span>
        <?= $this->e($logout_link['text']) ?>
      </a>
    </li>

  </ul>
  <?php $accordion_content = $this->capture(false); ?>

  <?php $this->insert('components::accordion', [
    'sections' => [[
      'title' => $accordion_title,
      'content' => $accordion_content
      ]]
    ])
  ?>

</nav>

<?= $this->mark() ?>
