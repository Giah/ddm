<?= $this->mark(true) ?>

<nav class="ddm-MainNavigation ddm-Container" is="MainNavigation">
  <ul class="ddm-MainNavigation-items">

    <?php foreach($main_menu as $item): ?>

      <li class="ddm-MainNavigation-item">

        <a class="ddm-MainNavigation-link" <?= $this->attributes([ 'href' => $item['href'], 'title' => $item['title'], ]) ?>>
          <?= $this->e($item['text']) ?>

          <?php if(!empty($item['dropdown'])): ?>

            <span class="ddm-Icon ddm-MainNavigation-indicator">
              <svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#menu-triangle"></use></svg>
            </span>

          <?php endif ?>
        </a>

        <?php if(!empty($item['dropdown'])): ?>

          <div class="ddm-DropDownMenu">

            <?php foreach($item['dropdown'] as $section): ?>

              <div <?= $this->classes(
                  array_merge(
                    ['ddm-DropDownMenu-section'],
                    array_map(function($class) {
                      return 'ddm-DropDownMenu-section--' . $class;
                    }, $section['classes'])
                  )
                ) ?>>

                <ul class="ddm-DropDownMenu-items">

                  <?php
                  $this->capture(true);
                  foreach ($section['items'] as $section_item): ?>

                    <li class="ddm-DropDownMenu-item">
                      <a
                        <?= $this->classes(
                          array_merge(
                            ['ddm-DropDownMenu-link'],
                            array_map(function($class) {
                              return 'ddm-DropDownMenu-link--' . $class;
                            }, $section_item['classes'])
                          )
                        ) ?>

                        <?= $this->attributes([
                          'href' => $section_item['href'],
                          'title' => (!empty($section_item['title']) ? $section_item['title'] : ''),
                        ]) ?>
                      >
                        <?php if(!empty($section_item['image'])): ?>

                          <img <?= $this->attributes([
                            'src' => $section_item['image']['src'],
                            'alt' => (!empty($section_item['image']['alt']) ? $section_item['image']['alt'] : ''),
                          ]) ?>/>
                        <?php endif ?>

                        <?php if(!empty($section_item['text'])): ?>
                          <span><?= $this->e($section_item['text']) ?></span>
                        <?php endif ?>
                      </a>
                    </li>

                  <?php
                  endforeach;
                  $section_content = $this->capture(false);
                  ?>

                  <?php if (!empty($section['title'])): ?>

                    <li class="ddm-DropDownMenu-item">
                      <span class="ddm-DropDownMenu-title"><?= $this->e($section['title']) ?></span>
                      <ul class="ddm-DropDownMenu-subSection">
                        <?= $section_content ?>
                      </ul>
                    </li>

                  <?php else: ?>

                    <?= $section_content ?>

                  <?php endif ?>

                </ul>
              </div>

            <?php endforeach ?>

          </div>

        <?php endif ?>

      </li>

    <?php endforeach ?>

  </ul>
</nav>

<?= $this->mark() ?>
