<?= $this->mark(true) ?>
<div class="ddm-FooterBar ddm-Container">
  <p class="ddm-FooterBar-text">
    <?= $this->e($footer['text']) ?>
  </p>

  <p class="ddm-FooterBar-copyright">
    <?= $this->e($footer['copyright']) ?>
  </p>

  <nav class="ddm-FooterBar-menu">

      <ul class="ddm-FooterBar-items">

          <li class="ddm-FooterBar-item">
            <a
              class="ddm-FooterBar-link"
              href="<?= $this->e($footer['menu'][0]['href']) ?>"
              title="<?= $this->e($footer['menu'][0]['title']) ?>"
            >
              <?= $this->e($footer['menu'][0]['text']) ?>
            </a>
          </li>

          <li class="ddm-FooterBar-item">
            <a
              class="ddm-FooterBar-link"
              href="<?= $this->e($footer['menu'][1]['href']) ?>"
              title="<?= $this->e($footer['menu'][1]['title']) ?>"
            >
              <?= $this->e($footer['menu'][1]['text']) ?>
            </a>
          </li>

          <li class="ddm-FooterBar-item">
            <a
              class="ddm-FooterBar-link"
              href="<?= $this->e($footer['menu'][2]['href']) ?>"
              title="<?= $this->e($footer['menu'][2]['title']) ?>"
            >
              <?= $this->e($footer['menu'][2]['text']) ?>
            </a>
          </li>

      </ul>

  </nav>

  <div class="ddm-FooterBar-share">
    <?= $this->insert('components::share', ['text' => $footer['share_label']]) ?>
  </div>

</div>

<?= $this->mark() ?>
