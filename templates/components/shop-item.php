<?= $this->mark(true) ?>
<?php
/**
 *
 * @param {array} $image
 *          {string} $image.src
 *          {string} $image.alt
 *
 * @param {string} $title
 * @param {string} $currency
 * @param {string} $price
 * @param {string} $info
 *
 * @param {array} $link
 *          {string} $link.href
 *          {string} $link.text
 *          {string} $link.title
 *
 * @param {array} wishlist_link
 *          {string} wishlist_link.href
 *          {string} wishlist_link.text
 *          {string} wishlist_link.title
 *
 */
?>

<div class="ddm-ShopItem">

  <?php if(!empty($image['src'])): ?>
    <div class="ddm-ShopItem-image">
      <picture>
        <source media="(max-width: 1023px)" srcset="<?= $this->e($image_mobile['src']) ?>" alt="<?= $this->e($image_mobile['alt']) ?>">
        <source media="(min-width: 1024px)" srcset="<?= $this->e($image['src']) ?>" alt="<?= $this->e($image['alt']) ?>">
        <img src="<?= $this->e($image['src']) ?>" alt="<?= $this->e($image['alt']) ?>">
      </picture>
    </div>
  <?php endif; ?>


  <div class="ddm-ShopItem-reveal m-t(s)@smartphone-only">
    <div class="ddm-ShopItem-revealContent">

      <?php if(!empty($title)): ?>
        <div class="ddm-ShopItem-title">
          <?= $title ?>
        </div>
      <?php endif; ?>


      <?php if(!empty($price)): ?>
        <div class="ddm-ShopItem-price">
          <?= $currency ?>
          <?= $price ?>
        </div>
      <?php endif; ?>


      <?php if(!empty($info)): ?>
        <div class="ddm-ShopItem-info">
          <?= $info ?>
        </div>
      <?php endif; ?>


      <?php
      if (isset($link)):
        $this->insert('components::rect-button', [
          'classes'=>[
            'ddm-ShopItem-button',
            "ddm-RectButton--brownOnPink",
          ],
          'link' => $link
        ]);
      endif;
      ?>


      <?php
      $this->insert('components::wishlist-button', [
        'link' => $wishlist_link,
        'classes'=> ['ddm-ShopItem-wishlistButton']
      ]);
      ?>

    </div>
  </div>

</div>

<?= $this->mark() ?>
