<?= $this->mark(true) ?>
<?php
/**
 *
 * @param {array} $sections - An array of sections
 * @param {string} $sections.title - HTML content
 * @param {array} $section.content - HTML content
 * @param {string} $attributes
 *
 */
?>

<div class="ddm-Accordion" <?php echo !empty($attributes) ? $attributes : ''; ?> is="Accordion">

  <?php foreach ($sections as $section): ?>

    <div class="ddm-Accordion-title">
      <?= $section['title'] ?>
    </div>

    <div class="ddm-Accordion-content">
      <?= $section['content'] ?>
    </div>

  <?php endforeach; ?>

</div>

<?= $this->mark() ?>
