<?= $this->mark(true) ?>
<?php
/**
 *
 * @param {string} $params.image
 * @param {string} $params.title
 * @param {string} $params.link
 *
 */
?>

<div class="ddm-ExpandingBox">

  <!-- Image -->
  <?php if(isset($image['src'])): ?>

    <?php $this->capture(true); ?>
    <img src="<?= $this->e($image['src']) ?>" alt="<?= $this->e($image['alt']) ?>">
    <?php $content = $this->capture(false) ?>

    <?php $this->insert('components::proportional-container', [
      'content' => $content,
      'class' => 'ddm-ExpandingBox-image'
    ]) ?>

  <?php endif; ?>

  <div class="ddm-ExpandingBox-content">

    <!-- Title -->
    <?php if(isset($title)): ?>
      <h3 class="<?= $this->e("ddm-ExpandingBox-title ddm-ExpandingBox-title--$title_size") ?>">
        <?= $title ?>
      </h3>
    <?php endif; ?>


    <!-- Link -->
    <?php if(!empty($link['text'])): ?>
      <a class="ddm-ExpandingBox-button" href="<?= $this->e($link['href']) ?>" title="<?= $this->e($link['title']) ?>" >
        <span class="ddm-ExpandingBox-buttonLabel"><?= $link['text'] ?></span>
      </a>
    <?php endif; ?>


  </div>

</div>

<?= $this->mark() ?>
