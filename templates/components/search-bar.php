<?= $this->mark(true) ?>

<div class="ddm-SearchBar">
    <input type="text" name="search" value="" class="ddm-SearchBar-input">
    <span class="ddm-SearchBar-icon ddm-Icon">
        <!-- TODO: replace with SVG -->
        <img src="<?= $this->asset($paths['images'] . 'search-icon.png'); ?>" alt="">
    </span>
</div>

<?= $this->mark() ?>
