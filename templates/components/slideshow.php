<div class="ddm-Slideshow" is="Slideshow" :wait="5000">
  <?php foreach($images as $key => $image): ?>
    <?php if(!empty($image['mobile']) && !empty($image['desktop'])): ?>

      <div class="ddm-Slideshow-item" is="SlideshowItem" :index="<?= $key ?>">
        <picture>
          <source <?= $this->attributes([
            'media' => '(max-width: 767px)',
            'srcset' => $image['mobile']['src'],
            'alt' => $image['mobile']['alt'],
          ]) ?>>
          <source <?= $this->attributes([
            'media' => '(min-width: 768px)',
            'srcset' => $image['desktop']['src'],
            'alt' => $image['desktop']['alt'],
          ]) ?>>
          <img class="ddm-Slideshow-image" <?= $this->attributes([
            'src' => $image['desktop']['src'],
            'alt' => $image['desktop']['alt'],
          ]) ?>>
        </picture>
      </div>

    <?php else: ?>

      <div class="ddm-Slideshow-item ddm-isVisible">
        <img class="ddm-Slideshow-image" <?= $this->attributes([
          'src' => $image['src'],
          'alt' => $image['alt'],
        ]) ?>/>
      </div>

    <?php endif; ?>
  <?php endforeach; ?>
</div>
