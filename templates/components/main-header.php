<?= $this->mark(true) ?>

<header class="ddm-MainHeader ddm-Wrapper" is="MainHeader" <?= $this->attributes([ 'data-always-mini' => $template === 'shop-emotional' ])?> >

  <?php $this->insert('components::main-logo', [ 'paths'=>$paths, 'white' => $template === 'shop-emotional' ]); ?>
  <?php $this->insert('components::menu-icon'); ?>
  <?php $this->insert('components::menu-container'); ?>

  <div class="ddm-MainHeader-shadow"></div>

</header>

<?= $this->mark() ?>
