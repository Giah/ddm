<?= $this->mark(true) ?>
<?php
/**
 *
 * @param {array} $table
 *          {array} $table.head
 *          {array} $table.body
 * @param {array|string} [$variants]
 *
 */

$variants = isset($variants) ? (!is_array($variants) ? [$variants] : $variants) : [];
?>


<table <?= $this->classes(array_merge(['ddm-Table'], array_map(function($variant) { return "ddm-Table--$variant"; }, $variants))); ?>>

  <?php if(!empty($table['head'])): ?>

    <thead <?= $this->classes(array_merge(['ddm-Table-head'], array_map(function($variant) { return "ddm-Table-head--$variant"; }, $variants))); ?>>
      <tr <?= $this->classes(array_merge(['ddm-Table-row', 'ddm-Table-row--head'], array_map(function($variant) { return "ddm-Table-row--$variant"; }, $variants))); ?>>

        <?php foreach($table['head'] as $t=> $th): $i = $t + 1; ?>
          <th <?= $this->classes(array_merge(['ddm-Table-cell', "ddm-Table-cell--$i", 'ddm-Table-th', "ddm-Table-th--$i"], array_map(function($variant) use ($i) { return "ddm-Table-cell--$variant ddm-Table-th--$variant ddm-Table-th--$variant-$i"; }, $variants))); ?>>
            <?php echo $th; ?>
          </th>
        <?php endforeach; ?>

      </tr>
    </thead>

  <?php endif; ?>

  <tbody <?= $this->classes(array_merge(['ddm-Table-body'], array_map(function($variant) { return "ddm-Table-body--$variant"; }, $variants))); ?>>

    <?php foreach($table['body'] as $r => $row): ?>
      <tr <?= $this->classes(array_merge(['ddm-Table-row', 'ddm-Table-row--body', "ddm-Table-row--".($r + 1)], array_map(function($variant) use($r) { return "ddm-Table-row--$variant ddm-Table-row--$variant-".($r + 1); }, $variants))); ?>>

        <?php foreach($row as $t => $td): $i = $t + 1; ?>
          <td <?= $this->classes(array_merge(['ddm-Table-cell', "ddm-Table-cell--$i", 'ddm-Table-td', "ddm-Table-td--$i"], array_map(function($variant) use ($i) { return "ddm-Table-cell--$variant ddm-Table-td--$variant ddm-Table-td--$variant-$i"; }, $variants))); ?>  data-header="<?php echo isset($table['head']) ? $table['head'][$t] : ''; ?>">
            <div <?= $this->classes(array_merge(['ddm-Table-cellInner', "ddm-Table-cellInner--$i"], array_map(function($variant) use($i) { return "ddm-Table-cellInner--$variant ddm-Table-cellInner--$variant-$i"; }, $variants))); ?>>
              <?php echo $td; ?>
            </div>
          </td>
        <?php endforeach; ?>

      </tr>
    <?php endforeach; ?>

  </tbody>

</table>

<?= $this->mark() ?>
