<?= $this->mark(true) ?>
<?php
/**
 *
 * @param {string="wide","narrow"} $params.size
 * @param {string} $image
 *          {string} $image.src
 *          {string} [$image.alt]
 * @param {string} $params.title
 * @param {string} $params.sub_title
 * @param {string} $params.link
 * @param {string} [$params.image_mobile]
 *
 */

?>

<div <?= $this->classes(['ddm-RevealBox', "ddm-RevealBox--$size"]) ?>>

  <div class="ddm-RevealBox-content">

    <!-- Image -->
    <?php if(isset($image_mobile['src'])): ?>

      <?php $this->capture(true); ?>
      <figure>
        <picture>
          <source media="(max-width: 767px)" srcset="<?= $this->e($image_mobile['src']) ?>" alt="<?= $this->e($image_mobile['alt']) ?>">
          <source media="(min-width: 768px)" srcset="<?= $this->e($image['src']) ?>" alt="<?= $this->e($image['alt']) ?>">
          <img src="<?= $this->e($image['src']) ?>" alt="<?= $this->e($image['alt']) ?>">
        </picture>
      </figure>
      <?php $content = $this->capture(false); ?>

      <?php $this->insert('components::proportional-container', [
        'content' => $content,
        'class' => "ddm-RevealBox-image ddm-RevealBox-image--$size"
      ]) ?>

    <?php else: ?>

      <?php $this->capture(true); ?>
      <img src="<?= $this->e($image['src']) ?>" alt="<?= $this->e($image['alt']) ?>">
      <?php $content = $this->capture(false) ?>

      <?php $this->insert('components::proportional-container', [
        'content' => $content,
        'class' => "ddm-RevealBox-image ddm-RevealBox-image--$size"
      ]) ?>

    <?php endif; ?>

    <div class="ddm-RevealBox-reveal">

        <div class="ddm-RevealBox-revealContent">

          <!-- Title -->
          <?php if(isset($title)): ?>

          <h3 class="ddm-RevealBox-title">
            <?= $this->e($title) ?>
          </h3>

          <?php endif; ?>


          <!-- Sub title -->
          <?php if(isset($sub_title)): ?>

          <p class="ddm-RevealBox-subTitle">
            <?= $this->e($sub_title) ?>
          </p>

          <?php endif; ?>

          <?php if (isset($link)): ?>
            <?php $this->insert('components::rect-button', [
              'classes'=>['ddm-RevealBox-button', "ddm-RectButton--transparent"],
              'link' => $link
            ]); ?>
          <?php endif; ?>

        </div>


    </div>

  </div>

</div>

<?= $this->mark() ?>
