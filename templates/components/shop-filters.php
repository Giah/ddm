<?= $this->mark(true) ?>
<?php
/**
 *
 * @param {array} $items - An array of links
 * @param {string} $items.href
 * @param {string} $items.text
 * @param {string} $items.title
 *
 */
?>

<ul class="ddm-Shop-filtersList">

  <?php foreach($items as $item): ?>

    <li class="ddm-Shop-filtersCategory">

    <?php if(!empty($item['href']) && !empty($item['text'])): ?>

      <a href="<?= $this->e($item['href']) ?>"
        title="<?= $this->e($item['title']) ?>"
      >
        <?= $item['text'] ?> (<?= $item['count'] ?>)
      </a>

    <?php endif; ?>

    </li>

  <?php endforeach; ?>

</ul>

<?= $this->mark() ?>
