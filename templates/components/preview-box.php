<?= $this->mark(true) ?>

<?php
/**
 *
 * @param {string} $cols - How many box should fit in a row. Possible values are: 3, 4
 * @param {string} $image
 *          {string} $image.src
 *          {string} [$image.alt]
 * @param {string} $title
 * @param {string} [$sub_title]
 * @param {string} [$image_mobile]
 *          {string} $image_mobile.src
 *          {string} [$image_mobile.alt]
 * @param {string} $link
 *          {string} $link.href
 *          {string} $link.text
 *          {string} [$link.title]
 * @param {string} [$title_size]
 * @param {bool} [$rounded]
 *
 */

if (!isset($title_size)) $title_size = '';
if (!isset($rounded)) $rounded = '';
?>

<div <?= $this->classes(['ddm-PreviewBox', "ddm-PreviewBox--$cols"]); ?>>

  <div class="ddm-PreviewBox-content">

    <?php if(!empty($image_mobile)): ?>

      <?php $this->capture(true) ?>
      <picture>
        <source <?= $this->attributes([
          'media' => '(max-width: 767px)',
          'srcset' => $image_mobile['src'],
          'alt' => $image_mobile['alt'],
        ]) ?>/>
        <source <?= $this->attributes([
          'media' => '(min-width: 768px)',
          'srcset' => $image['src'],
          'alt' => $image['alt'],
        ]) ?>/>
        <img <?php
        echo $this->classes([
          'ddm-PreviewBox-image',
          "ddm-PreviewBox-image--rounded" => $rounded,
        ]);
        echo $this->attributes([
          'src' => $image['src'],
          'alt' => !empty($image['alt']) ? $image['alt'] : '',
        ]) ?>/>
      </picture>
      <?php $content = $this->capture(false); ?>

      <?php $this->insert('components::proportional-container', [
        'class' => 'ddm-PreviewBox-image',
        'content' => $content,
      ]) ?>

    <?php else: ?>

      <?php $this->capture(true) ?>
      <img class="ddm-PreviewBox-image" <?= $this->attributes([
        'src' => $image['src'],
        'alt' => $image['alt'],
      ]) ?>/>
      <?php $content = $this->capture(false); ?>

      <?php $this->insert('components::proportional-container', [
        'class' => 'ddm-PreviewBox-image',
        'content' => $content,
      ]) ?>

    <?php endif; ?>


    <?php if (!empty($category) && !empty($date)): ?>
      <div class="ddm-PreviewBox-meta">
        <span class="ddm-PreviewBox-category"><?= $this->e($category) ?></span>
        <span class="ddm-PreviewBox-date"><?= $this->e($date) ?></span>
      </div>
    <?php endif; ?>

    <?php if (!empty($title)): ?>
      <h3 <?= $this->classes(['ddm-PreviewBox-title', "ddm-PreviewBox-title--$title_size" => $title_size]); ?>>
        <?= $title ?>
      </h3>
    <?php endif; ?>


    <?php if (!empty($sub_title)): ?>
      <p <?= $this->classes(['ddm-PreviewBox-subTitle', "ddm-PreviewBox-subTitle"]); ?>>
        <?= $sub_title ?>
      </p>
    <?php endif; ?>


    <?php if (!empty($price)): ?>
      <p <?= $this->classes(['ddm-PreviewBox-price', "ddm-PreviewBox-price"]); ?>>
        <?= $currency . $price ?>
      </p>
    <?php endif; ?>


    <?php
    if (isset($link)):
      if(!empty($buttonCover)):
        $this->insert('components::link', [
          'class'=>[
            'ddm-PreviewBox-button',
            "ddm-Button--invisible",
            "ddm-Button--cover",
          ],
          'link' => $link
        ]);
      else:
        $this->insert('components::linear-button', [
          'class'=>[
            'ddm-PreviewBox-button',
            'ddm-LinearButton',
            'ddm-LinearButton--brown',
          ],
          'link' => $link
        ]);
      endif;
    endif; ?>

  </div>

</div>

<?= $this->mark() ?>
