<?= $this->mark(true) ?>

<a title="Homepage" href="#" <?= $this->classes(['ddm-MainLogo'=>true, 'ddm-MainLogo--white'=>$white]) ?>>
  <img
    class="ddm-MainLogo-image"
    src="<?= $this->asset($paths['images'] . 'ddm-logo.svg') ?>"
    alt="Daniela De Marchi brand logo"
  />
  <img
    class="ddm-MainLogo-image ddm-MainLogo-image--white"
    src="<?= $this->asset($paths['images'] . 'ddm-logo-white.svg') ?>"
    alt="Daniela De Marchi brand logo"
  />
</a>

<?= $this->mark() ?>
