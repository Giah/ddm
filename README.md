## Atomic design

Templates, stylesheets and scripts are modularized by the principle of [Atomic Design](http://bradfrost.com/blog/post/atomic-web-design/)

## Naming Conventions

All class names follow [SUIT CSS](https://github.com/suitcss/suit/blob/master/doc/naming-conventions.md) naming convention and are prefixed with ```ddm-``` to avoid potential naming collisions

## Stylesheets organization
Stylesheets files are organized following [ITCSS](https://www.xfive.co/blog/itcss-scalable-maintainable-css-architecture/) model

## File tree
All source files belongs in the ```src``` folder and get compiled in the ```public``` folder
:warning: Do not put files inside the ```public``` folder.
Every time you do a copmlete build the ```public``` folder get erased and everything is rebuild from scratch from the ```src``` folder.

## Usage
Make sure Node installed.

#### Install Dependencies
```bash
npm install
```

#### Run development tasks:
:warning: This cleanup the ```public``` folder and regenerate everything from scratch
```
npm run gulp
```

#### Only compile javascript and css (skip images, icons, svg and others expensive tasks):
Do not perform cleanup
```
npm run code
```

#### Run tests once:
```bash
npm run test
```

#### Run in tests in watch mode:
```bash
npm run test:watch
```

#### Build production files:
:warning: This cleanup the ```public``` folder and regenerate everything from scratch
```bash
npm run production
```

### Navigate templates
To navigate between templates append the param ```type``` to the url, with name of the page template

```
http://www.siteurl.com/theme?template=home
```
