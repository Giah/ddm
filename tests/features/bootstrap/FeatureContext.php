<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Exception\PendingException;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Behat\Hook\Scope\BeforeFeatureScope;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements Context
{
    protected $output_path;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct($output_path) {
      $this->output_path = $output_path;
    }

    /**
     * @BeforeFeature @save-output
     *
     * Create or empty the output directory before saving the output
     */
    public static function createOutputDirectory(BeforeFeatureScope $scope) {
      $output_path = $scope->getSuite()->getSettings()['contexts'][0]['FeatureContext']['output_path'];
      // create output directory
      if (!file_exists($output_path)) {
        mkdir($output_path, 0777, true);
      }
      // or empty it if already exists
      else {
        $di = new RecursiveDirectoryIterator($output_path, FilesystemIterator::SKIP_DOTS);
        $ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($ri as $item) {
          $item->isDir() ?  rmdir($item) : unlink($item);
        }
      }
    }

    /**
     * @BeforeFeature @compare-output
     *
     * Delete diff files before comparing output
     */
    public static function removeDiffFiles(BeforeFeatureScope $scope) {
      $output_path = $scope->getSuite()->getSettings()['contexts'][0]['FeatureContext']['output_path'];
      $di = new RecursiveDirectoryIterator($output_path, FilesystemIterator::SKIP_DOTS);
      $ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
      foreach ($ri as $item) {
        if (!$item->isDir() && $item->getExtension() === 'diff') {
          unlink($item);
        }
      }
    }

    /**
     * @Then Save output
     */
    public function saveOutput() {
      $session = $this->getSession();
      $url = $session->getCurrentUrl();
      $templateName = $this->getTemplateNameFromURL($url);
      $html = $session->getPage()->getHtml();
      file_put_contents($this->output_path . DIRECTORY_SEPARATOR . $templateName . '.html', $html);
    }

    /**
     * @Then Compare output
     */
    public function compare_output() {
      $session = $this->getSession();
      $url = $session->getCurrentUrl();
      $templateName = $this->getTemplateNameFromURL($url);
      $output_file = $this->output_path . DIRECTORY_SEPARATOR . $templateName . '.html';
      if (!file_exists($output_file)) {
        throw new \Exception('No previous output to compare.');
      }
      $output = file_get_contents($output_file);
      $html = $session->getPage()->getHtml();
      if ($output !== $html) {
        file_put_contents($this->output_path . DIRECTORY_SEPARATOR . $templateName . '.diff', $html);
        throw new \Exception('Output do not match');
      }

    }

    protected function getTemplateNameFromURL($url) {
      preg_match('/template=(.+)$/', $url, $matches);
      return $matches[1];
    }

}
