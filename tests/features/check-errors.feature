Feature: Check
  Check templates have no errors

  Scenario Outline: check
    Given I am on <page>
    Then I should see a "#ddm-MainWrapper" element

    Examples:
      | page |
      | "index.php?template=account-orders" |
      | "index.php?template=account-overview" |
      | "index.php?template=account-preferences" |
      | "index.php?template=account-wishlist" |
      | "index.php?template=brand" |
      | "index.php?template=chart" |
      | "index.php?template=collection" |
      | "index.php?template=collections" |
      | "index.php?template=contact" |
      | "index.php?template=home" |
      | "index.php?template=login" |
      | "index.php?template=magazine" |
      | "index.php?template=materials" |
      | "index.php?template=news" |
      | "index.php?template=product" |
      | "index.php?template=registration" |
      | "index.php?template=resellers" |
      | "index.php?template=shop" |
      | "index.php?template=shop-emotional" |
      | "index.php?template=simple" |
      | "index.php?template=shops" |
