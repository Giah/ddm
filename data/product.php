<?php
require_once('lib/DataProvider.php');

class Product extends DataProvider {

  public function name() {
    return 'Anello contaminazioni';
  }

  public function price() {
    return '170,00';
  }

  public function sizes_label() {
    return 'Seleziona taglia';
  }

  public function sizes() {
    return [
      [
        'value' => 'S',
        'text' => 'S (Diametro interno 12-13mm)',
      ],
      [
        'value' => 'L',
        'text' => 'L (Diametro interno 14-15mm)',
      ],
      [
        'value' => 'XL',
        'text' => 'XL (Diametro interno 18-19mm)',
      ],
    ];
  }

  public function quantity_label() {
    return 'Seleziona quantita';
  }

  public function variants_label() {
    return 'Seleziona variante';
  }

  public function variants() {
    return [
      [
        'name' => 'Roxie Torres',
        'stone' => 'Tulueji',
        'metal' => 'Buwazewo',
        'button_image' => [
          'src' => 'http://www.placehold.it/50x50/f46504',
          'alt' => '',
        ],
        'thumbnails' => [
          [
            'src' => 'http://www.placehold.it/100x100/f46504',
            'alt' => '',
          ],
          [
            'src' => 'http://www.placehold.it/100x100/f46504',
            'alt' => '',
          ],
        ],
      ],
      [
        'name' => 'Ernest Kim',
        'stone' => 'Yuoajf',
        'metal' => 'Duoank',
        'button_image' => [
          'src' => 'http://www.placehold.it/50x50/662222',
          'alt' => '',
        ],
        'thumbnails' => [
          [
            'src' => 'http://www.placehold.it/100x100/662222',
            'alt' => '',
          ],
          [
            'src' => 'http://www.placehold.it/100x100/662222',
            'alt' => '',
          ],
        ],
      ],
    ];
  }

  public function images() {
    return [
      [
        'src' => 'http://www.placehold.it/710x550',
        'alt' => '',
      ],
      [
        'src' => 'http://www.placehold.it/710x550',
        'alt' => '',
      ],
      [
        'src' => 'http://www.placehold.it/710x550',
        'alt' => '',
      ],
      [
        'src' => 'http://www.placehold.it/710x550',
        'alt' => '',
      ],
      [
        'src' => 'http://www.placehold.it/710x550',
        'alt' => '',
      ],
    ];
  }

  public function notes() {
    return 'Tempi di realizzazione: 15 giorni lavorativi';
  }

  public function description_label() {
    return 'Descrizione';
  }

  public function description_text() {
    return 'Nosuuru rumit vawdop hegegod zufihu hu opfizwa julurlur lo vib giclatat odekubo uri. Udi powkatub enevaufo apa maoza ner mip eskun unotaavu obvikak wilsihus feldop jovseli igafemni dotse uvi divvo. Noifokum jov opujivuka dowbi nuh benzoje din ituuvdok ubeogu fovmobis va ma fepo ceja. Ohizuok dah coajsod kevmibrit pacateago kongeg jegegez varre ovfu fov puilgu ilfojez fodco isedwu gi. Tej vojhu feval evher zeztauf wiliguw vob pumak meuji moj ocu sujukucef aceup cura ar dugeat.';
  }

  public function description_read_more() {
    return '...leggi di piu';
  }

  public function materials_guide_link() {
    return [
      'href' => '#',
      'text' => 'Guida ai materiali',
      'title' => 'Guida ai materiali',
    ];
  }

  public function sizes_guide_link() {
    return [
      'href' => '#',
      'text' => 'Guida alle taglie',
      'title' => 'Guida alle taglie',
    ];
  }

  public function shipping_link() {
    return [
      'href' => '#',
      'text' => 'Spedizione e resi',
      'title' => 'Spedizione e resi',
    ];
  }

  public function chart_link() {
    return [
      'href' => '#',
      'text' => 'Aggiungi alla shopping bag',
      'title' => 'Aggiungi alla shopping bag',
    ];
  }

  public function wishlist_link() {
    return [
      'href' => '#',
      'text' => 'Aggiungi alla wishlist',
      'title' => 'Aggiungi alla wishlist',
    ];
  }

  public function buy_link() {
    return [
      'href' => '#',
      'text' => 'Procedi all\'acquisto',
      'title' => 'Procedi all\'acquisto',
    ];
  }

  public function product_modal_labels  () {
    return [
      'product-name' => 'I nostri artigiani creeranno per te il seguente gioiello:',
      'product-metal' => 'Metallo:',
      'product-stone' => 'Pietra:',
      'close' => 'Torna alla scheda prodotto',
    ];
  }

  public function share_label() {
    return 'Condividi';
  }

  public function product_matches_label() {
    return 'Abbinalo con:';
  }

  public function product_matches() {
    return [
      [
        'image' => [
          'src' => 'http://www.placehold.it/280x280/ffffff',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/587x587/ffffff',
          'alt' => '',
        ],
        'title' => 'Fanny Diaz',
        'price' => '454,00',
        'link' => [
          'href' => '#',
          'text' => 'Shop now',
          'title' => 'Shop now',
        ],
      ],
      [
        'image' => [
          'src' => 'http://www.placehold.it/280x280/ffffff',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/587x587/ffffff',
          'alt' => '',
        ],
        'title' => 'Fanny Diaz',
        'price' => '454,00',
        'link' => [
          'href' => '#',
          'text' => 'Shop now',
          'title' => 'Shop now',
        ],
      ],
      [
        'image' => [
          'src' => 'http://www.placehold.it/280x280/ffffff',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/587x587/ffffff',
          'alt' => '',
        ],
        'title' => 'Fanny Diaz',
        'price' => '454,00',
        'link' => [
          'href' => '#',
          'text' => 'Shop now',
          'title' => 'Shop now',
        ],
      ],
    ];
  }

  public function collection_teaser() {
    return [
      'images' => [
        [
          'desktop' => [
            'src' => 'http://www.placehold.it/1180x550',
            'alt' => '',
          ],
          'mobile' => [
            'src' => 'http://www.placehold.it/768x920',
            'alt' => '',
          ],
        ]
      ],
      'title' => 'Collezione Intramontabili',
      'text' => 'Colore intenso, denso, lucido e luminoso, contaminato soltando dai contorni in rilievo, lavorati con la tecnica del dopage.',
      'link' => [
        'href' => '#',
        'text' => 'Shop the collection',
        'title' => 'Shop the collection',
      ],
    ];
  }

  public function shop_suggestions_label() {
    return 'Ti potrebbero interessare:';
  }

  public function shop_suggestions() {
    return [
      [
        'image' => [
          'src' => 'http://www.placehold.it/280x280',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/587x587',
          'alt' => '',
        ],
        'title' => 'Fanny Diaz',
        'price' => '454,00',
        'link' => [
          'href' => '#',
          'text' => 'Shop now',
          'title' => 'Shop now',
        ],
      ],
      [
        'image' => [
          'src' => 'http://www.placehold.it/280x280',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/587x587',
          'alt' => '',
        ],
        'title' => 'Fanny Diaz',
        'price' => '454,00',
        'link' => [
          'href' => '#',
          'text' => 'Shop now',
          'title' => 'Shop now',
        ],
      ],
      [
        'image' => [
          'src' => 'http://www.placehold.it/280x280',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/587x587',
          'alt' => '',
        ],
        'title' => 'Fanny Diaz',
        'price' => '454,00',
        'link' => [
          'href' => '#',
          'text' => 'Shop now',
          'title' => 'Shop now',
        ],
      ],
      [
        'image' => [
          'src' => 'http://www.placehold.it/280x280',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/587x587',
          'alt' => '',
        ],
        'title' => 'Fanny Diaz',
        'price' => '454,00',
        'link' => [
          'href' => '#',
          'text' => 'Shop now',
          'title' => 'Shop now',
        ],
      ],
    ];
  }

}
