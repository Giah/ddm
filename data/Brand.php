<?php
require_once('lib/DataProvider.php');

class Brand extends DataProvider {

  public function jumbotron() {
    return [
      'images' => [
        [
          'desktop' => [
            'src' => 'http://www.placehold.it/1180x550/874126',
            'alt' => '',
          ],
          'mobile' => [
            'src' => 'http://www.placehold.it/768x920/874126',
            'alt' => '',
          ],
        ]
      ],
      'title' => 'Il marchio DDM',
      'sub_title' => 'Ri mecnijer wasti ne jus wiid cewikuju ubine jizut omakek utacokit goj ale poodsa teive.',
      'text' => '<p>Vamrov behu jo vaen afmi pavub ji lujahun iljig zag ep jot sudkoeko nozho jega rasozbo nenozi. Eptebti rugpu laizbup ugaletu rakiwson dekabih bocuju vojimho orecutol he medajrog bubito tucboipi lo def tesu zismil hubuse.</p><p>Gi rep zigoure ti pov det jusor rog nog ocatapraw leriv bi nedozvo uvagegwu iw. Nodzi sildo if hos budic vut gikgon hajipvec voeki zotit neweloim gi.</p><p>Ipzom ducak domres cu ze koz voh fifahgaj ecude boga kik kel ma umazid vivonailu.</p>',
    ];
  }

  public function title() {
    return 'Daniela De Marchi';
  }

  public function sub_title() {
    return '<p>Perpesma vubkeh vimci caowijaz mavakuh ov pur ekeri amahaati suv covezo sef akub tohovbu. <br/>Kauba de paw gekfuhu nuk gotlu zigcak tojbeojdapih po wook rokroc patnupo ti evfabri rihadeg valila aluovis.</p>';
  }

  public function timeline() {
    return [
      [
        'date' => '2001',
        'title' => 'Ci pe ut demlutwad muvemiz fuazatav dute jeog ijizo goru nozitevu ma efmu lomi zeunadez.',
        'text' => 'Tatbugod bad pat dakuf muw vavkituk vocu gipdajebo cotana nire iri jijhan zade. Avuatopok kozekron umnofav cofezwi ja ujipipac luinokaf je ejpikin ohceg osli levohnim fawrevis vute sagti.',
        'image' => [
          'src' => 'http://www.placehold.it/415x415',
          'alt' => ''
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/690x690',
          'alt' => ''
        ]
      ],
      [
        'date' => '2001',
        'title' => 'Ci pe ut demlutwad muvemiz fuazatav dute jeog ijizo goru nozitevu ma efmu lomi zeunadez.',
        'text' => 'Tatbugod bad pat dakuf muw vavkituk vocu gipdajebo cotana nire iri jijhan zade. Avuatopok kozekron umnofav cofezwi ja ujipipac luinokaf je ejpikin ohceg osli levohnim fawrevis vute sagti.',
        'image' => [
          'src' => 'http://www.placehold.it/415x415',
          'alt' => ''
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/690x690',
          'alt' => ''
        ]
      ],
      [
        'date' => '2001',
        'title' => 'Ci pe ut demlutwad muvemiz fuazatav dute jeog ijizo goru nozitevu ma efmu lomi zeunadez.',
        'text' => 'Tatbugod bad pat dakuf muw vavkituk vocu gipdajebo cotana nire iri jijhan zade. Avuatopok kozekron umnofav cofezwi ja ujipipac luinokaf je ejpikin ohceg osli levohnim fawrevis vute sagti.',
        'image' => [
          'src' => 'http://www.placehold.it/415x415',
          'alt' => ''
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/690x690',
          'alt' => ''
        ]
      ],
      [
        'date' => '2001',
        'title' => 'Ci pe ut demlutwad muvemiz fuazatav dute jeog ijizo goru nozitevu ma efmu lomi zeunadez.',
        'text' => 'Tatbugod bad pat dakuf muw vavkituk vocu gipdajebo cotana nire iri jijhan zade. Avuatopok kozekron umnofav cofezwi ja ujipipac luinokaf je ejpikin ohceg osli levohnim fawrevis vute sagti.',
        'image' => [
          'src' => 'http://www.placehold.it/415x415',
          'alt' => ''
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/690x690',
          'alt' => ''
        ]
      ],
      [
        'date' => '2001',
        'title' => 'Ci pe ut demlutwad muvemiz fuazatav dute jeog ijizo goru nozitevu ma efmu lomi zeunadez.',
        'text' => 'Tatbugod bad pat dakuf muw vavkituk vocu gipdajebo cotana nire iri jijhan zade. Avuatopok kozekron umnofav cofezwi ja ujipipac luinokaf je ejpikin ohceg osli levohnim fawrevis vute sagti.',
        'image' => [
          'src' => 'http://www.placehold.it/415x415',
          'alt' => ''
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/690x690',
          'alt' => ''
        ]
      ],
    ];
  }

}
