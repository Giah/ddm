<?php
require_once('lib/DataProvider.php');

class Collection extends DataProvider {

  public function title() {
    return 'Spring/Summer 2016';
  }

  public function sub_title() {
    return '<p>Perpesma vubkeh vimci caowijaz mavakuh ov pur ekeri amahaati suv covezo sef akub tohovbu. <br/>Kauba de paw gekfuhu nuk gotlu zigcak tojbeoj inidapih po wook rokroc patnupo ti evfabri rihadeg valila aluovis.</p>';
  }

  public function jumbotron() {
    return [
      'images' => [
        [
          'desktop' => [
            'src' => 'http://www.placehold.it/1180x550',
            'alt' => '',
          ],
          'mobile' => [
            'src' => 'http://www.placehold.it/768x920',
            'alt' => '',
          ],
        ]
      ],
      'title' => 'Collezione Intramontabili',
      'text' => 'Colore intenso, denso, lucido e luminoso, contaminato soltando dai contorni in rilievo, lavorati con la tecnica del dopage.',
      'link' => [
        'href' => '#',
        'text' => 'Scopri il gioiello perfetto',
        'title' => 'Scopri il gioiello perfetto',
      ],
    ];
  }

  public function reveal_boxes() {
    return [
      [
        'image' => [
          'src' => 'http://www.placehold.it/740x425',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'title' => 'I nostri negozi',
        'sub_title' => 'Fejjucbe vemiope umowiseb',
        'link' => [
          'href' => '#',
          'text' => 'Scopri di più',
          'title' => 'Scopri di più',
        ],
      ],
      [
        'image' => [
          'src' => 'http://www.placehold.it/425x425',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'title' => 'Novitá',
        'sub_title' => 'Fejjucbe vemiope umowiseb',
        'link' => [
          'href' => '#',
          'text' => 'Scopri di più',
          'title' => 'Scopri di più',
        ],
      ],
      [
        'image' => [
          'src' => 'http://www.placehold.it/425x425',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'title' => 'DDMagazine',
        'sub_title' => 'Fejjucbe vemiope umowiseb',
        'link' => [
          'href' => '#',
          'text' => 'Scopri di più',
          'title' => 'Scopri di più',
        ],
      ],
      [
        'image' => [
          'src' => 'http://www.placehold.it/740x425',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'title' => 'Stili',
        'sub_title' => 'Fejjucbe vemiope umowiseb',
        'link' => [
          'href' => '#',
          'text' => 'Scopri di più',
          'title' => 'Scopri di più',
        ],
      ],
    ];
  }

  public function preview_boxes() {
    return [
      [
        'image' => [
          'src' => 'http://www.placehold.it/280x280',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'title' => 'Anello serpente a due teste',
        'sub_title' => '',
        'link' => [
          'href' => '#',
          'text' => 'Shop now',
          'title' => 'Shop now',
        ],
      ],
      [
        'image' => [
          'src' => 'http://www.placehold.it/280x280',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'title' => 'Anello Pavone',
        'sub_title' => '',
        'link' => [
          'href' => '#',
          'text' => 'Shop now',
          'title' => 'Shop now',
        ],
      ],
      [
        'image' => [
          'src' => 'http://www.placehold.it/280x280',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'title' => 'Orecchini pendolo',
        'sub_title' => '',
        'link' => [
          'href' => '#',
          'text' => 'Shop now',
          'title' => 'Shop now',
        ],
      ],
      [
        'image' => [
          'src' => 'http://www.placehold.it/280x280',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'title' => 'Orecchini pendolo',
        'sub_title' => '',
        'link' => [
          'href' => '#',
          'text' => 'Shop now',
          'title' => 'Shop now',
        ],
      ],
    ];
  }
}
