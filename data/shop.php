<?php
require_once('lib/DataProvider.php');

class Shop extends DataProvider {

  public function jumbotron() {
    return [
      'images' => [
        [
          'src' => 'http://www.placehold.it/1180x220',
          'alt' => '',
        ],
      ],
      'title' => 'Tutti i gioielli Daniela De Marchi sono pezzi unici realizzati a mano.',
      'sub_title' => 'I tempi di realizzazione dei prodotti sono di 15 giorni lavorativi',
    ];
  }

  public function filter_title() {
    return 'Filtra per';
  }

  public function sort_title() {
    return 'Ordina per';
  }

  public function products_category_count() {
    return '98';
  }

  public function products_category_count_label() {
    return 'prodotti';
  }

  public function filters() {
    return [
      [
        'title' => 'Gioielli',
        'items' => [
          [
            'href' => '#',
            'text' => 'Anelli',
            'title' => 'Anelli',
            'count' => '10',
          ],
          [
            'href' => '#',
            'text' => 'Bracciali',
            'title' => 'Bracciali',
            'count' => '10',
          ],
          [
            'href' => '#',
            'text' => 'Collane e pendenti',
            'title' => 'Collane e pendenti',
            'count' => '10',
          ],
          [
            'href' => '#',
            'text' => 'Orecchini',
            'title' => 'Orecchini',
            'count' => '10',
          ],
          [
            'href' => '#',
            'text' => 'Ciondoli',
            'title' => 'Ciondoli',
            'count' => '10',
          ],
          [
            'href' => '#',
            'text' => 'Spille',
            'title' => 'Spille',
            'count' => '10',
          ],
          [
            'href' => '#',
            'text' => 'Accessori',
            'title' => 'Accessori',
            'count' => '10',
          ],
          [
            'href' => '#',
            'text' => 'Accessori uomo',
            'title' => 'Accessori uomo',
            'count' => '10',
          ],
          [
            'href' => '#',
            'text' => 'Oggettistica',
            'title' => 'Oggettistica',
            'count' => '10',
          ],
          [
            'href' => '#',
            'text' => 'Tutte',
            'title' => 'Tutte',
            'count' => '10',
          ],
        ],
      ],
      [
        'title' => 'Collezioni',
        'items' => [
          [
            'href' => '#',
            'text' => 'Intramontabili',
            'title' => 'Intramontabili',
            'count' => '10',
          ],
          [
            'href' => '#',
            'text' => 'Ricordi',
            'title' => 'Ricordi',
            'count' => '10',
          ],
          [
            'href' => '#',
            'text' => 'Contaminazioni',
            'title' => 'Contaminazioni',
            'count' => '10',
          ],
          [
            'href' => '#',
            'text' => 'Chirigori',
            'title' => 'Chirigori',
            'count' => '10',
          ],
          [
            'href' => '#',
            'text' => 'Serpente',
            'title' => 'Serpente',
            'count' => '10',
          ],
          [
            'href' => '#',
            'text' => 'Simply D',
            'title' => 'Simply D',
            'count' => '10',
          ],
          [
            'href' => '#',
            'text' => 'Sguardi',
            'title' => 'Sguardi',
            'count' => '10',
          ],
          [
            'href' => '#',
            'text' => 'Twiggy',
            'title' => 'Twiggy',
            'count' => '10',
          ],
          [
            'href' => '#',
            'text' => 'Tutte',
            'title' => 'Tutte',
            'count' => '10',
          ],
        ],
      ],
    ];
  }

  public function sort_options() {
    return [
      'price_desc' => 'Prezzo decrescente',
      'price_asc' => 'Prezzo crescente',
      'latest' => 'Ultimi arrivi',
    ];
  }

  public function products() {
    return array_fill(0, 12, [
      'image' => [
        'src' => 'http://www.placehold.it/290x290',
        'alt' => '',
      ],
      'image_mobile' => [
        'src' => 'http://www.placehold.it/710x710',
        'alt' => '',
      ],
      'title' => 'Product name',
      'price' => '170,00',
      'info' => 'Tempi di realizzazione: 15 giorni lavorativi',
      'link' => [
        'href' => '#',
        'text' => 'Guarda i dettagli',
        'title' => 'Product name',
      ],
      'wishlist_link' => [
        'href' => '#',
        'text' => 'Aggiungi alla whishlist',
        'title' => 'Aggiungi alla whishlist',
      ],
    ]);
  }

  public function load_more_link() {
    return [
      'href' => '#',
      'text' => 'Carica altri prodotti',
      'title' => 'Carica altri prodotti',
    ];
  }

}
