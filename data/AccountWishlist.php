<?php
require_once('lib/DataProvider.php');

class AccountWishlist extends DataProvider {

  public function title() {
    return 'Wishlist';
  }

  public function sub_title() {
    return sprintf('Hai %d articoli nella tua wishlist', 3);
  }

  public function wishlist_table() {
    return [
      'body' => [
        [
          [
            'image' => [
            'src' => 'http://www.placehold.it/140x140/ffffff',
            'alt' => ''
            ],
            'image_mobile' => [
              'src' => 'http://www.placehold.it/696x696/ffffff',
              'alt' => ''
            ],
          ],
          [
            'name' => 'Earl Oliver',
            'details' => 'Tegcu avulolos etegota nop hadol wo wehud sun ke izuto ak dur bewvajo onoavo nafa.',
          ],
          [
            'add_to_chart' => [
              'href' => '#',
              'text' => 'Aggiungi alla shopping bag',
              'title' => 'Aggiungi alla shopping bag',
            ],
            'remove' => [
              'href' => '#',
              'text' => 'Elimina',
              'title' => 'Elimina',
            ],
          ]
        ],
        [
          [
            'image' => [
              'src' => 'http://www.placehold.it/140x140/ffffff',
              'alt' => ''
            ],
            'image_mobile' => [
              'src' => 'http://www.placehold.it/696x696/ffffff',
              'alt' => ''
            ],
          ],
          [
            'name' => 'Earl Oliver',
            'details' => 'Tegcu avulolos etegota nop hadol wo wehud sun ke izuto ak dur bewvajo onoavo nafa.',
          ],
          [
            'add_to_chart' => [
              'href' => '#',
              'text' => 'Aggiungi alla shopping bag',
              'title' => 'Aggiungi alla shopping bag',
            ],
            'remove' => [
              'href' => '#',
              'text' => 'Elimina',
              'title' => 'Elimina',
            ],
          ]
        ],
        [
          [
            'image' => [
              'src' => 'http://www.placehold.it/140x140/ffffff',
              'alt' => ''
            ],
            'image_mobile' => [
              'src' => 'http://www.placehold.it/696x696/ffffff',
              'alt' => ''
            ],
          ],
          [
            'name' => 'Earl Oliver',
            'details' => 'Tegcu avulolos etegota nop hadol wo wehud sun ke izuto ak dur bewvajo onoavo nafa.',
          ],
          [
            'add_to_chart' => [
              'href' => '#',
              'text' => 'Aggiungi alla shopping bag',
              'title' => 'Aggiungi alla shopping bag',
            ],
            'remove' => [
              'href' => '#',
              'text' => 'Elimina',
              'title' => 'Elimina',
            ],
          ]
        ],
      ]
    ];
  }

  public function share_label() {
    return 'Condividi la wishlist';
  }

  public function share_item_label() {
    return 'Condividi';
  }

}
