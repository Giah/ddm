<?php
require_once('lib/DataProvider.php');

class Contact extends DataProvider {

  public function jumbotron() {
    return [
      'images' => [
        [
          'src' => 'http://www.placehold.it/1180x220',
          'alt' => '',
        ],
      ],
      'title' => 'Contatti',
    ];
  }

  public function title() {
    return 'La nostra customer line e sempre pronta a rispondere ad ogni domanda riguardante prodotti, servizi ed informazioni in generale';
  }

  public function sub_title() {
    return 'Compila il form e verrai ricontattato';
  }

  public function form() {
    return [
      'action' => '#',
      'name' => [
        'label' => 'Nome',
        'name' => 'name',
      ],
      'surname' => [
        'label' => 'Cognome',
        'name' => 'surname',
      ],
      'email' => [
        'label' => 'E-mail',
        'name' => 'email',
      ],
      'privacy' => [
        'label' => 'Accetto la politica sulla privacy',
        'name' => 'privacy',
      ],
      'newsletter' => [
        'label' => 'Voglio iscrivermi alla newsletter DDM',
        'name' => 'newsletter',
      ],
      'message' => [
        'label' => 'Scrivi qui il tuo messaggio',
        'name' => 'message',
      ],
      'submit' => [
        'name' => 'submit',
        'value' => 'Invia la richiesta di contatto'
      ],
    ];
  }

  public function stores_label() {
    return 'I nostri negozi';
  }

  public function cards() {
    return [
      $this->card(),
      $this->card(),
      $this->card(),
    ];
  }

  public function card() {
    return [
      'image' => [
        'src' => 'http://www.placehold.it/775x515',
        'alt' => '',
      ],
      'name' => 'Olive Mitchell',
      'address' => '817 Uwanu Avenue - 54044 Rijtera',
      'tel' => [
        'label' => 'Tel',
        'value' => '(547) 811-8575'
      ],
      'hours' => [
        'label' => 'Orari',
        'value' => '<p>Monday: 99:99 - 99:99</p>Monday: 99:99 - 99:99</p>Monday: 99:99 - 99:99</p>'
      ],
      'closed' => [
        'label' => 'Chiusura estiva',
        'value' => 'from Fri May 01 2020 to Thu Jul 17 2064'
      ],
    ];
  }

}
