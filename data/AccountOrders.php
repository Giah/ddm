<?php
require_once('lib/DataProvider.php');

class AccountOrders extends DataProvider {

  public function title() {
    return 'I miei ordini';
  }

  public function orders_table() {
    return [
      'head' => [
        'N. Ordine',
        'Data',
        'Totale',
        'Stato',
        'Info',
      ],
      'body' => [
        [
          '#444155',
          '01/01/2017',
          '100',
          'Effettuato',
          [
            'href' => '#',
            'text' => 'Dettagli',
            'title' => 'Dettagli',
          ]
        ],
        [
          '#444155',
          '01/01/2017',
          '100',
          'Effettuato',
          [
            'href' => '#',
            'text' => 'Dettagli',
            'title' => 'Dettagli',
          ]
        ],
        [
          '#444155',
          '01/01/2017',
          '100',
          'Effettuato',
          [
            'href' => '#',
            'text' => 'Dettagli',
            'title' => 'Dettagli',
          ]
        ],
      ]
    ];
  }

}
