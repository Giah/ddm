<?php
require_once('lib/DataProvider.php');

class Common extends DataProvider {

	public function paths() {
    return [
      'base_url' => './',
      'public' => './public',
      'images' => 'images/',
      'stylesheets' => 'stylesheets/',
      'javascripts' => 'javascripts/',
      'template_parts' => 'template_parts',
      'rev_manifest' => 'rev-manifest.json',
    ];
  }

	public function head() {
    // return output_buffer_contents('get_header');
  }

	public function main_menu() {
		return [
      [
        'href' => '#',
        'text' => 'Gioielli',
        'title' => 'Gioielli',
        'classes' => [],
        'dropdown' => [
          [
            'title' => '',
            'classes' => [],
            'items' => [
              [
                'href' => '#',
                'text' => 'Anelli',
                'title' => 'Anelli',
                'classes' => [],
              ],
              [
                'href' => '#',
                'text' => 'Bracciali',
                'title' => 'Bracciali',
                'classes' => [],
              ],
              [
                'href' => '#',
                'text' => 'Collane e pendenti',
                'title' => 'Collane e pendenti',
                'classes' => [],
              ],
              [
                'href' => '#',
                'text' => 'Orecchini',
                'title' => 'Orecchini',
                'classes' => [],
              ],
              [
                'href' => '#',
                'text' => 'Ciondoli',
                'title' => 'Ciondoli',
                'classes' => [],
              ],
              [
                'href' => '#',
                'text' => 'Spille',
                'title' => 'Spille',
                'classes' => [],
              ],
              [
                'href' => '#',
                'text' => 'Accessori',
                'title' => 'Accessori',
                'classes' => [],
              ],
              [
                'href' => '#',
                'text' => 'Accessori uomo',
                'title' => 'Accessori uomo',
                'classes' => [],
              ],
              [
                'href' => '#',
                'text' => 'Oggettistica',
                'title' => 'Oggettistica',
                'classes' => [],
              ],
              [
                'href' => '#',
                'text' => 'Vedi tutto',
                'title' => 'Vedi tutto',
                'classes' => ['small', 'more'],
              ],
            ]
          ],
          [
            'title' => 'Stile',
            'classes' => ['desktopOnly'],
            'items' => [
              [
                'href' => '#',
                'text' => 'Romantica',
                'title' => 'Romantica',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Glamour',
                'title' => 'Glamour',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Classica',
                'title' => 'Classica',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Bon Ton',
                'title' => 'Bon Ton',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Rock',
                'title' => 'Rock',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Fashionista',
                'title' => 'Fashionista',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Vedi tutto',
                'title' => 'Vedi tutto',
                'classes' => ['small', 'more'],
              ],
            ]
          ],
          [
            'title' => 'Momenti',
            'classes' => ['desktopOnly'],
            'items' => [
              [
                'href' => '#',
                'text' => 'Diciottesimo',
                'title' => 'Diciottesimo',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Laurea',
                'title' => 'Laurea',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Matrimonio',
                'title' => 'Matrimonio',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Anniversario',
                'title' => 'Anniversario',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Cocktail',
                'title' => 'Cocktail',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Ricorrenze bimbe',
                'title' => 'Ricorrenze bimbe',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Vedi tutto',
                'title' => 'Vedi tutto',
                'classes' => ['small', 'more'],
              ],
            ]
          ],
          [
            'title' => 'Filtra per',
            'classes' => ['desktopOnly'],
            'items' => [
              [
                'href' => '#',
                'text' => 'Novità',
                'title' => 'Novità',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Out of Collection',
                'title' => 'Out of Collection',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Colore',
                'title' => 'Colore',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Metallo',
                'title' => 'Metallo',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Pietra',
                'title' => 'Pietra',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Prezzo',
                'title' => 'Prezzo',
                'classes' => ['small'],
              ],
            ]
          ]
        ],
      ],
      [
        'href' => '#',
        'text' => 'Collezioni',
        'title' => 'Collezioni',
        'classes' => [],
        'dropdown' => [
          [
            'title' => '',
            'classes' => [],
            'items' => [
              [
                'href' => '#',
                'text' => 'Intramontabili',
                'title' => 'Intramontabili',
                'classes' => [],
              ],
              [
                'href' => '#',
                'text' => 'Ricordi',
                'title' => 'Ricordi',
                'classes' => [],
              ],
              [
                'href' => '#',
                'text' => 'Contaminazioni',
                'title' => 'Contaminazioni',
                'classes' => [],
              ],
              [
                'href' => '#',
                'text' => 'Ghirigori',
                'title' => 'Ghirigori',
                'classes' => [],
              ],
              [
                'href' => '#',
                'text' => 'Serpente',
                'title' => 'Serpente',
                'classes' => [],
              ],
              [
                'href' => '#',
                'text' => 'Simply D',
                'title' => 'Simply D',
                'classes' => [],
              ],
              [
                'href' => '#',
                'text' => 'Sguardi',
                'title' => 'Sguardi',
                'classes' => [],
              ],
              [
                'href' => '#',
                'text' => 'Twiggy',
                'title' => 'Twiggy',
                'classes' => [],
              ],
              [
                'href' => '#',
                'text' => 'Vedi tutto',
                'title' => 'Vedi tutto',
                'classes' => ['small', 'more'],
              ],
            ]
          ],
          [
            'title' => 'Stile',
            'classes' => ['desktopOnly'],
            'items' => [
              [
                'href' => '#',
                'text' => 'Romantica',
                'title' => 'Romantica',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Glamour',
                'title' => 'Glamour',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Classica',
                'title' => 'Classica',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Bon Ton',
                'title' => 'Bon Ton',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Rock',
                'title' => 'Rock',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Fashionista',
                'title' => 'Fashionista',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Vedi tutto',
                'title' => 'Vedi tutto',
                'classes' => ['small', 'more'],
              ],
            ]
          ],
          [
            'title' => 'Momenti',
            'classes' => ['desktopOnly'],
            'items' => [
              [
                'href' => '#',
                'text' => 'Diciottesimo',
                'title' => 'Diciottesimo',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Laurea',
                'title' => 'Laurea',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Matrimonio',
                'title' => 'Matrimonio',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Anniversario',
                'title' => 'Anniversario',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Cocktail',
                'title' => 'Cocktail',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Ricorrenze bimbe',
                'title' => 'Ricorrenze bimbe',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Vedi tutto',
                'title' => 'Vedi tutto',
                'classes' => ['small', 'more'],
              ],
            ]
          ],
          [
            'title' => 'Filtra per',
            'classes' => ['desktopOnly'],
            'items' => [
              [
                'href' => '#',
                'text' => 'Novità',
                'title' => 'Novità',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Out of Collection',
                'title' => 'Out of Collection',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Colore',
                'title' => 'Colore',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Metallo',
                'title' => 'Metallo',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Pietra',
                'title' => 'Pietra',
                'classes' => ['small'],
              ],
              [
                'href' => '#',
                'text' => 'Prezzo',
                'title' => 'Prezzo',
                'classes' => ['small'],
              ],
            ]
          ],
        ],
      ],
      [
        'href' => '#',
        'text' => 'DDM World',
        'title' => 'DDM World',
        'classes' => [],
        'dropdown' => [
          [
            'title' => 'Il Marchio',
            'classes' => [],
            'items' => [
              [
                'href' => '#',
                'text' => '',
                'title' => '',
                'classes' => ['small'],
                'image' => [
                  'src' => 'http://placehold.it/300x150',
                  'alt' => 'Il Marchio'
                ],
              ]
            ]
          ],
          [
            'title' => 'I materiali',
            'classes' => [],
            'items' => [
              [
                'href' => '#',
                'text' => '',
                'title' => '',
                'classes' => ['small'],
                'image' => [
                  'src' => 'http://placehold.it/300x150',
                  'alt' => 'I materiali'
                ],
              ],
            ]
          ],
        ],
      ],
      [
        'href' => '#',
        'text' => 'Store Locator',
        'title' => 'Store Locator',
        'classes' => [],
        'dropdown' => [
          [
            'title' => 'Atelier',
            'classes' => [],
            'items' => [
              [
                'href' => '#',
                'text' => 'Via dei Piatti, 9 | 20123 Milano',
                'title' => 'Via dei Piatti, 9 | 20123 Milano',
                'classes' => ['small'],
                'image' => [
                  'src' => 'http://placehold.it/150x150',
                  'alt' => 'Atelier'
                ],
              ]
            ]
          ],
          [
            'title' => 'Atelierino',
            'classes' => [],
            'items' => [
              [
                'href' => '#',
                'text' => 'Via Pontaccio, 2 - 20121 - Milano',
                'title' => 'Via Pontaccio, 2 - 20121 - Milano',
                'classes' => ['small'],
                'image' => [
                  'src' => 'http://placehold.it/150x150',
                  'alt' => 'Atelierino'
                ],
              ],
            ]
          ],
          [
            'title' => 'Boutique',
            'classes' => [],
            'items' => [
              [
                'href' => '#',
								'text' => 'Via dei Piatti, 9 | 20123 Milano',
                'title' => 'Via dei Piatti, 9 | 20123 Milano',
                'classes' => ['small'],
                'image' => [
                  'src' => 'http://placehold.it/150x150',
                  'alt' => 'Boutique'
                ],
              ],
            ]
          ],
					[
            'title' => 'Filtra per',
            'classes' => ['desktopOnly'],
            'items' => [
              [
                'href' => '#',
                'text' => 'Vedi tutti i rivenditori',
                'title' => 'Vedi tutti i rivenditori',
                'classes' => ['small'],
              ],
            ]
          ]
        ],
      ],
      [
        'href' => '#',
        'text' => 'Contatti',
        'title' => 'Contatti',
        'classes' => [],
      ],
      [
        'href' => '#',
        'text' => 'DDMagazine',
        'title' => 'DDMagazine',
        'classes' => [],
      ],
		];
	}

  public function account_menu() {
    return [
      'title' => 'Il mio account',
      'items' => [
        [
          'id' => 'account-menu-1',
          'href' => '#',
          'text' => 'Overview',
          'title' => 'Overview',
        ],
        [
          'id' => 'account-menu-2',
          'href' => '#',
          'text' => 'Wishlist',
          'title' => 'Wishlist',
        ],
        [
          'id' => 'account-menu-3',
          'href' => '#',
          'text' => 'Dati di registrazione',
          'title' => 'Dati di registrazione',
        ],
        [
          'id' => 'account-menu-4',
          'href' => '#',
          'text' => 'Indirizzi',
          'title' => 'Indirizzi',
        ],
        [
          'id' => 'account-menu-5',
          'href' => '#',
          'text' => 'I miei ordini',
          'title' => 'I miei ordini',
        ],
      ]
    ];
  }

	public function footer() {
    return [
      'newsletter_title' => 'Rimani sempre aggiornato',
      'newsletter_link' => [
        'text' => 'Iscriviti alla newsletter',
        'title' => 'Iscriviti alla newsletter',
        'href' => '#',
      ],
      'newsletter_form' => [
        'action' => '#',
        'name' => [
          'label' => 'Nome e Cognome',
          'name' => 'name',
          'value' => '',
        ],
        'email' => [
          'label' => 'E-mail',
          'name' => 'email',
          'value' => '',
        ],
        'privacy' => [
          'label' => 'Accetto la politica sulla privacy',
          'name' => 'privacy',
        ],
        'submit' => [
          'name' => 'submit',
          'value' => 'Invio',
        ],
      ],
      'store_locator_title' => 'Trova la boutique più vicina a te',
      'store_locator_link' => [
        'text' => 'Vai allo store locator',
        'title' => 'Vai allo store locator',
        'href' => '#',
      ],
      'size_guide_title' => 'Guida alle taglie',
      'size_guide_link' => [
        'text' => '',
        'title' => 'Guida alle taglie',
        'href' => '#',
      ],
      'shipping_title' => 'Spedizioni',
      'shipping_link' => [
        'text' => '',
        'title' => 'Spedizioni',
        'href' => '#',
      ],
      'return_title' => 'Resi',
      'return_link' => [
        'text' => '',
        'title' => 'Resi',
        'href' => '#',
      ],
      'payments_title' => 'Pagamenti',
      'payments_link' => [
        'text' => '',
        'title' => 'Pagamenti',
        'href' => '#',
      ],
      'text' => 'DDM SRL: Via dei Piatti, 9 - 20123 Milano, Italy - Tel. +39 02 8699 5040 - info@danielademarchi.it - P.iva 06722440960',
      'copyright' => 'copyright DDM 2016',
      'menu' => [
        [
          'text' => 'Politica sulla privacy',
          'title' => 'Politica sulla privacy',
          'href' => '#',
        ], [
          'text' => 'Cookies',
          'title' => 'Cookies',
          'href' => '#',
        ], [
          'text' => 'Website by Bryan',
          'title' => 'Website by Bryan',
          'href' => '#',
        ]
      ],
      'share_label' => 'Seguici su'
    ];
  }

	public function currency() {
    return '€';
  }

	public function chart_preview() {
    return [
      'link' => [
        'href' => '#',
        'text' => 'Carrello',
        'title' => 'Carrello',
      ],
      'items' => 2,
    ];
  }

	public function wishlist_preview() {
    return [
      'link' => [
        'href' => '#',
        'text' => 'Wishlist',
        'title' => 'Wishlist',
      ],
      'items' => 2,
    ];
  }

	public function resellers_link() {
    return [
      'href' => '#',
      'text' => 'Area rivenditori',
      'title' => 'Area rivenditori',
    ];
  }

	public function promo_shipping() {
    return 'Spedizione gratuita oltre 100E';
  }

	public function login_link() {
    return [
      'text' => 'Accedi o crea account',
      'title' => 'Accedi o crea account',
      'href' => '#'
    ];
  }

	public function logout_link() {
    return [
      'text' => 'Logout',
      'title' => 'Logout',
      'href' => '#'
    ];
  }

}
