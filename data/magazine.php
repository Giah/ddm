<?php
require_once('lib/DataProvider.php');

class Magazine extends DataProvider {
  public function jumbotron() {
    return [
      'images' => [
        [
          'src' => 'http://www.placehold.it/1180x220',
          'alt' => '',
        ],
      ],
      'title' => 'DDMagazine',
    ];
  }

  public function news() {
    $news = [];

    for ($i = 0; $i < 21; $i++) {
      $news[] = [
        'image' => [
          'src' => 'http://www.placehold.it/374x374',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'category' => $this->_faker->words(1, true),
        'date' => $this->_faker->date('d/m/Y', '2030-12-31'),
        'title' => $this->_faker->name,
        'sub_title' => $this->_faker->words(3, true),
        'link' => [
          'href' => '#',
          'text' => '',
          'title' => '',
        ]
      ];
    }

    return $news;
  }

}
