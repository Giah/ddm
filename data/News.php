<?php
require_once('lib/DataProvider.php');

class News extends DataProvider {

  public function images() {
    $images = [];
    for ($i = 0; $i < 3; $i++) {
      $images[] = [
        'src' => 'http://www.placehold.it/710x' . $this->_faker->numberBetween(500, 1000),
        'alt' => '',
      ];
    }
    return $images;
  }

  public function category() {
    return $this->_faker->words(1, true);
  }

  public function date() {
    return $this->_faker->date('d/m/Y', '2030-12-31');
  }

  public function title() {
    return $this->_faker->name;
  }

  public function sub_title() {
    return $this->_faker->words(3, true);
  }

  public function content() {
    return '<p>' . implode($this->_faker->paragraphs(3), '</p><p>') . '</p>';
  }

  public function link() {
    return [
      'href' => '#',
      'text' => '',
      'title' => '',
    ];
  }

}
