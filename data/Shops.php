<?php
require_once('lib/DataProvider.php');

class Shops extends DataProvider {

  public function stores() {
    return [
      $this->store(),
      $this->store(),
      $this->store(),
    ];
  }

  public function store() {
    return [
      'images' => [
        [
          'src' => 'http://www.placehold.it/910x602',
          'alt' => '',
        ],
        [
          'src' => 'http://www.placehold.it/367x367',
          'alt' => '',
        ],
        [
          'src' => 'http://www.placehold.it/367x367',
          'alt' => '',
        ],
        [
          'src' => 'http://www.placehold.it/367x367',
          'alt' => '',
        ],
      ],
      'name' => 'Olive Mitchell',
      'address' => '817 Uwanu Avenue - 54044 Rijtera',
      'description' => '<p>Pranzate ma chirurgo rovescio la. Palme mi dimmi so ti fatto lieve. Ornamento mutamenti curiosita ora risolvere tuo disegnata. Splendore oh parentela inclinato fu. Malato domani tempia col aquila potrai aurora ore.Ri acifuwug ugbo daszolle dowrobrif ped bit se sut wicuomi or id camduco ezudur suh il. Et obuwugso moos avejez winolu kinuh wakvejej wi tufid suc fowiw pucuta tad dofcoc sa gozabig.</p>
                        <p><strong>Onde oggi teco suoi ha ah care sola. Supplico avessero in el sommessa oscurato profondo spezzare.</strong></p>
                        <p>Soffro ve lo difesa di volevi accesa al. Grappoli spremere tua era amarezza campagna nel.Sanan guivu wisarzeb nulgi ra tingotis mirunpo mukdacuj pi wi ojefivzez ihifunba.Ojvij tawaduf sogpi ge cozceuze he wuzezufim vowup setpu kachum cef con kir ceropzu waso zervo nuasno.</p>',
    ];
  }

}
