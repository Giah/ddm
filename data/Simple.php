<?php
require_once('lib/DataProvider.php');

class Simple extends DataProvider {

  public function content() {
    $text = '<h1>' . $this->_faker->sentence() . '</h1>';;
    for ($i = 0; $i < 10; $i++) {
      $text .= '<h2>' . $this->_faker->sentence() . '</h2>';
      $text .= '<p>' . $this->_faker->sentence(100) . '</p>';
    }
    return $text;
  }

}
