<?php
require_once('lib/DataProvider.php');

class Registration extends DataProvider {

  public function title() {
    return 'Registrazione';
  }

  public function sub_title() {
    return 'Con un account presso DDM, il checkout è più veloce, l\'accesso al carrello degli acquisti
            e la visualizzazione della cronologia ordini sono possibili da qualsiasi dispositivo';
  }

  public function form() {
    return [
      'action' => '#',
      'name' => [
        'label' => 'Nome',
        'name' => 'name',
        'value' => '',
      ],
      'surname' => [
        'label' => 'Cognome',
        'name' => 'surname',
        'value' => '',
      ],
      'email' => [
        'label' => 'E-mail',
        'name' => 'email',
        'value' => '',
      ],
      'password' => [
        'label' => 'Password',
        'name' => 'password',
        'value' => '',
      ],
      'gender' => [
        'label' => 'Sesso',
        [
          'name' => 'gender',
          'value' => 'female',
          'label' => 'Femmina',
        ],
        [
          'name' => 'gender',
          'value' => 'male',
          'label' => 'Maschio',
        ],
      ],
      'date' => [
        'day' => [
          'label' => 'Data di nascita',
          'options' => array_map(function($i) {
            if ($i === 0) {
              return [
                'value' => '',
                'text' => 'Giorno'
              ];
            }
            return [
              'value' => $i,
              'text' => $i
            ];
          }, range(0, 31))
        ],
        'month' => [
          'label' => 'Data di nascita',
          'options' => array_map(function($i) {
            $months = ['Mese', 'Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugnio', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'];
            return [
              'value' => $i,
              'text' => $months[$i]
            ];
          }, range(0, 12))
        ],
      ],
      'privacy' => [
        'label' => 'Accetto la politica sulla privacy',
        'name' => 'privacy',
      ],
      'submit' => [
        'name' => 'submit',
        'value' => 'Crea account',
      ],
    ];
  }

  public function register() {
    return [
      'title' => 'Registrazione',
      'subtitle' => 'Non sei ancora registrato? Iscriviti ed entra nel mondo Daniela De Marchi',
      'link' => [
        'href' => '#',
        'text' => 'Registrati',
        'title' => 'Registrati',
      ]
    ];
  }

}
