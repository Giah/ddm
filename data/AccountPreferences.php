<?php
require_once('lib/DataProvider.php');

class AccountPreferences extends DataProvider {

  public function title() {
    return 'Preferenze notifiche';
  }

  public function sub_title() {
    return 'In questa sezione potrai decidere dove ricevere le nostre comunicazioni';
  }

  public function form() {
    return [
      'action' => '#',
      'promo-notifications' => [
        'header' => [
          'title' => 'Messaggi promozionali',
          'sub_title' => 'Come desideri ricevere questo tipo di comunicazione?',
        ],
        'promo-notify-email' => [
          'name' => 'promo-notify-email',
          'label' => 'Via e-mail',
        ],
        'promo-notify-sms' => [
          'name' => 'promo-notify-sms',
          'label' => 'Via SMS',
        ],
      ],
      'shop-notifications' => [
        'header' => [
          'title' => 'Messaggi Shop On Line',
          'sub_title' => 'Come desideri ricevere questo tipo di comunicazione?',
        ],
        'shop-notify-email' => [
          'name' => 'shop-notify-email',
          'label' => 'Via e-mail',
        ],
        'shop-notify-sms' => [
          'name' => 'shop-notify-sms',
          'label' => 'Via SMS',
        ],
      ],
      'tel' => [
        'name' => 'tel',
        'label' => 'Numero Cellulare',
      ],
      'submit' => [
        'name' => 'submit',
        'value' => 'Salva preferenze'
      ],
    ];
  }

}
