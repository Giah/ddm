<?php
require_once('lib/DataProvider.php');

class ShopEmotional extends DataProvider {

  public function jumbotron() {
    return [
      'images' => [
        [
          'src' => 'http://www.placehold.it/1180x220',
          'alt' => '',
        ],
      ],
      'title' => 'DDMagazine',
    ];
  }

  public function form() {
    return [
      [
        'background' => [
          'desktop' => 'http://unsplash.it/1280?image=1027',
          'tablet' => 'http://unsplash.it/1023?image=1027',
          'smartphone' => 'http://unsplash.it/767?image=1027',
        ],
        'question' => 'Stai cercando un <br/> gioiello per te o per una <br/> persona speciale?',
        'inputs' => [
          [ 'type'=>'button', 'text' => 'È per me', ],
          [ 'type'=>'button', 'text' => 'È un regalo', ]
        ]
      ],
      [
        'background' => [
          'desktop' => 'http://unsplash.it/1280?image=978',
          'tablet' => 'http://unsplash.it/1023?image=978',
          'smartphone' => 'http://unsplash.it/767?image=978',
        ],
        'question' => 'L\'età non è un numero, <br/> ma uno stato d\'animo. <br/> Tu quanti anni hai?',
        'inputs' => [
          [
            'type'=>'select',
            'options' => [
              [
                'value' => '',
                'text' => 'Seleziona la tua fascia di età',
                'selected' => 'selected',
              ],
              [
                'value' => '0-15',
                'text' => 'Da 0 a 15 anni',
              ],
              [
                'value' => '15-30',
                'text' => 'Da 15 a 30 anni',
              ],
              [
                'value' => '30-50',
                'text' => 'Da 30 a 50 anni',
              ],
              [
                'value' => '50+',
                'text' => 'Piu di 50 anni',
              ],
            ],
          ],
          [ 'type'=>'button', 'text' => 'Avanti', ]
        ]
      ],
      [
        'background' => [
          'desktop' => 'http://unsplash.it/1280?image=996',
          'tablet' => 'http://unsplash.it/1023?image=996',
          'smartphone' => 'http://unsplash.it/767?image=996',
        ],
        'question' => 'Rock, Bon Ton, Glamour,<br/> Classica, Romantica.<br/> Qual\'e il tuo stile?',
        'inputs' => [
          [
            'type'=>'select',
            'options' => [
              [
                'value' => '',
                'text' => 'Seleziona il tuo stile',
                'selected' => 'selected',
              ],
              [ 'value' => 'Rock', 'text' => 'Rock', ],
              [ 'value' => 'Bon-Ton', 'text' => 'Bon-Ton', ],
              [ 'value' => 'Glamour', 'text' => 'Glamour', ],
              [ 'value' => 'Classica', 'text' => 'Classica', ],
              [ 'value' => 'Romatica', 'text' => 'Classica', ],
            ],
          ],
          [ 'type'=>'button', 'text' => 'Avanti', ]
        ]
      ],
    ];
  }

}
