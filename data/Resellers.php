<?php
require_once('lib/DataProvider.php');

class Resellers extends DataProvider {

  public function jumbotron() {
    return [
      'images' => [
        [
          'src' => 'http://www.placehold.it/1180x220',
          'alt' => '',
        ],
      ],
      'title' => 'I nostri rivenditori',
    ];
  }

  public function title() {
    return 'Trova il rivenditore Daniela De Marchi piu vicino a te';
  }

  public function sub_title() {
    return 'Seleziona la tua città';
  }

  public function form() {
    $cities = [];
    for ($i = 0; $i < 100; $i ++) {
      $cities[] = [
        'value' => $this->_faker->city,
        'text' => $this->_faker->city,
        'selected' => false,
      ];
    }

    return [
      'action' => '#',
      'cities' => $cities,
      'submit' => [
        'name' => 'submit',
        'value' => 'Cerca rivenditori'
      ],
    ];
  }

  public function submit_label() {
    return 'Cerca rivenditori';
  }

  public function results() {
    return [
      [
        'name' => 'Jeffery Gordon',
        'address' => '1488 Aciuhu Square',
        'cap' => '71457',
        'city' => 'Nifkavwo',
        'prov' => 'NC',
        'tel' => '(761) 224-3806',
      ],
      [
        'name' => 'Jeffery Gordon',
        'address' => '1488 Aciuhu Square',
        'cap' => '71457',
        'city' => 'Nifkavwo',
        'prov' => 'NC',
        'tel' => '(761) 224-3806',
      ],
      [
        'name' => 'Jeffery Gordon',
        'address' => '1488 Aciuhu Square',
        'cap' => '71457',
        'city' => 'Nifkavwo',
        'prov' => 'NC',
        'tel' => '(761) 224-3806',
      ],
      [
        'name' => 'Jeffery Gordon',
        'address' => '1488 Aciuhu Square',
        'cap' => '71457',
        'city' => 'Nifkavwo',
        'prov' => 'NC',
        'tel' => '(761) 224-3806',
      ],
      [
        'name' => 'Jeffery Gordon',
        'address' => '1488 Aciuhu Square',
        'cap' => '71457',
        'city' => 'Nifkavwo',
        'prov' => 'NC',
        'tel' => '(761) 224-3806',
      ],
      [
        'name' => 'Jeffery Gordon',
        'address' => '1488 Aciuhu Square',
        'cap' => '71457',
        'city' => 'Nifkavwo',
        'prov' => 'NC',
        'tel' => '(761) 224-3806',
      ],
      [
        'name' => 'Jeffery Gordon',
        'address' => '1488 Aciuhu Square',
        'cap' => '71457',
        'city' => 'Nifkavwo',
        'prov' => 'NC',
        'tel' => '(761) 224-3806',
      ],
    ];
  }

}
