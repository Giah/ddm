<?php
require_once('lib/DataProvider.php');

class Materials extends DataProvider {

  public function jumbotron() {
    return [
      'images' => [
        [
          'desktop' => [
            'src' => 'http://www.placehold.it/1180x550/874126',
            'alt' => '',
          ],
          'mobile' => [
            'src' => 'http://www.placehold.it/768x920/874126',
            'alt' => '',
          ],
        ]
      ],
      'title' => 'Materia e tecnica',
      'sub_title' => 'Azienda milanese nata dala forza dei suoi prodotti e dal sogno di una donna cha ha fatto della sua grande passione una professione.',
      'text' => '<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p> <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p> <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing</p>',
    ];
  }

  public function disclaimer() {
    return 'Non tutte le varianti di pietre e metalli sono disponibili per tutte i modelli.';
  }

  public function materials_list() {
    $materials = [];

    for ($i= 0; $i < 32; $i ++) {
      $materials[] = [
        'image' => [
          'src' => 'http://www.placehold.it/227x227/' . substr($this->_faker->hexcolor(), 1),
          'alt' => '',
        ],
        'title' => 'Foaguba merjis hokaifo',
        'link' => [
          'text' => 'Scopri tutti i prodotti',
          'title' => 'Scopri tutti i prodotti',
          'href' => '#',
        ]
      ];
    }

    return $materials;
  }
}
