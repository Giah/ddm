<?php
require_once('lib/DataProvider.php');

class Home extends DataProvider {

  public function jumbotron() {
    return [
      'images' => [
        [
          'desktop' => [
            'src' => 'http://www.placehold.it/1180x550/' . substr($this->_faker->hexcolor(), 1),
            'alt' => '',
          ],
          'mobile' => [
            'src' => 'http://www.placehold.it/768x920/' . substr($this->_faker->hexcolor(), 1),
            'alt' => '',
          ],
        ],
        [
          'desktop' => [
            'src' => 'http://www.placehold.it/1180x550/' . substr($this->_faker->hexcolor(), 1),
            'alt' => '',
          ],
          'mobile' => [
            'src' => 'http://www.placehold.it/768x920/' . substr($this->_faker->hexcolor(), 1),
            'alt' => '',
          ],
        ],
        [
          'desktop' => [
            'src' => 'http://www.placehold.it/1180x550/' . substr($this->_faker->hexcolor(), 1),
            'alt' => '',
          ],
          'mobile' => [
            'src' => 'http://www.placehold.it/768x920/' . substr($this->_faker->hexcolor(), 1),
            'alt' => '',
          ],
        ],
        [
          'desktop' => [
            'src' => 'http://www.placehold.it/1180x550/' . substr($this->_faker->hexcolor(), 1),
            'alt' => '',
          ],
          'mobile' => [
            'src' => 'http://www.placehold.it/768x920/' . substr($this->_faker->hexcolor(), 1),
            'alt' => '',
          ],
        ],
      ],
      'title' => 'Gioielli per ogni <br />personalità',
      'link' => [
        'href' => '#',
        'text' => 'Scopri il gioiello perfetto',
        'title' => 'Scopri il gioiello perfetto',
      ]
    ];
  }

  public function title() {
    return 'Gioielli artigianali made in Italy';
  }

  public function sub_title() {
    return 'Azienda milanese nata dalla forza dei suoi prodotti e dal sogno di una donna che ha fatto della sua grande passione una professione.';
  }

  public function reveal_boxes() {
    return [
      // shops
      [
        'image' => [
          'src' => 'http://www.placehold.it/740x425',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'title' => 'I nostri negozi',
        'sub_title' => 'Fejjucbe vemiope umowiseb',
        'link' => [
          'href' => '#',
          'text' => 'Scopri di più',
          'title' => 'Scopri di più',
        ]
      ],
      // new_products
      [
        'image' => [
          'src' => 'http://www.placehold.it/425x425',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'title' => 'Novità',
        'sub_title' => 'Fejjucbe vemiope umowiseb',
        'link' => [
          'href' => '#',
          'text' => 'Scopri di più',
          'title' => 'Scopri di più',
        ]
      ],
      // magazine
      [
        'image' => [
          'src' => 'http://www.placehold.it/425x425',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'title' => 'DDMagazine',
        'sub_title' => 'Fejjucbe vemiope umowiseb',
        'link' => [
          'href' => '#',
          'text' => 'Scopri di più',
          'title' => 'Scopri di più',
        ]
      ],
      // styles
      [
        'image' => [
          'src' => 'http://www.placehold.it/740x425',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'title' => 'Stili',
        'sub_title' => 'Fejjucbe vemiope umowiseb',
        'link' => [
          'href' => '#',
          'text' => 'Scopri di più',
          'title' => 'Scopri di più',
        ]
      ],
    ];
  }

  public function preview_boxes() {
    return [
      // collezioni
      [
        'image' => [
          'src' => 'http://www.placehold.it/280x280',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'title' => 'Collezioni',
        'sub_title' => 'Scopri i nuovi gioielli DDM',
        'link' => [
          'href' => '#',
          'text' => 'Shop now',
          'title' => 'Shop now',
        ]
      ],
      // anelli
      [
        'image' => [
          'src' => 'http://www.placehold.it/280x280',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'title' => 'Anelli',
        'sub_title' => 'Scopri tutti i Top Sellers DDM',
        'link' => [
          'href' => '#',
          'text' => 'Shop now',
          'title' => 'Shop now',
        ]
      ],
      // orecchini
      [
        'image' => [
          'src' => 'http://www.placehold.it/280x280',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'title' => 'Orecchini',
        'sub_title' => 'Scopri tutti i Top Sellers DDM',
        'link' => [
          'href' => '#',
          'text' => 'Shop now',
          'title' => 'Shop now',
        ]
      ],
      // collane
      [
        'image' => [
          'src' => 'http://www.placehold.it/280x280',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'title' => 'Collane',
        'sub_title' => 'Gioielli per un\'occasione unica',
        'link' => [
          'href' => '#',
          'text' => 'Shop now',
          'title' => 'Shop now',
        ]
      ],
    ];
  }

  public function materials() {
    return [
      'title' => 'Materia e tecnica',
      'sub_title' => 'La luce ed il colore dei materiali si fondono nelle forme di uno stile tra il moderno e l\'antico, dove il richiamo al mondo naturale è sempre presente',
      'link' => [
        'href' => '#',
        'text' => 'Scopri tutti i materiali',
        'title' => 'Scopri tutti i materiali',
      ]
    ];
  }

  public function materials_list() {
    $materials = [];

    for ($i= 0; $i < 32; $i ++) {
      $materials[] = [
        'image' => [
          'src' => 'http://www.placehold.it/227x227/' . substr($this->_faker->hexcolor(), 1),
          'alt' => '',
        ],
        'title' => 'Foaguba merjis hokaifo',
        'link' => [
          'text' => 'Scopri tutti i prodotti',
          'title' => 'Scopri tutti i prodotti',
          'href' => '#',
        ]
      ];
    }

    return $materials;
  }
}
?>
