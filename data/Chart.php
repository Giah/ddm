<?php
require_once('lib/DataProvider.php');

class Chart extends DataProvider {

  public function title() {
    return 'Shopping bag';
  }

  public function summary() {
    return sprintf('Hai %d articoli nella shopping bag', 3);
  }

  public function form() {
    return [
      'action' => '#',
      'remove' => [
        'value' => 'remove',
        'text' => 'Elimina'
      ],
      'shipping_recalc' => [
        'value' => 'recalc',
        'text' => 'Calcolo spedizione'
      ],
      'promo_recalc' => [
        'value' => 'recalc',
        'text' => 'Ricalcola'
      ],
      'submit' => [
        'value' => 'submit',
        'text' => 'Procedi all\'acquisto'
      ],
    ];
  }

  public function promo_labels() {
    return [
      'label' => 'Codice promozionale',
      'placeholder' => 'Inserisci codice promozionale',
    ];
  }

  public function recap() {
    return [
      'title' => 'Totale',
      'subtotal' => 'Subtotale',
      'total' => 'Totale',
      'shipping' => 'Spedizione',
      'shipping_alternate' => 'Vuoi spedire in un altra nazione?',
    ];
  }

  public function articles_table() {
    return [
      'head' => [
        'Prodotto',
        'Dettagli',
        'Quantità',
        'Totale',
        '',
      ],
      'body' => [
        [
          [
            'image' => [
              'src' => 'http://www.placehold.it/70x70/ffffff',
              'alt' => ''
            ],
            'image_mobile' => [
              'src' => 'http://www.placehold.it/428x428/ffffff',
              'alt' => ''
            ],
            'name' => 'Earl Oliver',
          ],
          'Tegcu avulolos etegota nop hadol wo wehud sun ke izuto ak dur bewvajo onoavo nafa.',
          1,
          170,
          ''
        ],
        [
          [
            'image' => [
              'src' => 'http://www.placehold.it/70x70/ffffff',
              'alt' => ''
            ],
            'image_mobile' => [
              'src' => 'http://www.placehold.it/428x428/ffffff',
              'alt' => ''
            ],
            'name' => 'Earl Oliver',
          ],
          'Tegcu avulolos etegota nop hadol wo wehud sun ke izuto ak dur bewvajo onoavo nafa.',
          1,
          170,
          ''
        ],
        [
          [
            'image' => [
              'src' => 'http://www.placehold.it/70x70/ffffff',
              'alt' => ''
            ],
            'image_mobile' => [
              'src' => 'http://www.placehold.it/428x428/ffffff',
              'alt' => ''
            ],
            'name' => 'Earl Oliver',
          ],
          'Tegcu avulolos etegota nop hadol wo wehud sun ke izuto ak dur bewvajo onoavo nafa.',
          1,
          170,
          ''
        ],
      ],
    ];
  }

  public function total_articles() {
    return 10;
  }

  public function subtotal() {
    $res = 100;
    return [
      'real' => $res,
      'formatted' => (string)$res // return a string formatted according to locale
    ];
  }

  public function shipping_cost() {
    $res = 7.5;
    return [
      'real' => $res,
      'formatted' => (string)$res // return a string formatted according to locale
    ];
  }

  public function total() {
    $sum = 100;
    $shipping_cost = 7.5;
    return $sum['real'] + $shipping_cost['real']; // return a string formatted according to locale
  }

}
