<?php
require_once('lib/DataProvider.php');

class AccountOverview extends DataProvider {

  public function title() {
    return 'Benvenuta';
  }

  public function sub_title() {
    return 'Dal tuo account puoi gestire i tuoi dati e le tue preferenze. Goditi lo shopping su Daniela De Marchi';
  }

  public function registration_header() {
    return 'Dati di registrazione';
  }

  public function registration_data() {
    return [
      [
        'label' => 'Nome',
        'content' => 'Kelley',
      ],
      [
        'label' => 'Cognome',
        'content' => 'Kelley',
      ],
      [
        'label' => 'Indirizzo e-mail',
        'content' => 'da@jizidu.gy',
      ],
      [
        'label' => 'Sesso',
        'content' => 'donna',
      ],
      [
        'label' => 'Data di nascita',
        'content' => '1 Marzo',
      ],
    ];
  }

  public function edit_registration_link() {
    return [
      'href' => '#',
      'text' => 'Modifica i tuoi dati',
      'title' => 'Modifica i tuoi dati',
    ];
  }

  public function orders_header() {
    return 'I miei ordini';
  }

  public function orders_table() {
    return [
      'head' => [
        'N. Ordine',
        'Data',
        'Totale',
        'Stato',
        'Info',
      ],
      'body' => [
        [
          '#444155',
          '01/01/2017',
          '100',
          'Effettuato',
          [
            'href' => '#',
            'text' => 'Dettagli',
            'title' => 'Dettagli',
          ]
        ],
        [
          '#444155',
          '01/01/2017',
          '100',
          'Effettuato',
          [
            'href' => '#',
            'text' => 'Dettagli',
            'title' => 'Dettagli',
          ]
        ],
        [
          '#444155',
          '01/01/2017',
          '100',
          'Effettuato',
          [
            'href' => '#',
            'text' => 'Dettagli',
            'title' => 'Dettagli',
          ]
        ],
      ]
    ];
  }

  public function show_orders_link() {
    return [
      'href' => '#',
      'text' => 'Vedi tutti gli ordini',
      'title' => 'Vedi tutti gli ordini',
    ];
  }

  public function wishlist_header() {
    return [
      'title' => 'Wishlist',
      'sub_title' => sprintf('Hai %d articoli nella tua wishlist', 3),
    ];
  }

  public function wishlist() {
    return [
      [
        'image' => [
          'src' => 'http://www.placehold.it/290x290/ffffff',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720/ffffff',
          'alt' => '',
        ],
        'title' => 'Fanny Diaz',
        'price' => '454,00',
        'link' => [
          'href' => '#',
          'text' => 'Aggiungi alla shopping bag',
          'title' => 'Aggiungi alla shopping bag',
        ],
      ],
      [
        'image' => [
          'src' => 'http://www.placehold.it/290x290/ffffff',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720/ffffff',
          'alt' => '',
        ],
        'title' => 'Fanny Diaz',
        'price' => '454,00',
        'link' => [
          'href' => '#',
          'text' => 'Aggiungi alla shopping bag',
          'title' => 'Aggiungi alla shopping bag',
        ],
      ],
      [
        'image' => [
          'src' => 'http://www.placehold.it/290x290/ffffff',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720/ffffff',
          'alt' => '',
        ],
        'title' => 'Fanny Diaz',
        'price' => '454,00',
        'link' => [
          'href' => '#',
          'text' => 'Aggiungi alla shopping bag',
          'title' => 'Aggiungi alla shopping bag',
        ],
      ],
    ];
  }

  public function show_wishlist_link() {
    return [
      'href' => '#',
      'text' => 'Vedi la tua wishlist',
      'title' => 'Vedi la tua wishlist',
    ];
  }

  public function share_label() {
    return 'Condividi la wishlist';
  }

}
