<?php
require_once('lib/DataProvider.php');

class Collections extends DataProvider {

  public function jumbotron() {
    return [
      'images' => [
        [
          'src' => 'http://www.placehold.it/1180x220',
          'alt' => '',
        ],
      ],
      'title' => 'Collezioni',
    ];
  }

  public function preview_boxes() {
    return [
      [
        'image' => [
          'src' => 'http://www.placehold.it/374x374',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'title' => 'Collezione',
        'sub_title' => 'Collezione SS 2016',
        'link' => [
          'href' => '#',
          'text' => '',
          'title' => '',
        ],
      ],
      [
        'image' => [
          'src' => 'http://www.placehold.it/374x374',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'title' => 'Collezione',
        'sub_title' => 'Collezione SS 2016',
        'link' => [
          'href' => '#',
          'text' => '',
          'title' => '',
        ],
      ],
      [
        'image' => [
          'src' => 'http://www.placehold.it/374x374',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'title' => 'Collezione',
        'sub_title' => 'Collezione SS 2016',
        'link' => [
          'href' => '#',
          'text' => '',
          'title' => '',
        ],
      ],
      [
        'image' => [
          'src' => 'http://www.placehold.it/374x374',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'title' => 'Collezione',
        'sub_title' => 'Collezione SS 2016',
        'link' => [
          'href' => '#',
          'text' => '',
          'title' => '',
        ],
      ],
      [
        'image' => [
          'src' => 'http://www.placehold.it/374x374',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'title' => 'Collezione',
        'sub_title' => 'Collezione SS 2016',
        'link' => [
          'href' => '#',
          'text' => '',
          'title' => '',
        ],
      ],
      [
        'image' => [
          'src' => 'http://www.placehold.it/374x374',
          'alt' => '',
        ],
        'image_mobile' => [
          'src' => 'http://www.placehold.it/720x720',
          'alt' => '',
        ],
        'title' => 'Collezione',
        'sub_title' => 'Collezione SS 2016',
        'link' => [
          'href' => '#',
          'text' => '',
          'title' => '',
        ],
      ],
    ];
  }
}
