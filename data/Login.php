<?php
require_once('lib/DataProvider.php');

class Login extends DataProvider {

  public function title() {
    return 'Accedi o crea account';
  }

  public function sub_title() {
    return 'Dal tuo account puoi gestire i tuoi dati, i tuoi irdini e le tue preferenze. Goditi lo shopping su Daniela De Marchi.';
  }

  public function form() {
    return [
      'title' => 'Utente registrato',
      'sub_title' => 'Se sei gia registrato, effettua il login.',
      'action' => '#',
      'email' => [
        'label' => 'E-mail',
        'name' => 'email',
        'value' => '',
      ],
      'password' => [
        'label' => 'Password',
        'name' => 'password',
        'value' => '',
      ],
      'password_recover' => [
        'href' => '#',
        'text' => 'Hai dimenticato la password?',
        'title' => 'Hai dimenticato la password?',
      ],
      'remember' => [
        'label' => 'Ricordami',
        'name' => 'remember',
      ],
      'submit' => [
        'name' => 'submit',
        'value' => 'Accedi al tuo account',
      ],
    ];
  }

  public function register() {
    return [
      'title' => 'Registrazione',
      'subtitle' => 'Non sei ancora registrato? Iscriviti ed entra nel mondo Daniela De Marchi',
      'link' => [
        'href' => '#',
        'text' => 'Registrati',
        'title' => 'Registrati',
      ]
    ];
  }

}
