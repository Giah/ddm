<?php
function get_template_name() {
  // return get_post_type($post);
  return $_GET['template'];
}

/**
 *
 * Wrap a function that output a string and return the output
 *
 * Example:
 *
 * $header = output_buffer_contents('get_header');
 * ...
 * ...
 * echo $header;
 *
 */
function output_buffer_contents($function, $args = array()){
  ob_start();
  $function($args);
  return ob_end_clean();
}
?>
