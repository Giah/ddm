/* global document: document */
/* global prefix: ddm */
import 'babel-polyfill';
import store from './store';
import ResizeObserver from './modules/ResizeObserver';
import getMedia from './modules/media';

import MainWrapper from './components/MainWrapper';

function storeMedia() {
  var media = getMedia(document.querySelector(`#${prefix}-mediaBreakpoints`));
  if (store.getState().media !== media) {
    store({
      media,
      type: 'MEDIA_CHANGE',
    });
  }
}

function init() {
  MainWrapper(store);
  setTimeout(storeMedia);
  const resizeObserver = new ResizeObserver(storeMedia);
  resizeObserver.start();
}

if (document.readyState === 'complete' || (document.readyState !== 'loading' && !document.documentElement.doScroll)) {
  init();
} else {
  document.addEventListener('DOMContentLoaded', init);
}
