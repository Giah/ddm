import createStore from 'store-emitter';

function reducer(action, state) {
  switch (action.type) {
    case 'MEDIA_CHANGE':
      return {
        ...state,
        media: action.media,
      };
    case 'SCROLL':
      return {
        ...state,
        scrollPos: action.scrollPos,
      };
    case 'TOGGLE_MOBILE_MENU':
      return {
        ...state,
        mobileMenuOpened: action.opened,
      };
    case 'TOGGLE_SHOP_FILTERS_MENU':
      return {
        ...state,
        shopFiltersMenuOpened: action.opened,
      };
    case 'DROP_DOWN_MENU_OPENED':
      return {
        ...state,
        DropDownMenuOpened: action.index,
      };
    case 'STORE_HEADER_HEIGHT':
      return {
        ...state,
        header_height: action.height,
      };
    case 'SELECT_PRODUCT_VARIANT':
      return {
        ...state,
        product_variant: action.product_variant,
      };
    case 'ADD_TO_SHOPPING_BAG':
      return {
        ...state,
        product: action.product,
      };
    default:
      return state;
  }
}

const store = createStore(reducer, {
  media: '',
  scrollPos: 0,
  DropDownMenuOpened: false,
  mobileMenuOpened: false,
  shopFiltersMenuOpened: false,
  header_height: 0,
  product_variant: false,
  product: {},
});

export default store;
