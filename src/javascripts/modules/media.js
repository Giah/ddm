export default function getMedia(el) {
  var els = el.querySelectorAll('div');
  for (let i = 0; i < els.length; i++) {
    const display = window.getComputedStyle(els[i]).getPropertyValue('display');
    if (display !== 'none') {
      return els[i].getAttribute('data-breakpoint-name');
    }
  }
}
