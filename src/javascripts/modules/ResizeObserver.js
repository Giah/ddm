export default class ResizeObserver {
    constructor(callback) {
        this.running = false;
        this.onResizeHandler = this.onResize.bind(this);
        this.callback = callback;
    }

    start() {
        this.running = false;
        window.addEventListener('resize', this.onResizeHandler);
    }

    stop() {
        window.removeEventListener('resize', this.onResizeHandler);
    }

    onResize() {
        if (!this.running) {
            this.running = true;
            window.requestAnimationFrame(() => {
                this.running = false;
                this.callback();
            });
        }
    }
}
