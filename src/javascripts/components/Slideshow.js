/* global document: document */
/* global prefix: 'prefix' */

export function SlideshowItem(store) {
  return {
    template: `<div :class="{ '${prefix}-isVisible': $parent.visible === index }"><slot></slot></div>`,
    props: [
      'index',
    ],
  };
}

export function Slideshow(store) {
  return {
    template: `
      <div :class="{ '${prefix}-Slideshow--active': active }"><slot></slot></div>`,
    props: {
      wait: {
        type: Number,
        default: 5000,
      },
    },
    data() {
      return {
        active: false,
        visible: 0,
      };
    },
    mounted() {
      if (this.$children.length > 1) {
        this.next();
        this.active = true;
      }
    },
    methods: {
      next() {
        setInterval(() => {
          let visible = this.visible += 1;
          if (visible >= this.$children.length) {
            visible = 0;
          }
          this.visible = visible;
        }, this.wait);
      },
    },
  };
}
