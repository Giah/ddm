/* global document: document */
/* global prefix: 'ddm' */

import MainNavigationItem from './MainNavigationItem';

export default function (store) {
  return {
    template: `
      <nav>
        <ul class="${prefix}-MainNavigation-items">
          <MainNavigationItem
            v-for="(item, index) in items"
            :index="index"
            :enabled="!this.smartphone"
            :item="item"
            :selected="selectedIndex === index || selectedIndex === 'ALL'"
            :switch="selectedIndex !== false"
            @select="itemGetSelected" />
        </ul>
      </nav>`,
    data() {
      return {
        items: [],
        smartphone: false,
        media: null,
        selectedIndex: false,
      };
    },
    created() {
      this.getDataFromDOM();
      store.on('DROP_DOWN_MENU_OPENED', (action, state) => { this.selectedIndex = state.DropDownMenuOpened; });
      store.on('MEDIA_CHANGE', (action, state, oldState) => { this.setEnabled(state.media, oldState.media); });
    },
    methods: {
      getDataFromDOM() {
        const items = document.querySelectorAll(`.${prefix}-MainNavigation .${prefix}-MainNavigation-item`);
        const _items = [];
        for (let i = 0; i < items.length; i++) {
          const item = items[i];
          const link = item.querySelector(`.${prefix}-MainNavigation-link`);
          const menu = item.querySelector(`.${prefix}-DropDownMenu`);
          _items.push({
            menu: menu ? menu.innerHTML : false,
            text: link.textContent.trim(),
            title: link.getAttribute('title'),
            href: link.getAttribute('href'),
          });
        }
        this.items = _items;
      },
      itemGetSelected(index) {
        store({
          type: 'DROP_DOWN_MENU_OPENED',
          index: this.smartphone ? 'ALL' : index,
        });
      },
      setEnabled(media, oldMedia) {
        var selectedIndex;
        if (media === 'smartphone') {
          selectedIndex = 'ALL';
          this.smartphone = true;
        } else if (!oldMedia || (media !== 'smartphone' && oldMedia !== media)) {
          selectedIndex = false;
          this.smartphone = false;
        }
        setTimeout(() => {
          store({ type: 'DROP_DOWN_MENU_OPENED', index: selectedIndex });
        });
      },
    },
    components: {
      MainNavigationItem: MainNavigationItem(store), //eslint-disable-line
    },
  };
}
