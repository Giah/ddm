/* global window: window */
/* global prefix: 'ddm' */

export default function (store) {
  return {
    template: `<div :class="{ '${prefix}-Appear':true, '${prefix}-Appear--appeared': appeared }"><slot></slot></div>`,
    data() {
      return {
        desktop: false,
        appeared: false,
      };
    },
    created() {
      store.on('MEDIA_CHANGE', (action, state, oldState) => { this.setEnabled(state.media, oldState.media); });
    },
    mounted() {
      setTimeout(() => {
        this.appear();
        store.on('SCROLL', this.appear);
      }, 500);
    },
    methods: {
      appear() {
        if (this.appeared) {
          return;
        }
        window.requestAnimationFrame(() => {
          var bounds = this.$el.getBoundingClientRect();
          var winHeight = window.innerHeight;
          if (bounds.top < winHeight) {
            this.appeared = true;
          }
        });
      },
      setEnabled(media, oldMedia) {
        if (media === 'desktop') {
          this.desktop = true;
          this.appeared = false;
          setTimeout(this.appear, 500);
        } else if (!oldMedia || (media !== 'desktop' && oldMedia !== media)) {
          this.desktop = false;
          this.appeared = true;
        }
      },
    },
  };
}
