/* global document: document */
/* global prefix: 'ddm' */

export default function (store) {
  return {
    template: `<div :class="{ '${prefix}-isVisible': opened }" :style="style"><slot></slot></div>`,
    data() {
      return {
        media: false,
        opened: false,
        top: 0,
      };
    },
    computed: {
      style() {
        if (this.opened && this.media === 'smartphone') {
          return {
            top: `${this.getPosition()}px`,
          };
        }

        return {};
      },
    },
    created() {
      store.on('MEDIA_CHANGE', (action, state, oldState) => { this.setEnabled(state.media, oldState.media); });
      store.on('TOGGLE_SHOP_FILTERS_MENU', (action, state) => { this.opened = state.shopFiltersMenuOpened; });
    },
    methods: {
      getPosition() {
        var shopHeader = document.querySelector(`.${prefix}-Shop-header`);
        var bb = shopHeader.getBoundingClientRect();
        return bb.bottom;
      },
      setEnabled(media, oldMedia) {
        if (media === 'desktop') {
          this.opened = true;
        } else if (!oldMedia || oldMedia !== media) {
          if (media !== 'desktop') {
            this.opened = false;
          }
          this.media = media;
        }

        setTimeout(() => {
          store({ type: 'TOGGLE_SHOP_FILTERS_MENU', opened: this.opened });
        });
      },
    },
  };
}
