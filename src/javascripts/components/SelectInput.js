/* global window: window */
/* global document: document */
/* global prefix: 'ddm' */

// import { v4 } from 'uuid';
import { cloneDeep } from 'lodash';

const keyCodes = {
  ENTER: 13,
  SPACE: 32,
  LEFT: 37,
  UP: 38,
  RIGHT: 39,
  DOWN: 40,
  ESCAPE: 27,
};

const SelectOption = {
  template: `
    <div
      :class="['${prefix}-Select-option', { '${prefix}-ishighlighted':highlighted }]"
      value="value"
      @click="onClick"
      @mouseenter="onMouseEnter"
    ><slot></slot></div>`,
  props: [
    'index',
    'highlighted',
    'value',
    'onChange',
    'onMounted',
  ],
  mounted() {
    this.$emit('onMounted');
  },
  methods: {
    onClick(event) {
      event.preventDefault();
      event.stopPropagation();
      this.$emit('onChange', this.index);
    },
    onMouseEnter(event) {
      event.preventDefault();
      event.stopPropagation();
      this.$emit('onSelecting', this.index);
    },
  },
};

const SelectIcon = {
  template: `<div :class="[ '${prefix}-Select-icon']"><slot></slot></div>`,
};

const SelectTextbox = {
  template: `<div :class="[ '${prefix}-Select-textbox' ]" tabindex="-1"><slot></slot></div>`,
};

export default function (store) { //eslint-disable-line
  return {
    template: `
      <select v-if="!desktop"><slot></slot></select>
      <div
        v-else
        :data-selected-index="selectedIndex"
        :class="[ '${prefix}-Select', { '${prefix}-isOpened': opened } ]"
        :tabindex="tabindex"
        :style="style"
        @click="onClick"
        @keydown="onKeydown"
        @focusout="onFocusout"
      >
        <slot v-if="!options.length"></slot>
        <template v-else>
          <SelectTextbox>{{ text }}</SelectTextbox>
          <SelectIcon class="${prefix}-ArrowDown ${prefix}-Icon"><svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#arrow-down"></use></svg></SelectIcon>
          <div ref="listbox" :class="['${prefix}-Select-listbox', { '${prefix}-isClosed': !opened }]" tabindex="-1">
            <SelectOption
              v-for="(option, index) in options"
              :value="option.value"
              :index="index"
              :highlighted="index === highlightedIndex"
              @onSelecting="onSelecting"
              @onChange="onChange"
              @onMounted="setWidth"
            >
              {{ option.text }}
            </SelectOption>
          </div>
        </template>
      </div>`,
    props: {
      tabindex: {
        type: [String, Number],
        default: 0,
      },
    },
    data() {
      return {
        text: '',
        options: [],
        opened: true,
        highlightedIndex: 0,
        selectedIndex: 0,
        disabled: false,
        desktop: false,
        style: {
          minWidth: 0,
        },
        childrenRendered: 0,
      };
    },
    mounted() {
      this.getDataFromDOM();
      document.addEventListener('click', () => {
        this.close();
      });
      store.on('MEDIA_CHANGE', (action, state, oldState) => { this.setEnabled(state.media, oldState.media); });
    },
    methods: {
      setEnabled(media, oldMedia) {
        if (media === 'desktop') {
          this.desktop = true;
        } else if (!oldMedia || (media !== 'desktop' && oldMedia !== media)) {
          this.desktop = false;
        }
      },
      getDataFromDOM() {
        let text = '';
        const items = this.$el.querySelectorAll('option');
        const _items = [];
        for (let i = 0; i < items.length; i++) {
          const item = {
            text: items[i].textContent,
            value: items[i].getAttribute('value'),
            selected: items[i].getAttribute('selected'),
          };
          _items.push(item);
          if (i === 0 || item.selected) {
            text = item.text;
            this.selectedIndex = i;
          }
        }
        this.options = cloneDeep(_items);
        this.text = text;
        this.disabled = this.$el.getAttribute('disabled');
      },
      setWidth() {
        if (this.$el.style.minWidth) return;
        this.childrenRendered++;
        if (this.childrenRendered === this.options.length) {
          const width = this.$refs.listbox.getBoundingClientRect().width;
          this.$set(this.style, 'minWidth', `${width}px`);
          this.opened = false;
        }
      },
      open() {
        this.opened = true;
        this.highlightedIndex = this.selectedIndex;
        this.$el.focus();
      },
      close(keepFocus) {
        if (!this.opened) return;
        this.opened = false;
        if (keepFocus) this.$el.focus();
      },
      select(index) {
        let selectedIndex = index;
        if (index < 0) selectedIndex = 0;
        else if (index > this.options.length - 1) selectedIndex = this.options.length - 1;
        this.selectedIndex = selectedIndex;
        this.highlightedIndex = selectedIndex;
        this.text = this.options[selectedIndex].text;
      },
      jumpTo(keyCode) {
        const char = String.fromCharCode(keyCode);
        for (let i = 0; i < this.options.length; i++) {
          if (this.options[i].text.substr(0, 1) === char) {
            this.select(i);
            break;
          }
        }
      },
      onClick(event) {
        event.preventDefault(); event.stopImmediatePropagation();
        if (this.disabled) return;
        if (this.opened) {
          this.close();
          return;
        }
        this.opened = true;
      },
      onChange(index) {
        if (this.disabled) return;
        this.select(index);
        this.close(true);
      },
      onSelecting(index) {
        this.highlightedIndex = index;
      },
      onFocusout(event) {
        if (this.$el.contains(event.relatedTarget) || this.$el === event.relatedTarget) return;
        event.stopPropagation();
        this.select(this.highlightedIndex);
        this.close();
      },
      onKeydown(event) {
        if (this.disabled) return;
        switch (event.keyCode) {
          case keyCodes.ENTER:
            if (!this.opened) { this.open(); return; }
            this.onChange(this.highlightedIndex);
            event.preventDefault(); event.stopPropagation();
            break;
          case keyCodes.SPACE:
            this.open();
            event.preventDefault(); event.stopPropagation();
            break;
          case keyCodes.UP:
            this.select(this.selectedIndex - 1);
            event.preventDefault(); event.stopPropagation();
            break;
          case keyCodes.DOWN:
            this.select(this.selectedIndex + 1);
            event.preventDefault(); event.stopPropagation();
            break;
          case keyCodes.LEFT:
            if (this.opened) return;
            this.select(this.selectedIndex - 1);
            event.preventDefault(); event.stopPropagation();
            break;
          case keyCodes.RIGHT:
            if (this.opened) return;
            this.select(this.selectedIndex + 1);
            event.preventDefault(); event.stopPropagation();
            break;
          case keyCodes.ESCAPE:
            this.close(true);
            event.preventDefault(); event.stopPropagation();
            break;
          default:
            event.preventDefault(); event.stopPropagation();
            this.jumpTo(event.keyCode);
            return;
        }
      },
    },
    components: {
      SelectOption,
      SelectIcon,
      SelectTextbox,
    },
  };
}
