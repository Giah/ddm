/* global prefix: 'ddm' */

export default function (store) {
  return {
    template: `
      <div @click.stop="onClick">
        <span :class="['${prefix}-Icon', '${prefix}-MenuIcon-open', { '${prefix}-isVisible': !closeIcon }]">
          <svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#menu-hamburger-open"></use></svg>
          <svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#menu-hamburger-open-inverted"></use></svg>
        </span>
        <span :class="['${prefix}-Icon', '${prefix}-MenuIcon-close', { '${prefix}-isVisible': closeIcon }]">
          <svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#menu-hamburger-close"></use></svg>
          <svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#menu-hamburger-close-inverted"></use></svg>
        </span>
      </div>`,
    data() {
      return {
        closeIcon: false,
      };
    },
    created() {
      store.on('TOGGLE_MOBILE_MENU', (action, state) => { this.closeIcon = state.mobileMenuOpened; });
    },
    methods: {
      onClick() {
        store({
          type: 'TOGGLE_MOBILE_MENU',
          opened: Boolean(!store.getState().mobileMenuOpened),
        });
      },
    },
  };
}
