/* global prefix: prefix */

export default function (store) {
  return {
    template: `
      <div
        :class="['${prefix}-DropDownMenu', { '${prefix}-isInvisible': !this.selected, '${prefix}-isOpening': (this.selected && this.animate) }]"
        @click.stop=""
        v-html="content"
        ></div>`,
    props: [
      'content',
      'selected',
      'animate',
    ],
  };
}
