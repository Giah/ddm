/* global window: window */
/* global prefix: 'ddm' */

import Sticky from './Sticky';

export default function (store) {
  return {
    template: `
      <form :class="{
          '${prefix}-isPositioned': isPositioned,
          '${prefix}-isSticky': isSticky,
          '${prefix}-stickToBottom': stickToBottom && isPositioned,
        }"
        :style="style"
      >
        <slot></slot>
      </form>`,
    mixins: [
      Sticky(store), //eslint-disable-line
    ],
    mounted() {
      const variantsRadio = this.$el.querySelectorAll('input[name="variant"]');
      variantsRadio[0].checked = true;
      store({ type: 'SELECT_PRODUCT_VARIANT', product_variant: variantsRadio[0].value });

      for (let i = 0; i < variantsRadio.length; i++) {
        variantsRadio[i].addEventListener('change', () => {
          var checked = this.$el.querySelectorAll('input[name="variant"]:checked');
          if (checked.length) {
            store({ type: 'SELECT_PRODUCT_VARIANT', product_variant: checked[0].value });
          }
        });
      }
    },
  };
}
