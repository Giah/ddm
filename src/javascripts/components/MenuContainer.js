/* global prefix: 'ddm' */

export default function (store) {
  return {
    template: `<div :class="[ '${prefix}-MenuContainer', { '${prefix}-isVisible': visible } ]"><slot></slot></div>`,
    data() {
      return {
        smartphone: false,
        opened: false,
        hidden: false,
      };
    },
    computed: {
      visible() {
        var visible = true;

        if (this.smartphone || this.hidden) {
          visible = this.opened;
        }
        if (visible && this.$el) {
          this.$el.scrollTop = 0;
        }
        return visible;
      },
    },
    created() {
      store.on('MEDIA_CHANGE', (action, state, oldState) => { this.setEnabled(state.media, oldState.media); });
      store.on('TOGGLE_MOBILE_MENU', (action, state) => { this.opened = state.mobileMenuOpened; });
    },
    mounted() {
      this.hidden = this.$el.getAttribute('data-hidden');
    },
    methods: {
      setEnabled(media, oldMedia) {
        if (media === 'smartphone') {
          this.smartphone = true;
        } else if (!oldMedia || (media !== 'smartphone' && oldMedia !== media)) {
          this.smartphone = false;
        }

        this.opened = false;

        setTimeout(() => {
          store({ type: 'TOGGLE_MOBILE_MENU', opened: this.visible });
        });
      },
    },
  };
}
