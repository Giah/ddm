/* global window: window */
/* global prefix: 'ddm' */

export default function (store) {
  return {
    data() {
      return {
        desktop: false,
        scrolled: false,
        stickToBottom: false,
        position: {},
      };
    },
    computed: {
      isPositioned() {
        return this.desktop;
      },
      isSticky() {
        return this.scrolled && this.isPositioned;
      },
      style() {
        let style = {};

        if (this.isPositioned && this.scrolled && !this.stickToBottom) {
          style = {
            top: `${this.position.top}px`,
            left: `${this.position.left}px`,
            width: `${this.position.width}px`,
          };
        }

        return style;
      },
    },
    created() {
      store.on('STORE_HEADER_HEIGHT', (action, state) => {
        this.position = Object.assign({}, this.position, {
          top: state.header_height + 1,
        });
      });

      store.on('SCROLL', () => {
        this.storePosition();
      });

      window.addEventListener('resize', () => {
        this.storePosition();
      });

      store.on('MEDIA_CHANGE', (action, state, oldState) => { this.setEnabled(state.media, oldState.media); });
    },
    mounted() {
      this.storePosition();
    },
    methods: {
      storePosition() {
        window.requestAnimationFrame(() => {
          this.scrolled = store.getState().scrollPos;
          const parentNode = this.$el.parentNode;
          const boundingBox = this.$el.getBoundingClientRect();
          const parentBoundingBox = parentNode.getBoundingClientRect();

          const position = Object.assign({}, this.position, {
            left: parentBoundingBox.left,
            width: boundingBox.width,
          });

          if (this.scrolled && boundingBox.bottom >= parentBoundingBox.bottom) {
            this.stickToBottom = true;
          }
          if (boundingBox.top > position.top) {
            this.stickToBottom = false;
          }

          this.position = position;
        });
      },
      setEnabled(media, oldMedia) {
        if (media === 'desktop') {
          this.desktop = true;
        } else if (!oldMedia || (media !== 'desktop' && oldMedia !== media)) {
          this.desktop = false;
        }
      },
    },
  };
}
