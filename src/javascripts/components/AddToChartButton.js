/* global document: document */

export default function (store) {
  return {
    template: `
      <div @click.stop="onClick"><slot></slot></div>`,
    methods: {
      onClick() {
        const selectedVatiantElement = document.querySelector('input[name="variant"]:checked');
        const selectedVariant = selectedVatiantElement ? selectedVatiantElement.value : false;
        if (selectedVariant) {
          const json = document.querySelector(`input[name="${selectedVariant}"]`).value;
          store({
            type: 'ADD_TO_SHOPPING_BAG',
            product: JSON.parse(json),
          });
        }
        return;
      },
    },
  };
}
