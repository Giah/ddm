/* global prefix: 'ddm' */

import Sticky from './Sticky';

const ImageStripThumbnail = {
  template: `
    <div
      :class="['${prefix}-ImageStrip-thumbnail', { '${prefix}-isVisible': selected }]"
      v-html="content"
    />`,
  props: [
    'content',
    'selected',
  ],
};

const ImageStripImage = {
  template: `
    <div :class="['${prefix}-ImageStrip-image']" v-html="content" />`,
  props: [
    'content',
  ],
};

function ImageStripThumbnails(store) {
  return {
    template: `
      <div
        :class="{
          '${prefix}-ImageStrip-thumbnails': true,
          '${prefix}-isPositioned': isPositioned,
          '${prefix}-isSticky': isSticky,
          '${prefix}-stickToBottom': stickToBottom && isPositioned,
        }"
        :style="style"
      >
        <slot></slot>
      </div>`,
    mixins: [
      Sticky(store), //eslint-disable-line
    ],
  };
}

export default function (store) {
  return {
    template: `
      <div>
        <slot v-if="!thumbnails.length || !images.length"></slot>
        <template v-else>
          <ImageStripThumbnails>
            <ImageStripThumbnail v-for="item in thumbnails" :content="item.content" :selected="selected === item.id" />
          </ImageStripThumbnails>
          <div :class="[ 'ddm-ImageStrip-images', { 'ddm-ImageStrip-images--narrow': images.length > 1 } ]">
            <ImageStripImage v-for="(item, i) in images" :content="item.content" />
          </div>
        </template>
      </div>`,
    data() {
      return {
        thumbnails: [],
        images: [],
        selected: '',
      };
    },
    mounted() {
      this.getDataFromDOM();
      store.on('SELECT_PRODUCT_VARIANT', (action, state) => { this.selected = state.product_variant; });
    },
    methods: {
      getDataFromDOM() {
        const thumbnails = this.$el.querySelectorAll(`.${prefix}-ImageStrip-thumbnail`);
        const images = this.$el.querySelectorAll(`.${prefix}-ImageStrip-image`);

        const _thumbnails = [];
        for (let i = 0; i < thumbnails.length; i++) {
          _thumbnails.push({
            id: thumbnails[i].getAttribute('data-id'),
            content: thumbnails[i].innerHTML,
          });
        }

        const _images = [];
        for (let i = 0; i < images.length; i++) {
          _images.push({
            content: images[i].innerHTML,
          });
        }

        this.thumbnails = _thumbnails;
        this.images = _images;
      },
    },
    components: {
      ImageStripThumbnails: ImageStripThumbnails(store), //eslint-disable-line
      ImageStripThumbnail,
      ImageStripImage,
    },
  };
}
