/* global prefix: 'ddm' */

const AccordionTitle = {
  template: `
    <div
      :class="['${prefix}-Accordion-title', {
        '${prefix}-isOpened': opened,
        '${prefix}-isClosed': !opened
      }]"
      v-html="title" @click="onClick"
    />`,
  props: [
    'title',
    'index',
    'opened',
    'select',
  ],
  methods: {
    onClick() {
      this.$emit('select', this.index);
    },
  },
};

const AccordionContent = {
  template: `
    <div
      :class="['${prefix}-Accordion-content', {
        '${prefix}-isOpened': opened,
        '${prefix}-isClosed': !opened
      }]"
      v-html="content" />`,
  props: [
    'content',
    'index',
    'opened',
  ],
};

export default function (store) {
  return {
    template: `
      <div>
        <slot v-if="!items.length"></slot>
        <template v-else v-for="(item, index) in items">
          <AccordionTitle :title="item.title" :index="index" :opened="isOpened(index)" @select="onSelect"/>
          <AccordionContent :content="item.content" :index="index" :opened="isOpened(index)" />
        </template>
      </div>`,
    mounted() {
      var opened = this.$el.getAttribute('data-opened');
      var titles = this.$el.querySelectorAll(`.${prefix}-Accordion-title`);
      var contents = this.$el.querySelectorAll(`.${prefix}-Accordion-content`);
      var items = [];
      for (let i = 0; i < titles.length; i++) {
        items.push({
          title: titles[i].innerHTML,
          content: contents[i].innerHTML,
        });
      }
      this.items = items;

      if (opened) {
        this.opened = opened.split(',').map(item => parseInt(item, 10));
      }
    },
    data() {
      return {
        items: [],
        opened: [],
      };
    },
    methods: {
      isOpened(index) {
        return this.opened.indexOf(index) > -1;
      },
      onSelect(index) {
        var pos = this.opened.indexOf(index);
        if (pos > -1) {
          this.opened.splice(pos, 1);
        } else {
          this.opened.push(index);
        }
      },
    },
    components: {
      AccordionTitle,
      AccordionContent,
    },
  };
}
