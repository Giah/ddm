/* global prefix: 'ddm' */
import DropDownMenu from './DropDownMenu';

export default function (store) {
  return {
    template: `
      <li
        :class="['${prefix}-MainNavigation-item', { '${prefix}-isSelected': (selected !== false && selected !== 'ALL') }]"
        @mouseenter.stop.prevent="onMouseEnter"
        @mouseleave.stop.prevent="onMouseLeave"
      >
        <a class="${prefix}-MainNavigation-link" :href="item.href" :title="item.title">
          {{ item.text }}
          <span v-if="item.menu" class="${prefix}-Icon ${prefix}-MainNavigation-indicator">
            <svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="#menu-triangle"></use></svg>
          </span>
        </a>
        <DropDownMenu
          v-if="item.menu"
          :selected="selected"
          :animate="animate"
          :content="item.menu" />
      </li>`,
    props: [
      'item',
      'index',
      'enabled',
      'switch',
      'selected',
      'select',
    ],
    data() {
      return {
        animate: false,
        timeout: false,
      };
    },
    whatch: {
      selected() {
        if (this.selected === false) {
          this.animate = false;
        }
      },
    },
    methods: {
      onMouseEnter() {
        if (!this.enabled) return;

        let delay;
        if (!this.switch) {
          delay = 1000;
          this.animate = true;
        } else {
          delay = 0;
          this.animate = false;
        }

        this.timeout = setTimeout(() => {
          this.$emit('select', this.index);
        }, delay);
      },
      onMouseLeave() {
        clearTimeout(this.timeout);
        this.timeout = false;
      },
    },
    components: {
      DropDownMenu: DropDownMenu(store), //eslint-disable-line
    },
  };
}
