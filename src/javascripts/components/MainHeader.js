/* global window: window */
/* global prefix: 'ddm' */

export default function (store) {
  return {
    template: `
      <header :class="{
        '${prefix}-isSticky': isSticky,
        '${prefix}-isScrolled': pageScrolled,
        '${prefix}-isMini': isMini,
        '${prefix}-isAnimatingIn': isMini && !alwaysMini,
        '${prefix}-isAnimatingOut': !isMini && !alwaysMini,
      }">
        <slot></slot>
      </header>`,
    data() {
      return {
        smartphone: false,
        headerHeight: 0,
        pageScrolled: false,
        alwaysMini: false,
      };
    },
    computed: {
      isMini() {
        if (this.smartphone) { return false; }
        const mini = this.alwaysMini || (this.pageScrolled >= this.headerHeight);
        return mini;
      },
      isSticky() {
        return !this.smartphone;
      },
    },
    created() {
      store.on('MEDIA_CHANGE', (action, state, oldState) => { this.setEnabled(state.media, oldState.media); });
      store.on('SCROLL', (action, state) => { this.onScroll(state.scrollPos); });
    },
    mounted() {
      if (this.$el.getAttribute('data-always-mini')) this.alwaysMini = true;
      this.storeHeight();
    },
    updated() {
      this.storeHeight();
    },
    methods: {
      setEnabled(media, oldMedia) {
        if (media === 'smartphone') {
          this.smartphone = true;
        } else if (!oldMedia || (media !== 'smartphone' && oldMedia !== media)) {
          this.smartphone = false;
        }
        this.storeHeight();
      },
      onScroll(scrollPos) {
        window.requestAnimationFrame(() => {
          this.pageScrolled = scrollPos || false;
        });
      },
      storeHeight() {
        if (!this.$el) {
          return;
        }
        setTimeout(() => {
          const height = (this.alwaysMini || this.smartphone) ? 0 : this.$el.clientHeight;
          this.headerHeight = height;
          store({ type: 'STORE_HEADER_HEIGHT', height });
        }, 150);
      },
    },
  };
}
