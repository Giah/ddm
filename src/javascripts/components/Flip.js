/* global document: document */
/* global prefix: 'ddm' */

export default function (store) {
  return {
    template: `<div :class="{ '${prefix}-isFlipped':flipped }" @click="onClick"><slot></slot></div>`,
    data() {
      return {
        flipped: false,
      };
    },
    methods: {
      onClick(event) {
        const target = event.target;
        if (target.closest('[data-hook-flip]')) {
          event.preventDefault();
          this.flipped = !this.flipped;
        }
      },
    },
  };
}
