/* global prefix: prefix */

export default function (store) {
  return {
    template: `
      <div
        :class="[ '${prefix}-ShopEmotional-formSection ddm-ShopEmotional-formSection--slide', { '${prefix}-isSelected': $parent.visibleSection === sectionIndex, } ]"
        @click="onClick"
      >
        <slot></slot>
      </div>`,
    props: [
      'sectionIndex',
    ],
    methods: {
      onClick(event) {
        event.preventDefault();
        const el = event.srcElement;
        if (el.classList.contains(`${prefix}-ShopEmotional-button`)) {
          const selects = this.$children;
          if (!selects.length || selects[0].selectedIndex > 0 || selects[0].$el.selectedIndex > 0) {
            this.$parent.visibleSection++;
          }
        }
      },
    },
  };
}
