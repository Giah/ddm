/* global prefix: 'ddm' */

export default function (store) {
  return {
    template: `
      <div @click.stop="onClick"><slot></slot></div>`,
    data() {
      return {
        closeIcon: false,
      };
    },
    created() {
    },
    methods: {
      onClick() {
        store({
          type: 'TOGGLE_SHOP_FILTERS_MENU',
          opened: Boolean(!store.getState().shopFiltersMenuOpened),
        });
      },
    },
  };
}
