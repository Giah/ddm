/* global document: document */
/* global prefix: 'ddm' */

const hookRand = 'Xt4f';

export default function (store) {
  return {
    template: `
      <div :class="{ '${prefix}-isVisible': visible }" @click="onClick"><slot></slot></div>`,
    data() {
      return {
        visible: false,
      };
    },
    created() {
      store.on('ADD_TO_SHOPPING_BAG', (action, state) => { this.addToShoppingBag(state.product); });
    },
    methods: {
      onClick(event) {
        var target = event.target;
        if (target.closest(`[data-hook="close-${hookRand}"]`)) {
          this.visible = false;
        }
      },
      addToShoppingBag(product) {
        if (this.$el) {
          document.querySelector(`[data-hook="name-${hookRand}"]`).innerHTML = product.name;
          document.querySelector(`[data-hook="metal-${hookRand}"]`).innerHTML = product.metal;
          document.querySelector(`[data-hook="stone-${hookRand}"]`).innerHTML = product.stone;
        }
        this.visible = true;
      },
    },
  };
}
