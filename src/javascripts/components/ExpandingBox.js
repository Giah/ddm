/* global prefix:'ddm' */

export default function (store) {
  return {
    template: `
      <div :class="{
          '${prefix}-ExpandingBox': true,
          '${prefix}-isLastCol': isLastCol,
          '${prefix}-isLastRow': isLastRow,
          '${prefix}-isShifted': isShifted,
          '${prefix}-isExpanded': isExpanded,
        }"
        :style="style"
        @mouseenter="onMouseEnter"
        v-html="item.content"
      />`,
    props: [
      'item',
      'isLastCol',
      'isLastRow',
      'isShifted',
      'isExpanded',
    ],
    data() {
      return {
        style: {},
      };
    },
    watch: {
      isShifted() {
        if (this.isShifted) {
          this.style = {
            transform: `translateX(${this.isShifted * 100}%)`,
            transformOrigin: this.isShifted < 0 ? '100% 0' : '0 0',
          };
        } else {
          this.style = {};
        }
      },
    },
    methods: {
      onMouseEnter() {
        this.$emit('expand', this.item.row, this.item.col);
      },
    },
  };
}
