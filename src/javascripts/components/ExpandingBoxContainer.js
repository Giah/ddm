/* global prefix:'ddm'; */

import { cloneDeep } from 'lodash';
import ExpandingBox from './ExpandingBox';

/**
 *
 * Map the items in rows and cols based on the position
 * Append an 'index' prop to each item which correspond to the index
 * of the item in the itemsMap array in order to cross referencing
 * between 'itemsMap' and 'itemsMatrix'
 *
 */
function getItemsMatrix(nodes, items) {
  const _items = cloneDeep(items);
  if (_items.length < 2) return;

  const l = _items.length;
  let i = 0;
  let row = 0;
  let col = 0;

  let item = { ..._items[i] };
  item.index = i;
  const itemsMatrix = [[]];
  itemsMatrix[0][0] = item;

  for (i = 1; i < l; i++) {
    item = _items[i];
    item.index = i;
    const node = nodes[i];
    const prevNode = nodes[i - 1];

    if (node.offsetLeft > prevNode.offsetLeft) {
      col++;
    }

    if (node.offsetTop > prevNode.offsetTop) {
      col = 0;
      row++;
    }

    if (!itemsMatrix[row]) {
      itemsMatrix[row] = [];
    }
    itemsMatrix[row][col] = item;
  }

  return itemsMatrix; //eslint-disable-line
}

/**
 *
 * Read the position of each item, and add the following
 * props to each one of them:
 *
 * item.row {number}
 * item.col {number}
 * item.isLastCol {bool}
 * item.isLastRow {bool}
 *
 */
function getItemsMap(items, itemsMatrix) {
  const _items = cloneDeep(items);
  const _itemsMatrix = cloneDeep(itemsMatrix);

  /**
   *
   * Mark the following items, which have a different tranform origin
   * 1. Last item of each row
   * 2. Items from the last row (except last in the corner, see below)
   * 3. Last item of the last row, if it's in the corner (i.e. when the last row is full)
   *
   */
  const lastRowIndex = _itemsMatrix.length - 1;
  const firstRowLength = _itemsMatrix[0].length;
  const lastRowLength = _itemsMatrix[lastRowIndex].length;
  const lastRowIsFull = firstRowLength === lastRowLength;

  for (let row = 0; row < itemsMatrix.length; row++) {
    const rowLength = itemsMatrix[row].length;
    for (let col = 0; col < rowLength; col++) {
      const index = _itemsMatrix[row][col].index;
      _items[index].row = row;
      _items[index].col = col;
      _items[index].lastCol = false;
      _items[index].lastRow = false;

      if (col === rowLength - 1) { // last element of each row
        _items[index].lastCol = true;
        if (!lastRowIsFull) {
          _items[index].lastCol = false;
        }
      }

      if (row === lastRowIndex) {
        _items[index].lastRow = true;
      }
    }
  }

  return _items;
}

export default function (store) {
  /**
   *
   * Use the <slot> tag at first to get the content from the page HTML
   * Once mounted, replace each item with <ExpandingBox> components
   *
   */
  return {
    template: `
      <div @mouseleave="onMouseLeave">
        <slot v-if="!itemsMap.length"></slot>
        <ExpandingBox v-else v-for="(item, index) in itemsMap"
          :item="item"
          :isLastCol="item.lastCol"
          :isLastRow="item.lastRow"
          :isShifted="shifted[index]"
          :isExpanded="expanded === index"
          @expand="expand"
        />
      </div>`,
    data() {
      return {
        media: false,
        enabled: false,
        boxes: null,
        items: [],
        itemsMap: [],
        itemsMatrix: [[]],
        expanded: false,
        shifted: {},
      };
    },
    mounted() {
      store.on('MEDIA_CHANGE', (action, state, oldState) => { this.setEnabled(state.media, oldState.media); });
    },
    methods: {
      setEnabled(media, oldMedia) {
        if (media === 'smartphone') {
          this.smartphone = true;
        } else if (!oldMedia || (media !== 'smartphone' && oldMedia !== media)) {
          this.smartphone = false;
          this.expanded = false;
          this.shifted = false;
          this.mapItems();
        }
      },
      mapItems() {
        /**
         *
         * map them in rows and cols
         *
         */
        const boxes = this.$el.querySelectorAll(`.${prefix}-ExpandingBox`);
        const items = [];
        for (let i = 0; i < boxes.length; i++) {
          const box = boxes[i];
          items.push({
            text: box.textContent.trim() || '',
            title: box.getAttribute('title') || '',
            href: box.getAttribute('href') || '',
            content: box.innerHTML || '',
          });
        }
        this.boxes = boxes;
        this.items = items;
        this.itemsMatrix = getItemsMatrix(this.boxes, this.items);
        this.itemsMap = getItemsMap(this.items, this.itemsMatrix);
      },
      expand(row, col) {
        if (this.smartphone) {
          return;
        }
        const index = this.itemsMatrix[row][col].index;
        const item = this.itemsMap[index];
        const isLastRow = item.lastRow;
        const isLastCol = item.lastCol;

        const rows = [row];
        rows[1] = isLastRow ? row - 1 : row + 1;

        const shifted = {};

        this.itemsMatrix[rows[0]].forEach((_item, _col) => {
          const _index = this.itemsMatrix[rows[0]][_col].index;
          if (isLastCol && _col < col) {
            shifted[_index] = -1;
          } else if (_col > col) {
            shifted[_index] = 1;
          }
        });

        this.itemsMatrix[rows[1]].forEach((_item, _col) => {
          const _index = this.itemsMatrix[rows[1]][_col].index;
          if (isLastCol && _col <= col) {
            shifted[_index] = -2;
          } else if (_col >= col) {
            shifted[_index] = 2;
          }
        });

        this.expanded = index;
        this.shifted = shifted;
      },
      onMouseLeave() {
        this.expanded = false;
        this.shifted = false;
      },
    },
    components: {
      ExpandingBox: ExpandingBox(store), //eslint-disable-line
    },
  };
}
