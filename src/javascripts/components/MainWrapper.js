/* global window: window */
/* global document: document */
/* global prefix: 'ddm' */
import Vue from 'vue';
import AsyncComputed from 'vue-async-computed';
import MainHeader from './MainHeader';
import MainNavigation from './MainNavigation';
import MenuContainer from './MenuContainer';
import MenuIcon from './MenuIcon';
import ExpandingBoxContainer from './ExpandingBoxContainer';
import Accordion from './Accordion';
import ProductForm from './ProductForm';
import ImageStrip from './ImageStrip';
import ShopEmotionalForm from './ShopEmotionalForm';
import ShopEmotionalFormSection from './ShopEmotionalFormSection';
import ShopFiltersMenuButton from './ShopFiltersMenuButton';
import ShopFiltersMenu from './ShopFiltersMenu';
import AddToChartButton from './AddToChartButton';
import ProductModal from './ProductModal';
import Flip from './Flip';
import Appear from './Appear';
import SelectInput from './SelectInput';
import { Slideshow, SlideshowItem } from './Slideshow';

Vue.use(AsyncComputed);

export default function (store) {
  return new Vue({
    el: `#${prefix}-MainWrapper`,
    data() {
      return {
        smartphone: false,
        navHeight: 0,
      };
    },
    created() {
      store.on('MEDIA_CHANGE', (action, state, oldState) => { this.setEnabled(state.media, oldState.media); });
      store.on('STORE_HEADER_HEIGHT', (action, state) => { this.setPaddingTop(state.header_height); });
      store.on('TOGGLE_MOBILE_MENU', (action, state) => { this.togglePageOverflow(state.mobileMenuOpened); });
      store.on('TOGGLE_SHOP_FILTERS_MENU', (action, state) => { this.togglePageOverflow(state.shopFiltersMenuOpened); });
    },
    mounted() {
      this.onScroll();
      document.addEventListener('click', this.onClick);
      window.addEventListener('scroll', this.onScroll);
    },
    methods: {
      setEnabled(media, oldMedia) {
        if (media === 'smartphone') {
          this.smartphone = true;
        } else if (!oldMedia || (media !== 'smartphone' && oldMedia !== media)) {
          this.smartphone = false;
        }
      },
      onClick() {
        if (!this.smartphone) {
          store({ type: 'DROP_DOWN_MENU_OPENED', index: false });
        }
      },
      onScroll() {
        if (!this.smartphone) {
          store({ type: 'SCROLL', scrollPos: (window.scrollY || window.pageYOffset) });
        }
      },
      setPaddingTop(padding) {
        this.$el.style.paddingTop = `${padding}px`;
      },
      togglePageOverflow(mobileMenuOpened) {
        if (!this.smartphone || !mobileMenuOpened) {
          document.body.style.overflow = 'auto';
        } else {
          document.body.style.overflow = 'hidden';
        }
      },
    },
    components: {
      /*eslint-disable */
      MainHeader: MainHeader(store),
      MainNavigation: MainNavigation(store),
      MenuContainer: MenuContainer(store),
      MenuIcon: MenuIcon(store),
      ExpandingBoxContainer: ExpandingBoxContainer(store),
      Accordion: Accordion(store),
      ProductForm: ProductForm(store),
      ImageStrip: ImageStrip(store),
      ShopEmotionalForm: ShopEmotionalForm(store),
      ShopEmotionalFormSection: ShopEmotionalFormSection(store),
      ShopFiltersMenuButton: ShopFiltersMenuButton(store),
      ShopFiltersMenu: ShopFiltersMenu(store),
      AddToChartButton: AddToChartButton(store),
      ProductModal: ProductModal(store),
      Flip: Flip(store),
      Appear: Appear(store),
      SelectInput: SelectInput(store),
      Slideshow: Slideshow(store),
      SlideshowItem: SlideshowItem(store),
      /*eslint-enable */
    },
  });
}
