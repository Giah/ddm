/* global window: window */
/* global document: document */
/* global prefix: prefix */

export default function (store) {
  return {
    template: `
      <form :class="'${prefix}-ShopEmotional-form--slide'"><slot></slot></form>`,
    data() {
      return {
        visibleSection: 0,
      };
    },
    watch: {
      visibleSection() {
        if (this.visibleSection === this.$children.length) {
          window.location.href = this.$el.action;
        }
      },
    },
  };
}
