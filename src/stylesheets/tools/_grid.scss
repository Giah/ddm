@function is-fraction($size) {
  $is_number: type-of($size) == number;
  $has_unit: $is_number and unitless($size) == false;
  $is_percentage: $is_number and unit($size) == %;
  @return $is_number and $has_unit == false and $is_percentage == false and $size < 1;
}

/**
 *
 * Setup a container to be a grid row
 *
 * @param {number} $options.gutter=0
 * @param {list} $options.align-items="left stretch" - Possible values ("left","right","center","distribute") ("stretch","top","bottom","center")
 * @param {bool} $options.wrap=true
 * Examples:
 *  @include row;
 *  @include row((gutter: 10px));
 *  @include row((align-items: left center)); // vertical center items
 *
 */
@mixin row($options:()) {
    $_defaults: (
        gutter: 0px,
        align-items: left stretch,
        wrap: true
    );

    $_options: map-merge($_defaults, $options);

    $gutter: map-get($_options, gutter);
    $align-items: map-get($_options, align-items);
    $wrap: map-get($_options, wrap);

    box-sizing: border-box;
    display: flex;
    flex: 0 1 auto;
    flex-direction: row;
    overflow: hidden;

    @include row-gutter($gutter);
    @include row-wrap($wrap);
    @include row-align-items($align-items);
}

/**
 *
 * Setup an element to be a grid column
 *
 * @param {auto|number|percentage|fraction} $options.size=auto
 * @param {number} $options.gutter=0 - This value usually match the container row gutter setting
 * @param {list} $options.align-items="auto" - Possible values ("auto","stretch","top","bottom","center")
 * @param {number} $options.order=1 - Change the default item display order
 *
 * Examples:
 *  @include col();
 *  @include col((size:auto, gutter:10px));
 *  @include col((size:100px, gutter:10px));
 *  @include col((size:25%, gutter:10px));
 *  @include col((size:4/12, gutter:$col-gutter));
 *
 */
@mixin col($options:()) {
    $_defaults: (
        size: auto,
        gutter: 0px,
        align: auto,
        order: 1
    );

    $_options: map-merge($_defaults, $options);

    $size: map-get($_options, size);
    $gutter: map-get($_options, gutter);
    $align: map-get($_options, align);
    $order: map-get($_options, order);

    @include col-size($size, $gutter);
    @include col-align($align);
    @include col-order($order);
}

@mixin row-gutter($gutter: 0) {
  margin-right: $gutter / 2 * -1;
  margin-left: $gutter / 2 * -1;
}
/**
 *
 * First value align items horizontally, second value align items vetically.
 *
 * @param {list} $options.align-items="left stretch" - Possible values ("left","right","center","distribute") ("stretch","top","bottom","center")
 *
 */
@mixin row-align-items($align-items: left stretch) {
    $align-h: nth($align-items, 1);
    $align-v: nth($align-items, 2);

    @if     ($align-h == left) { justify-content: flex-start; }
    @elseif ($align-h == right) { justify-content: flex-end; }
    @elseif ($align-h == center) { justify-content: center; }
    @elseif ($align-h == distribute) { justify-content: space-between; }

    @if     ($align-v == top) { align-items: flex-start; align-content: flex-start; }
    @elseif ($align-v == bottom) { align-items: flex-end; align-content: flex-end; }
    @elseif ($align-v == center) { align-items: center; align-content: flex-center; }
    @elseif ($align-v == stretch) { align-items: stretch; align-content: flex-stretch; }
}

/**
 *
 * Allow or prohibit row items to wrap on multiple lines
 *
 * @param {bool} $options.wrap=true
 *
 */
@mixin row-wrap($wrap: true) {
    @if ($wrap == true) {
        flex-wrap: wrap;
    }
    @else {
        flex-wrap: nowrap;
    }
}

/**
 *
 * Set the size of the column
 *
 * @param {auto|grow|number|percentage|fraction} $size=auto
 * @param {number} $gutter=0 - This value usually match the container row gutter setting
 *
 * Examples:
 *  @include col-size();
 *  @include col-size(50%);
 *  @include col-size(auto, 10px);
 *  @include col-size(100px, 10px);
 *  @include col-size(25%, 10px);
 *  @include col-size(4/12, $col-gutter);
 *
 */
@mixin col-size($size: auto, $gutter: 0px) {
    @if ($size == auto) {
        flex-grow: 0;
        flex-shrink: 0;
        flex-basis: auto;
        min-width: 0;
        max-width: 100%;
    }
    @elseif ($size == grow) {
        flex-grow: 1;
        flex-shrink: 1;
        flex-basis: auto;
        min-width: 0;
        max-width: 100%;
    }
    @else {
        $size_min: null;
        $size_max: null;

        @if (type-of($size) == list) {
          $size_min: nth($size, 1);
          $size_max: nth($size, 2);
        }
        @else {
          $size_min: $size;
          $size_max: $size;
        }

        @if(is-fraction($size_min)) { $size_min: percentage($size_min); }
        @if(is-fraction($size_max)) { $size_max: percentage($size_max); }

        $grow: 0;
        @if ($size_min != $size_max) {
          $grow: 1;
        }

        flex-grow: $grow;
        flex-shrink: 0;
        flex-basis: auto;
        width: #{$size_min};
        min-width: #{$size_min};
        max-width: #{$size_max};
    }

    padding-left: $gutter / 2;
    padding-right: $gutter / 2;
}

/**
 *
 * Align the column vertically inside the containing row
 *
 * @param {list} $align="auto" - Possible values ("auto","stretch","top","bottom","center")
 *
 */
@mixin col-align($align: auto) {
    @if ($align != auto) {
        @if     ($align == top)     { align-self: flex-start; }
        @elseif ($align == bottom)  { align-self: flex-end; }
        @elseif ($align == center)  { align-self: center; }
        @elseif ($align == stretch) { align-self: stretch; }
    }
}

/**
 *
 * Change the column display order inside the containing row
 *
 * @param {number} $order=1 - Change the default item display orders
 *
 */
@mixin col-order($order: 1) {
    order: $order;
}
