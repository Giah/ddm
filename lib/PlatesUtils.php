<?php

use League\Plates\Engine;
use League\Plates\Extension\ExtensionInterface;

class PlatesUtils implements ExtensionInterface {

  protected $engine;
  public $template;

  public function register(Engine $engine) {
    $this->engine = $engine;
    $engine->registerFunction('asset', [$this, 'asset']);
    $engine->registerFunction('attributes', [$this, 'attributes']);
    $engine->registerFunction('classes', [$this, 'classes']);
    $engine->registerFunction('capture', [$this, 'capture']);
    $engine->registerFunction('mark', [$this, 'mark']);
    $engine->registerFunction('defaults', [$this, 'defaults']);
  }


  /**
   *
   * Return a versioned path for static assets
   *
   * @param {string} $path
   * @return {string}
   *
   */
  public function asset($path) {
    $data = $this->template->data();
    $paths = $data['paths'];

    if (!file_exists($paths['public'] . '/' . $paths['rev_manifest'])) {
      return $paths['public'] . '/' . $path;
    }

    $rev_manifest = json_decode(file_get_contents($paths['public'] . '/' . $paths['rev_manifest']), true);
    $rev_manifest_normalized = [];
    foreach ($rev_manifest as $key => $value) {
      $key_normalized = str_replace('\\', '/', $key);
      $value_normalized = str_replace('\\', '/', $value);
      $rev_manifest_normalized[$key_normalized] = $value_normalized;
    }
    $rev_manifest = $rev_manifest_normalized;

    if(array_key_exists($path, $rev_manifest)) {
      return $paths['public'] . '/' . $rev_manifest[$path];
    }

    return $paths['public'] . '/' . $path;
  }

  /**
   *
   * Render a list of attributes.
   *
   * @param $variables {array} $attributes - The array key is rendered as attributes name while the corresponding value is rendered as the attributes value
   * @return {string}
   *
   */
  public function attributes($attributes) {
    $_attributes = [];

    foreach ($attributes as $key => $value) {
      if (is_string($key) && $value) {
        $_attributes[$key] = $key . '="' . $this->template->e($value) . '"';
      }
    }

    return implode(' ', $_attributes);
  }

  /**
   *
   * Render a list of classes.
   *
   * When a string is passed, it gets rendered as it is.
   *
   * When $class is an array:
   *  If the index is integer the value gets added to the string.
   *  If the index is a string, the correnspoding value gets evaluated and, if not falsy, the index gets added to the resulting string.
   *
   * @param {string|array} $classes
   * @return {string}
   *
   * @example
   * [ 'a', 'b', 'c' ] = 'a b c'
   * [ 'a', 'b':false, 'c':true ] = 'a c'
   * [ 'a', 'b':'foo', 'c':0 ] = 'a b'
   *
   */
  public function classes($class = []) {
    $_class = [];

    if (is_string($class)) {
      $class = [$class];
    }

    foreach ($class as $key => $value) {
      if (is_int($key)) {
        $_class[] = $this->template->e($value);
      }
      elseif (is_string($key) && $value) {
        $_class[] = $this->template->e($key);
      }
    }

    $result = self::attributes([
      'class' => implode(' ', $_class)
    ]);

    return $result;
  }

  /**
   *
   * Capture the output into a variable
   *
   * @param {bool} $start - Enter or exit capture mode.
   *
   */
  public function capture($start) {
    if ($start) {
      ob_start();
    }
    else {
      return ob_get_clean();
    }
  }


  /**
   *
   * Generate HTML comments to mark the begin and end of templates
   *
   * @param {bool} [$end=false] - Whether the comment is at beginning or at the end of the template
   *
   */
  public function mark($end = false) {
    $data = $this->template->data();
    $debug = $data['debug'];
    if (!$debug) {
      return;
    }
    return $end
    ? "<!-- BEGIN " . basename($this->template->path(), '.php') . " -->"
    : "<!-- END " . basename($this->template->path(), '.php') . " -->";
  }

}
