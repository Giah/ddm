<?php
class DataProvider {

  protected $_mirror;
  protected $_skipPublic = [
    'toArray',
    'output_buffer_contents'
  ];
  protected $_faker;

  public function __construct($faker_seed = NULL) {
    $this->_mirror = new \ReflectionClass($this);
    $this->_faker = \Faker\Factory::create();
    if ($faker_seed) {
      $this->_faker->seed($faker_seed);
    }
  }

  public static function output_buffer_contents($function, $args = array()){
    ob_start();
    $function($args);
    return ob_end_clean();
  }

  public function toArray() {
    $data = [];

    $properties = $this->_mirror->getProperties(\ReflectionMethod::IS_PUBLIC);
    foreach ($properties as $property) {
      $key = $property->name;
      if (!in_array($key, $this->_skipPublic)) {
        $data[$key] = $this->$key;
      }
    }

    $methods = $this->_mirror->getMethods(\ReflectionMethod::IS_PUBLIC);
    foreach ($methods as $method) {
      $key = $method->name;
      if (!in_array($key, $this->_skipPublic)) {
        $data[$key] = $this->$key();
      }
    }

    return $data;
  }
}
